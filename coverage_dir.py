#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
It calculates coverage descriptive statistics of BAM files in given directory, according to a genomic intervals reference BED file.
Requires installation of the following packages:
    - pandas
    - pybedtools
    - numpy
    - xlsxwriter
    - xlwt
    - matplotlib
    - multiprocessing

Bed file should contains 6 columns. See format in example below:

#CHR	START	END	LOCUS	SCORE	STRAND
chr1	955552	955753	AGRN_int_01	AGRN	+
chr1	957580	957842	AGRN_int_02	AGRN	+
chr1	970656	970704	AGRN_int_03	AGRN	+

Where LOCUS column contains exon unique id and SCORE column contains gene id (for grouping).

* Use data.frame functions:
    - read_sql(sql, con[, index_col, ...])	Returns a DataFrame corresponding to the result set of the query
    - read_frame(sql, con[, index_col, ...])	Returns a DataFrame corresponding to the result set of the query
    - write_frame(frame, name, con[, flavor, ...])	Write records stored in a DataFrame to a SQL database.
    

"""

__author__ = "Javier Diez Perez"
__credits__ = ["Javier Diez Perez"]
__license__ = "GPL"
__version__ = "0.1"
__maintainer__ = "Javier Diez Perez"
__email__ = "jdiezperezj@gmail.com"
__status__ = "Development"


import os
import sys
from string import upper
import argparse
from pandas import DataFrame,read_table
import pybedtools
import numpy
import xlsxwriter	#Use xlswriter to insert graphics, have a look to annotate_mito.py
from operator import itemgetter
import multiprocessing
import matplotlib
import matplotlib.pyplot as plt

class CoverageHDF5(object):
    """ Class to work with coverage data stored in hdf5 file, containing: summary, genes and exons tables."""
    def __init__(self, hdf5):
	self.hdf5 = hdf5
	self.summary = self.hdf5['summary']
	self.genes = self.hdf5['genes']
	self.exons = self.hdf5['exons']
    
    def get_pivot_matrix(self,):
	""" Get pivot matrix by sample and a given DF field. """
	pass
    
    def plot_x(self,):
	""" Plot functions."""
	pass
    


class BaseCoverage(object):
    """ This class calculate coverage statistics for a given set of bases. """
    def __init__(self,base_coverage):
	self.base_coverage = base_coverage
	self.length_design = len(base_coverage)
	self.mean = "{0:.2f}".format(numpy.mean(self.base_coverage))
	self.median = "{0:.2f}".format(numpy.median(self.base_coverage))
	self.coverage_values()
	self.cov_summary()

    def coverage_values(self):
	""" Returns the base coverage equal or above a given threshold. """
        self.c250=0
        self.c100=0
	self.c50=0
	self.c30=0
	self.c20=0
	self.c10=0
	self.c1=0
	self.c0=0
	for cov in self.base_coverage:
            if cov >= 250:
                self.c250 += 1
                self.c100 += 1
		self.c50 += 1
		self.c30 += 1
		self.c20 += 1
		self.c10 += 1
		self.c1 += 1
            elif cov >= 100:
                self.c100 += 1
		self.c50 += 1
		self.c30 += 1
		self.c20 += 1
		self.c10 += 1
		self.c1 += 1
	    elif cov >= 50:
		self.c50 += 1
		self.c30 += 1
		self.c20 += 1
		self.c10 += 1
		self.c1 += 1
	    elif cov >= 30:
		self.c30 += 1
		self.c20 += 1
		self.c10 += 1
		self.c1 += 1
	    elif cov >= 20:
		self.c20 += 1
		self.c10 += 1
		self.c1 += 1
	    elif cov >= 10:
		self.c10 += 1
		self.c1 += 1
	    elif cov >= 1:
		self.c1 += 1
	    elif cov == 0:
		self.c0 += 1

    def pc_coverage(self,value):
	""" This function calculates the percentage of the original region(s) coveraged at a given threshold. """
	#return "{0:.2f}%".format((value/float(self.length_design))*100)
	return (value/float(self.length_design))*100
	#return 
    
    def cov_summary(self):
	""" Represents the percentages of coverage of the targeted region(s) at different threshold values: 1X, 10X, 20X, 30X and 50X. """
	self.pc0 = self.pc_coverage(self.c0)
	self.pc1 = self.pc_coverage(self.c1)
	self.pc10 = self.pc_coverage(self.c10)
	self.pc20 = self.pc_coverage(self.c20)
	self.pc30 = self.pc_coverage(self.c30)
	self.pc50 = self.pc_coverage(self.c50)
        self.pc100 = self.pc_coverage(self.c100)
        self.pc250 = self.pc_coverage(self.c250)

    def stats_summary(self,sample,subpanel='all_genes'):
	""" Return the summary list to display in the report. """
	df = DataFrame(data=[[subpanel,self.length_design, self.mean, self.median,self.pc0, self.pc1, self.pc10, self.pc20, self.pc30, self.pc50, self.pc100, self.pc250]], columns= ['SUBPANEL','LENGTH','MEAN','MEDIAN','0X','1X','10X','20X','30X','50X','100X','250X'], dtype=numpy.float64, index = [sample,])
	return df


class CovStats(object):
    """
    Coverage statistics object deal with DataFrame subsets.
    Perform simple statistics calculations.
    """
    def __init__(self,name,group,thr=30,sample=''):
	self.sample = sample
	self.name = name
        self.group = group
	self.thr = thr
	self.length = str(len(self.group))
	self.values = self.group.to_dict()['COV'].values()
	self.chr = self.group.to_dict()['CHR'].values()[0]
	self.pos0 = min(self.group.to_dict()['START'].values()) - 1
	self.stop = max(self.group.to_dict()['STOP'].values())

    def get_constiguous(self,data):
	""" Return contiguous intervals of not covered regions. """
	intervals =[]
	interval=[]
	for i in data:
	    if len(interval) == 0:
		interval = [i,]
	    else:
		if max(interval) + 1 == i:
                    interval += [i,]
            	else:
                    intervals += [(min(interval),max(interval)),]
                    interval = [i,]
	if len(interval) > 0:
	    intervals += [(min(interval),max(interval)),]
	return intervals

    def get_pos_blcov(self,cov=0):
	""" Represent url in Genome Browser for positions not covered. """
	data = [self.pos0 + i for i,j in zip(self.group.to_dict()['POS'].values(),self.values) if j <= cov]
	if len(data) > 0:
	    intervals = self.get_constiguous(data)
	    sorted(intervals,key=itemgetter(0))
	    #return " ,".join(["http://genome-euro.ucsc.edu/cgi-bin/hgTracks?db=hg19&position={0}%3A{1}-{2}".format(self.chr,v[0],v[1]) for v in intervals])
	    return " ,".join(["{0}:{1}-{2}".format(self.chr,v[0],v[1]) for v in intervals])
	else:
	    return ''

    def get_n_blcov(self,cov=0):
	""" Positions above coverage threshold. """
	return [self.pos0 + i for i,j in zip(self.group.to_dict()['POS'].values(),self.values) if j <= cov]

    def get_pos_abcov(self,cov=0):
	""" Coverage value above a given value. """
	return [i for i,j in zip(self.group.to_dict()['POS'].values(),self.values) if j >= cov]

    def get_mean(self):
	""" Mean coverage value. """
	return "{0:.2f}".format(numpy.mean(self.values))

    def get_median(self):
	""" Median coverage values. """
	return "{0:.2f}".format(numpy.median(self.values))

    def get_std(self):
	""" Standard deviation of coverage values for the interval. """
	return "{0:.2f}".format(numpy.std(self.values))

    def get_min(self):
	""" Min coverage value in the interval. """
	return min(self.values)
    
    def get_max(self):
	""" Max coverage value in the interval. """
        return max(self.values)

    def get_pcAbvThr(self,thr):
	""" Percentage of a region obove a given threshold. """
        return "{0:.2f}".format((len(self.get_pos_abcov(thr))/float(self.length))*100)

    def get_pc0(self):
	""" Percentage of reagion not covered. """
        return "{0:.2f}".format((len(self.get_n_blcov())/float(self.length))*100)

    def get_stats(self):
	""" Return statistics. """
	results = [self.name,self.chr,self.pos0 + 1,self.stop,self.length,self.get_mean(),self.get_std(),self.get_median(),self.get_min(),self.get_max(), self.get_pcAbvThr(1), self.get_pcAbvThr(10), self.get_pcAbvThr(20),self.get_pcAbvThr(self.thr), self.get_pcAbvThr(100), self.get_pcAbvThr(250),self.get_pc0(), self.get_pos_blcov(), self.get_pos_blcov(10), self.get_pos_blcov(20),self.get_pos_blcov(100), self.get_pos_blcov(250)  ]
	if self.sample != '':
	    results = [self.sample,] + results
	return results

    def get_header(self):
	""" Represent excel header file. """
	header = ['exon','chr','start','stop','length','mean','std','median','min','max','pc_>{thr}X'.format(thr=1),'pc_>{thr}X'.format(thr=10), 'pc_>{thr}X'.format(thr=20),'pc_>{thr}X'.format(thr=self.thr),'pc_>{thr}X'.format(thr=100),'pc_>{thr}X'.format(thr=250),'pc0','pos_0_cov','pos_=<10X_cov', 'pos_=<20X_cov', 'pos_=<100X_cov', 'pos_=<250X_cov']
	if self.sample != '':
	    header = ['sample',] + header
	return header
    
def pc_coverage(base_coverge,thr=10):
    """ Calculate percentage of coverage."""
    length_design = len(base_coverage)
    base_coverage = [c for c in base_coverage if c >= thr]
    return (sum(base_coverage)/float(length_design)) * 100


def coverage_barplots(gdf,outdir,sample,name,gene_group,thr=30):
    """ Creates barplots for gene and exons. """
    df = DataFrame(gdf[map(upper,['pc_1X','pc_10X','pc_20X','pc_{thr}X'.format(thr=thr),'pc_0_cov'])], dtype=numpy.float64)
    df.index = gdf.LOCUS
    #plt.xkcd()
    fig = plt.figure(figsize=(30,6))	#Try to apply rule that varies x axis length according to the data.
    ax  = fig.add_subplot(211)
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width*0.9, box.height])
    df.plot(ax=ax, kind = 'bar',legend=False, colormap='terrain',title= 'Coverage summary')
    plt.ylabel('% coverage')
    fig.autofmt_xdate()
    filename = os.path.join(outdir, "_".join([sample,gene_group, name + '.png']))
    ax.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol=2, mode="expand", borderaxespad=0.)
    plt.savefig(filename,bbox_inches='tight',dpi=150)
    plt.close(fig)
    return filename

def coverage_rawplots(df,sample,name,outdir):
    """ """
    filenames = {}
    boundaries = DataFrame({'mins':df.groupby(['SCORE','LOCUS'])['START'].idxmin(),'maxs':df.groupby(['SCORE','LOCUS'])['STOP'].idxmax()})
    #print boundaries.to_string()
    for gene in df['SCORE'].unique():
	coverage = df[['POS','COV','LOCUS','ABS_POS','SCORE']][df['SCORE'].isin([gene,])]
	filename = os.path.join(outdir, "_".join([sample, name + '_' + str(gene) +'_'+'.png']))
	#plt.xkcd()
	fig = plt.figure()
	ax  = fig.add_subplot(111)
	coverage[['COV']].plot(ax=ax, title=" - ".join([sample,gene,'base coverage']), legend=False, color="#014051")
	
	#ax = plt.axes()
	
	ax.spines['top'].set_visible(False)
	ax.spines['right'].set_visible(False)
	ax.spines['bottom'].set_visible(False)
	plt.tick_params(\
	    axis='x',          # changes apply to the x-axis
	    which='both',      # both major and minor ticks are affected
	    bottom='off',      # ticks along the bottom edge are off
	    top='off',         # ticks along the top edge are off
	    labelbottom = 'off')
	ax.tick_params(axis='y', labelsize=9)
	
	ax.xaxis.grid(False)
	ax.set_ylim(bottom=0)
	plt.xlabel('cDNA coordinates')
	plt.ylabel('coverage')
	plt.axhline(y=10, linestyle='-', alpha=True, label='10X', ms=0.7, c='#e50005')
	plt.axhline(y=20, linestyle='-', alpha=True, label='20X', ms=0.7, c='#e59400')
	plt.axhline(y=30, linestyle='-', alpha=True, label='30X', ms=0.7, c='#558943')
	plt.axhspan(0, 10, facecolor='0.3', alpha=0.2)
	
	max_cov = coverage[['COV']].max()
	max_cov =  max_cov['COV']
	n=0
	for exon in coverage['LOCUS'].unique():
	    coverage_e = coverage[['COV']][coverage['SCORE'].isin([exon,])]
	    b = boundaries.ix[gene].ix[exon]['mins']
	    n += 1
	    if n !=1:
		plt.axvline(x=int(b), ymin=0, ymax=int(max_cov), linestyle=':', alpha=True, ms=0.5, c='black')
	    #plt.axvline(x=100, ymin=0, ymax=100, linestyle='.', alpha=True, ms=0.7, c='red')
	    #plt.axvline(b, linestyle='.', alpha=True, ms=0.7, c='red')
	    #plt.axvspan(xmin, xmax, ymin=0, ymax=1, hold=None, **kwargs)#To display exon boundaries.
	    ax.text(int(b),int(max_cov),'int_'+str(n), style='italic', fontsize=6, rotation='vertical', color='#61269e')
	plt.savefig(filename,dpi=175)
	filenames[gene] = filename
	plt.close(fig)
    return filenames


def pivot_data(data):
    """ """
    result={}
    for i in ['10X','20X','30X','50X','100X','250X']:
	result[i] = data.pivot(index='LOCUS', columns='SAMPLE', values='PC_{val}'.format(val=i))
    result


    #import matplotlib.colors as mcolors
    #cmap, norm = mcolors.from_levels_and_colors([0, 0.5, 1], ['red', 'green'])
    #cmap, norm = mcolors.from_levels_and_colors([0, 80,90,95,99,], ['red', 'orange', 'yellow','green','blue'])
    #plt.pcolor(df > 0.5, cmap='RdYlGn')
    #http://nbviewer.ipython.org/github/ucsd-scientific-python/user-group/blob/master/presentations/20131016/hierarchical_clustering_heatmaps_gridspec.ipynb
    #http://stackoverflow.com/questions/8342549/matplotlib-add-colorbar-to-a-sequence-of-line-plots
    #Improve colorbar apearance -> http://datasciencelab.wordpress.com/2013/12/21/beautiful-plots-with-pandas-and-matplotlib/

def heat_map(df, filename, title='', rows=6, cols=11, min=0, max=100):
    """ Function to plot heatmap and color bar. """
    #min, max = (0, 100)
    step = 10
    mymap = matplotlib.colors.LinearSegmentedColormap.from_list('mycolors',['white','yellow','green','blue'])
    Z = [[0,0],[0,0]]
    levels = range(min,max+step,step)
    CS3 = plt.contourf(Z, levels, cmap=mymap)
    fig = plt.figure(figsize=(rows,cols))
    ax1 = plt.axes(frameon=False)
    plt.tick_params(axis='x', which='both', top='off', labelsize=5)
    plt.tick_params(axis='y', labelsize=5, left='off',right='off')
    plt.pcolor(df,cmap=mymap)
    plt.yticks(numpy.arange(0.5, len(df.index), 1), df.index)
    plt.xticks(numpy.arange(0.5, len(df.columns), 1), df.columns)
    locs, labels = plt.xticks()
    plt.setp(labels, rotation=90)
    fig.patch.set_visible(False)
    cbar=plt.colorbar(CS3, shrink=.3, aspect=10)
    cbar.ax.tick_params(labelsize=5)
    cbar.outline.set_visible(False)
    plt.title(title)
    plt.savefig(filename,dpi=175)
    plt.close(fig)
    return filename


def calculate_coverage(sample,bam_f,bed_f,thr,only_genes,gene_group,out_dir):
    """ This function calculates coverage for all intervals using PyBedTools. """
    
    bed = pybedtools.BedTool(bed_f)
    bam = pybedtools.BedTool(bam_f)
    
    #global SUMMARY, EXONS, GENES
    
    #cols = ['chr','start','stop','locus','score','strand','pos','cov']
    cols = ['SAMPLE', 'CHR', 'START', 'STOP', 'LOCUS', 'SCORE', 'STRAND', 'POS', 'ABS_POS', 'COV']
    #cov_data = [[i.fields[0], numpy.int(i.fields[1]), numpy.int(i.fields[2]), i.fields[3], i.fields[4], i.fields[5], numpy.int(i.fields[6]), numpy.int(i.fields[7])] for i in bam.coverage(bed, d=True)]
    cov_data = [[sample,i.fields[0], numpy.int(i.fields[1]), numpy.int(i.fields[2]), i.fields[3], i.fields[4], i.fields[5], numpy.int(i.fields[6]),numpy.int(i.fields[6]) + numpy.int(i.fields[1]), numpy.int(i.fields[7])] for i in bam.coverage(bed, d=True)]
    
    
    summary = BaseCoverage([c[9] for c in cov_data]).stats_summary(sample)	
    print(summary)
    
    #SUMMARY FOR THE SUBPANELS:
    
    header = map(upper,('sample','locus','chr','start','stop','length','mean','std','median','min','max','pc_1X','pc_10X','pc_20X','pc_{thr}X'.format(thr=thr),'pc_100X','pc_250X','pc_0_cov','pos_0_cov','pos_=<10X_cov','pos_=<20X_cov', 'pos_=<100X_cov','pos_=<250X_cov'))
    
    group_name = "_".join(gene_group)
    
    try:
        df = DataFrame(cov_data,columns = cols)
	figures = coverage_rawplots(df,sample,'raw_coverage',out_dir)
	
	
	if only_genes == False:
	    grouped = df.groupby('LOCUS', as_index = False)
	    e_rows = [CovStats(name, group, sample = sample, thr=thr).get_stats() for name,group in grouped]
	    edf= DataFrame(e_rows, columns = header)
	    exon_fig = coverage_barplots(edf,out_dir,sample,'exons',group_name)

	else:
	    edf = False
	g_grouped = df.groupby('SCORE' ,as_index=False)
	g_rows = [CovStats(name, group, sample = sample, thr=thr).get_stats() for name,group in g_grouped]
	gdf= DataFrame(g_rows, columns = header)
	gene_fig = coverage_barplots(gdf,out_dir,sample,'genes',group_name)

    except Exception,e:
	print e
    else:
	return {'sample':sample, 'summary':summary, 'df':df, 'gdf':gdf, 'g_plot':gene_fig , 'edf':edf, 'e_plot':exon_fig, 'gene_plots':figures}	#Use dataframe as output


def DFtoXLSX(ws,df,startrow,startcol,formats):
    """ Transform a dataframe into a excel spreadsheet. """
    #Read the following about conditional formating.
    #https://xlsxwriter.readthedocs.org/working_with_conditional_formats.html
    #Working with tables.
    #https://xlsxwriter.readthedocs.org/working_with_conditional_formats.html
    #data = df.values.tolist()
    #l = len(data)
    #header = df.columns.values.tolist()
    #n = len(header)
    #columns = [{'header':i} for i in header]
    #ws.add_table(startrow, startcol, startrow + l, startcol + n, {'data':data,'first_column': True,'header_row':header,'banded_rows': 0, 'banded_columns': 1})
    header = df.columns.values.tolist()
    for i in range(len(header)):
	ws.write(startrow, startcol+i, header[i],formats['header_format'])
    startrow += 1
    for row, data in enumerate(df.values.tolist()):
	for i in range(len(data)):
	    if i == 0:
		ws.write(row + startrow, i+startcol, data[i],formats['names_format'])
	    else:
		ws.write(row + startrow, i+startcol, data[i])
	    """if i in range(2,15):
		    try:
		        int(data[i])
		    except (TypeError,ValueError):
		        ws.write(row + startrow, i+startcol, data[i])
		    else:
		        ws.write_number(row + startrow, i+startcol, data[i])"""
    return ws

def xlsxw(outdir,run_name,sample_name,data, only_genes,gene_group,logo,genes):
    """ """
    
    filename = os.path.join(outdir,"_".join([run_name, sample_name]) + '.xlsx')
    report_book = xlsxwriter.Workbook(filename)
    formats = {}
    header_format = report_book.add_format({'bold': True, 'font_color': '#444444', 'bottom_color':'#444444'})
    title_format = report_book.add_format({'bold': True, 'font_color': 'blue'})
    subt_format = report_book.add_format({'font_color': 'green'})
    names_format = report_book.add_format({'bold': True})
    
    formats['header_format'] = header_format
    formats['title_format'] = title_format
    formats['subt_format'] = title_format
    formats['names_format'] = title_format
    
    summary_worksheet = report_book.add_worksheet('Summary')
    summary_worksheet.insert_image('A1', logo, {'x_scale': 1, 'y_scale': 1})
    summary_worksheet.write('B2', 'Centre for Molecular Pathology', title_format)
    summary_worksheet.set_row(1, 70)
    summary_worksheet.write('B4', 'Run name:', subt_format)
    summary_worksheet.write('C4', run_name)
    summary_worksheet.write('B5', 'Sample name:', subt_format)
    summary_worksheet.write('C5', sample_name)
    if gene_group != False:
	summary_worksheet.write('B6', 'Panels:', subt_format)
	summary_worksheet.write('C6', " ,".join(gene_group))
	summary_worksheet.write('B7', 'Genes tested:', subt_format)
	summary_worksheet.write('C7', genes)
    
    #Write summary content
    summary_worksheet = DFtoXLSX(summary_worksheet,data['summary'],9,1,formats)
    summary_worksheet.insert_image('B15', data['g_plot'])
    
    #Write Genes
    genes_worksheet = report_book.add_worksheet('Genes')
    genes_worksheet.write('A2', 'Genes Report', title_format)
    
    genes_worksheet = DFtoXLSX(genes_worksheet,data['gdf'][data['gdf'].columns.values.tolist()[1:]],3,1,formats)	#Table will not contains neither runname nor sample columns.
    genes_worksheet.autofilter('B4:S4')
    
    #Write Exons
    if only_genes == False:
	exons_worksheet = report_book.add_worksheet('Exons')
	exons_worksheet.write('A2', 'Exons Report', title_format)
	
	exons_worksheet = DFtoXLSX(exons_worksheet,data['edf'][data['edf'].columns.values.tolist()[1:]],3,1,formats)	#Table will not contains neither runname nor sample columns.
	exons_worksheet.autofilter('B4:S4')
    
    #Add plot tabs ?
    figs={}
    for k,v in sorted(data['gene_plots'].items()):
	figs[k] = report_book.add_worksheet(k)
	figs[k].insert_image('B2', v, {'x_scale': 0.5, 'y_scale': 0.5})
    
    report_book.close()


def pcoverage(params):
    """ Parallel wrapper function. """
    #data = calculate_coverage(params['samplename'], params['bam_file'], params['bed'], params['thr'], params['only_genes'], params['gene_group'], params['out_dir'], params['lrdb'])
    data = calculate_coverage(params['samplename'], params['bam_file'], params['bed'], params['thr'], params['only_genes'], params['gene_group'], params['out_dir'])
    xlsxw(params['out_dir'], params['rname'], params['basename'], data, params['only_genes'],params['gene_group'], params['logo'], params['genes'])
    return data


def get_group_genes(groups,gene_group_file,panel='trusight',):
    """Returns a tuple of gene ids that corresponds to group content. """
    result = []
    try:
	if gene_group_file == False:
	    basedir = os.path.split(os.path.abspath(__file__))[0]
	    filename = os.path.join(os.path.abspath(basedir),'annotation',panel,'genegroups.tsv')
	else:
	    filename = gene_group_file
	df = read_table(filename)
    except Exception,e:
	print e
    else:
	ref_groups = set(sorted(df['GROUP'].values.tolist()))
	groups = set(groups)
	if groups.issubset(ref_groups) != True:
	    sys.exit("Groups {g} are not in reference groups: {r}".format(g=",".join(groups), r=",".join(ref_groups)))
	else:
	    try:
	        for i in df[df['GROUP'].isin(groups)]['GENES'].values.tolist():
		    i = i.rstrip()
	            result += i.split(',')
	    except Exception,e:
	        print e
	    else:
		r = tuple(sorted(set(result)))
		print r
	        return r

def subset_bed(full_bed,genes_of_interest,outdir,group_name):
    """ Subset original design bedfile for a group of desired genes and returns another subset bedfile. """
    original = pybedtools.BedTool(full_bed)
    subset = pybedtools.BedTool([i for i in original if i.score in genes_of_interest])
    subset_file = os.path.join('/tmp',group_name)
    subset.saveas(subset_file)
    return subset_file	# BedTool object could be returned as well to intersect with the bam file.

def idx_and_flag_stats(bamfile):
    """ An easy way to have idx stats for a given bamfile. """
    import pysam
    filename = bamfile
    print reduce(lambda x, y: x + y, [ eval('+'.join(l.rstrip('\n').split('\t')[2:]) ) for l in pysam.idxstats(filename) ])
    print reduce(lambda x, y: x + y, [ eval('+'.join(l.rstrip('\n').split('\t')[2:]) ) for l in pysam.flagstat(filename) ])


def create_hdf5(filename):
    """ HDF file object to store coverage for all samples. It will allow calculations. """
    from pandas import HDFStore
    return HDFStore(filename,complevel=9, complib='bzip2')


def remove_by_extension(path,exts=('.png', '.jpg')):
    """ Remove files in a given directory that end with a give set of extensions. """
    for root, dirs, files in os.walk(path):
        for currentFile in files:
            print "processing file: " + currentFile
            if any(currentFile.lower().endswith(ext) for ext in exts):
                os.remove(os.path.join(root, currentFile))


def subset_df(df,sample,genes,output='dataframe'):
    """ Subset a coverage dataset. """
    df = df[df['SAMPLE'].isin([sample,])]
    df = df[df['SCORE'].isin(genes)]
    if output == 'dataframe':
        return df
    elif output == 'list':
	return list(df.to_records())
    elif output == 'coverage':
	return list(df['COV'])
    else:
	raise ValueError('Valid arguments: dataframe, list, coverage.')

def subset_df_panels(df,sample,groups):
    """ """
    st = False
    for group,genes in groups:
	coverage = subset_df(df,sample,genes,output='COV')
	bc = BaseCoverage(coverage).stats_summary(sample, subpanel = group)
	if st == False:
	    st = bc
	else:
	    st.append(bc)
    return st

def subset_df_sample(df):
    """ Subset a dataframe by sample. """
    for sample in sorted(list(df['SAMPLE'].unique())):
        yield sample, df[df['SAMPLE'].isin([sample,])]


def subset_df_groups_bc(df,groups):
    """ Given a coverage data_frame and groups dictionary we get summary statistics for each of the subpanels. """
    for sample,i in subset_df_sample(df):
	for j, g in enumerate(groups.keys()):
	    cov = list(i[i['LOCUS'].isin([groups[g],])]['COV'])
	    coverage = BaseCoverage(cov).stats_summary(sample,subpanel = g)
	    if j == 0:
		result = coverage
	    else:
		result.append(coverage)
    return result	#return a DF that contains summary statistics for subpanels for a given sample.
    
    
#Plotting functions here:
def create_locus_plot():
    """ """
    pass

def create_sample_plot():
    """ """
    pass


def main(args):
    print args
    sys.stderr.write("Calculating coverage.\n")
    
    header = map(upper,['sample','locus','chr','start','stop','length','mean','std','median','min','max','pc_1X','pc_10X','pc_20X','pc_{thr}X'.format(thr=args.thr),'pc_100X','pc_250X','pc_0_cov','pos_0_cov','pos_=<10X_cov', 'pos_=<20X_cov','pos_=<100X_cov', 'pos_=<250X_cov' ])
    params = []
    if args.gene_group != False:
	group_name = "_".join(args.gene_group) + '.bed'
	genes_of_interest = get_group_genes(args.gene_group,args.gene_group_file)
	subset = subset_bed(args.bed,genes_of_interest,args.out_dir,group_name)
	args.bed = subset
    elif args.gene_group == 'all':
	args.bed = args.bed
    else:
	args.bed = args.bed
	
    if args.out_dir == False:
	args.out_dir = os.getcwd()

    for f in os.listdir(args.in_dir):
	ext = ".".join([args.ext,'bam'])
	if f.endswith(ext):
	    d = {}
	    d['out_dir'] = args.out_dir
	    d['basename'] = f.split('.')[0]
	    d['bam_file'] = os.path.join(args.in_dir,f)
	    head,tail = os.path.split(f)
	    d['samplename'] = tail.split('.')[0]
	    d['bed'] = args.bed
	    d['thr'] = args.thr
	    d['rname'] = args.rname
	    d['header'] = header
	    d['only_genes'] = args.only_genes
	    d['gene_group'] = args.gene_group
	    d['logo'] = args.logo
	    d['genes'] = ",".join(genes_of_interest)
	    params.append(d)

    if len(params) > 1:
	data={}
	pool = multiprocessing.Pool(args.n_cores)
	for p in pool.imap_unordered(pcoverage,params):
	    print p['sample']
	    if 'summary' not in  data.keys():
		data['summary'] = p['summary']
		data['df'] = p['df']
		data['genes'] = p['gdf']
		data['exons'] = p['edf']
	    else:
		data['summary'] = data['summary'].append(p['summary'])
		data['base_cov'] = data['df'].append(p['df'])
		data['genes'] = data['genes'].append(p['gdf'])
		data['exons'] = data['exons'].append(p['edf'])
	
	#Remove files by extension
	remove_by_extension(args.out_dir)
	

	cov_f = os.path.join(args.out_dir,args.rname + '_cov.hdf5')
	store = create_hdf5(cov_f)

	#Summary
	store.append('summary', data['summary'])
	store['summary'] = store['summary'].convert_objects(convert_numeric=True)
	name = ".".join([args.rname,"summary","csv"])
	filepath = os.path.join(args.out_dir,name)
	store['summary'].to_csv(filepath)
	
	store.append('base_cov', data['base_cov'])
	store['base_cov'] = store['base_cov'].convert_objects(convert_numeric=True)

	#Genes
	store.append('genes', data['genes'])
	store['genes'] = store['genes'].convert_objects(convert_numeric=True)
	name = ".".join([args.rname,"genes","csv"])
	filepath = os.path.join(args.out_dir,name)
	store['genes'].to_csv(filepath)

	#Exons
	store.append('exons', data['exons'])
	store['exons'] = store['exons'].convert_objects(convert_numeric=True)
	name = ".".join([args.rname,"exons","csv"])
	filepath = os.path.join(args.out_dir,name)
	store['exons'].to_csv(filepath)

	#Genes HeatMap 100X
	g_hmp = os.path.join(args.out_dir,'genes_100X_heatmap.pdf')
	df_g = store['genes'].pivot(index='LOCUS', columns='SAMPLE', values='PC_100X')
	heat_map(df_g,g_hmp,title='Gene coverage 100X')

	#Genes HeatMap 250X
	g_hmp = os.path.join(args.out_dir,'genes_250X_heatmap.pdf')
	df_g = store['genes'].pivot(index='LOCUS', columns='SAMPLE', values='PC_250X')
	heat_map(df_g,g_hmp,title='Gene coverage 250X')

    store.close()    
    sys.stderr.write("Coverage calculation is done.\n")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description = '')
    parser.add_argument('-i','--in_dir', required = True, help = 'Processed BAM directory.')
    parser.add_argument('-o','--out_dir', required = False, default= False, help = 'Report output directory.')
    parser.add_argument('-b','--bed', required = True, help = 'Bed file representing targeted regions. Only bed 6 file format is allowed, where 4th column contains the unique ID of the intervals. The fith column contains grouping factor, usually locus name, to group intervals (exons).')
    parser.add_argument('-r','--rname', required = True, help = 'Run name. It will be used as suffix for the excel file.')
    parser.add_argument('-s','--sep', required = False, default='.', help = 'Separator character.')
    parser.add_argument('-e','--ext', required = False, default='.rd', help = 'BAM extension character.')
    parser.add_argument('-n','--n_cores',type=int, required = False, default=10, help = 'Number of cores to parallelize. Default value is 4.')
    parser.add_argument('-t','--thr', required = False, default=30, type=int, help = 'Coverage threshold value. Default: 30X.')
    parser.add_argument('--only_genes', dest='only_genes', action='store_true', default=False, help = 'Display only gene info.')
    parser.add_argument('-g','--gene_group', nargs = '*', required=False, default=False, help='Report only genes in desired groups. Default: all.')
    parser.add_argument('--gene_group_file', required = False, default=False, help = 'User provided gene groups file. Format: GROUP\tgene1,gene2,...')
    parser.add_argument('-l','--logo', required = False, default='/home/jdiezperez/data/media/rmh_logo.jpeg', help = 'NHNN logo file path.')
    parser.add_argument('--rundb', required = False, help = 'Local Run Database path.')
    args = parser.parse_args()
    main(args)


