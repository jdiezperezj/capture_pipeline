#!/usr/bin/env bash

#Scripts takes the following arguments:
#variant_folder, interval_file, name,  path
args=("$@")
echo ${args}

VARIANT_FOLDER=${args[0]}
INTERVALS=${args[1]}
HOTSPOTS=${args[2]}
BASENAME=${args[3]}
FILENAME=${args[4]}
HG19=/home/jdiezperez/data/references/hg19/hg19.fa
ONCOTATOR=/home/pipeuser/data/annotation/oncotator/oncotator_v1_ds_June112014
VEP=/home/pipeuser/data/annotation/vep/ensembl-tools-release-75/scripts/variant_effect_predictor/variant_effect_predictor.pl

SAMTOOLS=/opt/samtools-0.1.19/samtools

VCF_LOG="${VARIANT_FOLDER}/${BASENAME}.log"
VCF_CNS="${VARIANT_FOLDER}/${BASENAME}.cns.vcf"
VCF_CNS_SORTED="${VARIANT_FOLDER}/${BASENAME}.cns.vcf.gz"
VCF_CNS_SORTED_FILTERED="${VARIANT_FOLDER}/${BASENAME}.cns.filtered.vcf"
ONCOTATOR_FILE="${VARIANT_FOLDER}/${BASENAME}.tsv"
VEP_VCF="${VARIANT_FOLDER}/${BASENAME}.vep.vcf"
VEP_VCF_GZ="${VARIANT_FOLDER}/${BASENAME}.vep.vcf.gz"
VCF_SAMPLE_LIST="${VARIANT_FOLDER}/${BASENAME}.sample.list"

echo ${BASENAME} > ${VCF_SAMPLE_LIST} ;

#Variant calling:
/opt/samtools-0.1.19/samtools mpileup -l $INTERVALS -f ${HG19} ${FILENAME} | java -jar /opt/VarScan.v2.3.6.jar mpileup2cns --strand-filter 1 --vcf-sample-list ${VCF_SAMPLE_LIST} --output-vcf 1 --min-avg-qual 25 --min-coverage 20 --min-reads2 3 --min-var-freq 0.01 --p-value 0.001 1> ${VCF_CNS} 2>> ${VCF_LOG} ;

#sorted
cat ${VCF_CNS} | vcf-sort | gzip -c9 1> ${VCF_CNS_SORTED} 2>> ${VCF_LOG};

#filtered
vcf_hotspots_filter.py -i ${VCF_CNS_SORTED} -s ${HOTSPOTS} -o ${VARIANT_FOLDER};

#annotation
oncotator -o SIMPLE_TSV -i VCF --db-dir ${ONCOTATOR}  ${VCF_CNS_SORTED_FILTERED} ${ONCOTATOR_FILE} hg19;
perl  ${VEP} --allow_non_variant --no_progress --per_gene --cache --refseq --canonical --everything --database --vcf --force_overwrite -i ${VCF_CNS_SORTED_FILTERED} -o ${VEP_VCF} ;
bgzip -f -c ${VEP_VCF} 1> ${VEP_VCF_GZ} 2>> ${VCF_LOG} ;
tabix -p vcf ${VEP_VCF_GZ} ;

#Remove tmp files
rm  ${VARIANT_FOLDER}/${BASENAME}.*.html ;
rm  ${VARIANT_FOLDER}/${BASENAME}.*.vcf ;
rm ${VCF_CNS_SORTED} ;
rm ${VCF_SAMPLE_LIST} ;

#Structural:
#./delly_v0.5.9_parallel_linux_x86_64bit /home/jdiezperez/data/results/GI/RUN11/Alignments/RM3994006-2_S2.rd.bam -o /home/jdiezperez/data/results/GI/RUN11/SV/sv.vcf -t INV -g /home/jdiezperez/data/references/hg19/hg19.fa

#CNV
