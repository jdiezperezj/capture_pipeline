#!/usr/bin/env python

import os
import subprocess
import pysam
import argparse
import multiprocessing


class FastqPair(object):
    pass


class FastqFolder(object):
    pass


def parse_fastq_name(f,directory):
    """ """
    n = f.split(".fastq.gz")[0]
    id,sample,lane,read,other = n.split("_")
    d = {"path":os.path.join(directory,f),"id":id,"sample":sample, "lane":lane, "read":read, "base":n, "filename":f}
    return d


def fastq_file_generator(fastq_folder,end=".fa.gz"):
    """ """
    for f in sorted(os.listdir(fastq_folder)):
        if f.endswith(end):
            f = parse_fastq_name(f,fastq_folder)
            yield f


def fastq_pair_generator(fastq_folder):
    """ """
    fastq_iter = fastq_file_generator(fastq_folder)
    for i in fastq_iter:
        j = fastq_iter.next()
        if i['id'] == j['id'] and i['sample'] == j['sample'] and i['read'] != j['read']:
            name = "_".join([i['id'],i['sample']])
            yield {'id':i['id'],'sample':i['sample'],i['read']:i['path'],j['read']:j['path'],"R1_base":i["base"],"R2_base":j["base"],"name":name}


def align_amplicon_sample(f):
    """ """
    print f
    outdir, bedfile ,sample = f
    cmd = ["novoalign_amplicon.sh", outdir, bedfile, sample["name"], sample["R1"], sample["R2"]]#outdir,amplicons,basename,read1,read2
    subprocess.check_call(cmd)
    return (sample["name"], "Done")



def bam_file_generator(bamfolder,end=".bam"):
    """ """
    for f in sorted(os.listdir(fastq_folder)):
        if f.endswith(end):
            name = f.split(".")[0]
            p = os.path.join(bamfolder,f)
            yield name,p

def call_variants(f):
    """ """
    variant_folder, interval_file, name, path = f
    cmd = ["varscan_amplicon.sh", variant_folder, interval_file, name, path]
    subprocess.check_call(cmd)
    return (name,"Done")


def main(args):
    files = [(args.aln, args.bed,f) for f in fastq_pair_generator(args.fastq_in) if f["sample"] != "S0"]
    pool = multiprocessing.Pool(args.cores)
    for p in pool.imap_unordered(align_amplicon_sample, files):
        print "\t".join([str(p[0]),str(p[1])])


if __name__ == '__main__':
    parser = argparse.ArgumentParser("")
    parser.add_argument('--fastq', dest='fastq_in', required=True, help='Fastq folder.')
    parser.add_argument('--bed', dest='bed', required=True, help='Amplicon bed file.')
    parser.add_argument('--index', dest='index', required=True, help='Index file.')
    parser.add_argument('--aln', dest='aln', required=False, help='Alignment out directory.')
    parser.add_argument('--cores', dest='cores', type=int, default=6, required=False, help='Number of cores to use')
    args = parser.parse_args()
    main(args)
