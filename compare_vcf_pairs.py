#!/usr/bin/env python
# -*- coding: utf-8 -*-

import hashlib
import os
import sys
import vcf
from operator import itemgetter
from string import upper
import argparse


class VCF_file(object):
    """ VCF file wrapper. """
    def __init__(self,f,directory):
	""" Fields to be provided. """
	self.f = f
	self.directory = directory
	self.get_path()
	self.get_fields()
	self.parse_vcf()
	
    def __repr__(self):
	return self.name
    
    def is_pair(self,other):
	""" Check if compared VCF objects are pairs. """
	if other.id1 == self.id1 and other.id2 == self.id2 and self.project == other.project and self.ctype == other.ctype:
	    if self.rep != other.rep:
		return True
	    else:
		return False
	else:
	    False
    
    def get_path(self,):
	""" It returns file path. """
	self.path = os.path.join(self.directory,self.f)
    
    def get_fields(self,):
	""" It get id fields from file name. """
	self.name = self.f.split('.vcf')[0]
	self.fs, self.sid = self.name.split('_')
	self.id1,self.id2,self.project,self.ctype,self.rep = self.fs.split('-')

    def parse_vcf(self,thr=150):
	""" Very simple VCF parser. To be improved in next version. """
	self.hash = {}
	vcf_records = [record for record in vcf.Reader(open(self.path, 'r'))]
	for i in vcf_records:
	    if len(i.samples) == 1 and i.samples[0].is_variant == True:
		s = i.samples[0]
		r_dc,a_dc = s.data.AD
		cov = r_dc + a_dc
		cov_l = "HIGH_COV" if cov >= thr else "LOW_COV" 
		frq = "{0:.2f}".format((float(a_dc)/float(cov))* 100)
		h = get_hash("_".join(map(str,[s.site.CHROM, s.site.POS,s.site.REF, s.site.ALT[0]])))
		chr = format_chr(s.site.CHROM)
		self.hash[h] = [chr, s.site.POS,s.site.REF, s.site.ALT[0], cov_l, r_dc,a_dc,frq,]
	
class VCF_pair(object):
	""" This class deal with VCF pair object as sequencing replicates. """
    def __init__(self,a,b):
	self.p1 = a
	self.p2 = b
	self.data = self.cmp_vcf_hash()
	self.data_dict = (self.p1.id1,self.data)
    
    def check_pair(self,):
	""" This function check if files are true VCF pair files. """
	if self.p1.is_pair(self.p2) != True:
	    raise PairError(self.p1,self.p2)
	
    def get_paths(self,):
	""" It returns a tuple of file absoulte paths."""
	return (self.p1.path, self.p2.path)
    
    def cmp_vcf_hash(self,):
	""" It returns values."""
	s1 = set(self.p1.hash.keys())
	intersection = s1.intersection(set(self.p2.hash.keys()))
	res = sorted([self.p1.hash[i] + self.p2.hash[i][-4:] for i in intersection], key=itemgetter(0,1))
	return res



class PairError(Exception):
	""" Error class for VCF pairs. """
    def __init__(self, p1,p2):
	self.value = "Files {p1} and {p2} are not pairs.".format(p1=p1,p2=p2)
    def __str__(self):
	return repr(self.value)

class DirPairError(Exception):
    """ Error class for paired VCF directory. """
    def __init__(self, msg,n):
	self.value = msg % n
    def __str__(self):
	return repr(self.value)

    
def get_hash(word):
    """ Returns hash from input string. """
    word = unicode(word)
    hash_object = hashlib.md5(word)
    return hash_object.hexdigest()

def format_chr(chr):
    """ Format chromosome as two digit str for autosomes. """
    chr = chr[3:] if chr.startswith('chr') else chr
    try:
	chr = format(int(chr), '02d')
    except Exception,e:
	return chr
    else:
	return chr

def parse_vcf(filename,thr=150):
    """ Very simple parser. To be fully implemented later on. """
    res = {}
    basename = filename.split('.')[0]
    vcf_records = [record for record in vcf.Reader(open(filename, 'r'))]
    for i in vcf_records:
	if len(i.samples) == 1 and i.samples[0].is_variant == True:
	    s = i.samples[0]
	    r_dc,a_dc = s.data.AD
	    cov = r_dc + a_dc
	    cov_l = "HIGH_COV" if cov >= thr else "LOW_COV" 
	    frq = "{0:.2f}".format((float(a_dc)/float(cov))* 100)
            h = get_hash("_".join(map(str,[s.site.CHROM, s.site.POS,s.site.REF, s.site.ALT[0]])))
	    chr = format_chr(s.site.CHROM)
	    res[h] = [chr, s.site.POS,s.site.REF, s.site.ALT[0], cov_l, r_dc, a_dc, frq,]	#cov_l: class (HIGH or LOW coverage variant), r_dc: reference allele depth of coverage, a_dc: alternative allele depth of coverage , frq: allele frequency
	else:
	    pass
    return res

def cmp_vcf_hash(h1,h2):
    """ Comparisson of vcf hashes list. Returns variant intersection. """
    s1 = set(h1.keys())
    intersection = s1.intersection(set(h2.keys()))
    res = sorted([h1[i] + h2[i][-4:] for i in intersection], key=itemgetter(0,1))
    return res

def create_excel_file(data,outfile,header=False):
    """ Create a multitab excel file from a dictionary of elements. """
    from xlsxwriter import Workbook as wb
    workbook = wb(outfile)
    worksheets={}
    for k in data.keys():
	worksheets[k] = workbook.add_worksheet(k)
	if header != False:
	    worksheets[k].write_row(1, 0, header)
	for row, row_data in enumerate(data[k]):
	    worksheets[k].write_row(row +2, 0, map(str,row_data))
    workbook.close()
    
def vcf_file_pair_generator(directory):
    """ Generator of pair objects from a paired directory. """
    from itertools import izip
    data = sorted([i for i in os.listdir(directory) if i.endswith('vcf')])
    if len(data) % 2 > 0:
	raise DirPairError("The number of VCF files (%i) in the directory is not pair.", len(data))
    for a,b in izip(*[iter(data)]*2):
	a = VCF_file(a,directory)
	b = VCF_file(b,directory)
	yield VCF_pair(a,b)


def main(args):
    data = dict([pair.data_dict for pair in vcf_file_pair_generator(args.in_dir)])
    outdir = args.out_dir if args.out_dir == False else os.path.join(args.in_dir,'results')
    if os.path.exists(outdir) == False:
	os.makedirs( outdir, 0755 )
    header = map(upper,['chromosome', 'position', 'reference', 'alternative', 'class_A', 'ref_cov_A', 'alt_cov_A', 'freq_A','class_B', 'ref_cov_B', 'alt_cov_B', 'freq_B'])
    outfile = os.path.join(outdir,'variants_intersection.xlsx')
    create_excel_file(data,outfile,header=header)
    print "VCF folder comparisson ended successfully."
    print "Results file path is: {of}".format(of=outfile)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description = 'VCF replicates directory.')
    parser.add_argument('-i','--in_dir', required = True, help = 'Replicates folder.')
    parser.add_argument('-o','--out_dir', required = False, help = 'Output folder.')
    args = parser.parse_args()
    main(args)
