#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import pybedtools
import pandas
import argparse
import multiprocessing
from numpy import cumsum
import numpy
from os.path import join,isdir
from matplotlib.colors import cnames
from matplotlib import pylab
from copy import copy
import random

    
def matplot_coverage(list_dfs,run_name,outdir=False,x_limit=1000):
    """ Plot coverage histogram and retuns img path and private plot url."""
    fig = pylab.figure()
    ax = fig.add_subplot(111)
    cols = copy(cnames)
    def simpleaxis(ax):
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
        ax.get_xaxis().tick_bottom()
        ax.get_yaxis().tick_left()
        ax.set_ylim([0,1])
        return ax
    
    ## format axes
    ax.set_ylabel('% covered')
    ax.set_xlabel('Depth of coverage')
    ax.set_title(run_name)
    ax = simpleaxis(ax)

    for df in list_dfs:
        col = random.choice(list(cnames))
        ccol = cnames[col]
        del(cnames[col])
        x=df[1]['depth'].tolist()
        y=df[1]['rcumsum'].tolist()
        line, = ax.plot(x, y, color=ccol, lw=2, label=df[0])

    params = {'legend.fontsize': 8,'legend.linewidth': 0.5}
    pylab.rcParams.update(params)
    pylab.legend(loc='upper right')
    fig.savefig(run_name + '.test.pdf')
    

def split_coverage(ifh):
    """ Takes a filename or a BedTool instance (ifh).\n Returns a bedtool and pandas dataframe that contains coverage histogram for panel targed regions.\n """
    if isinstance(ifh, basestring):
        fn = ifh
    else:
        fn = ifh.fn

    f = open(fn)
    hist_lines = []

    def gen():
        """
        Generator that yields only valid BED lines and then stops.
        The first "all" line is appended to hist_lines.
        """
        while True:
            line = f.next()
            toks = line.strip().split('\t')
            if toks[0] == 'all':
                hist_lines.append(toks[1:])	#exclude 'all string'
                break
            yield pybedtools.create_interval_from_list(toks)

    b = pybedtools.BedTool(gen()).saveas()

    while True:
        try:
            line = f.next()
        except StopIteration:
            break
        hist_lines.append(line.strip().split('\t')[1:])                                                                                                                             

    df = pandas.DataFrame(
        hist_lines,
        columns=['depth', 'count', 'size', 'percent'])
    return b, df


def get_hist(data):
    """ Returns histograme dataframe for a given sample. """
    sample,bam,bed = data
    bd = pybedtools.BedTool(bed)
    ba = pybedtools.BedTool(bam)
    coverage = ba.coverage(bd,hist=True)
    new_bedtool, dataframe = split_coverage(coverage)
    nrows = dataframe.shape[0]                                                                                                                                                                      
    sample_col = pandas.DataFrame([sample for i in range(nrows)],columns=['sample',])
    f = lambda x: [numpy.sum(map(float,x))-i for i in numpy.cumsum(map(float,x))]
    rcumsum = pandas.DataFrame(f(dataframe.percent),columns=['rcumsum',])
    dataframe = pandas.concat([dataframe,sample_col,rcumsum],axis=1)
    print "Sample {s} processed.".format(s=sample)
    return (sample,dataframe)


def read_bam_directory(directory,ext=False):
    """ Bam directory generator."""
    ext = ext if ext else '.bam'
    for aln in os.listdir(directory):
        if aln.endswith(ext):
            yield aln


def main(args):
    files = sorted([(bam.split('.')[0],os.path.join(args.in_dir,bam),args.bed) for bam in read_bam_directory(args.in_dir,ext=args.ext)])
    pool = multiprocessing.Pool(args.cores)
    dfs = [(p[0],p[1].convert_objects(convert_numeric=True)) for p in pool.imap_unordered(get_hist, files)]
    df = pandas.concat([d[1] for d in dfs],axis=0)
    df = df.sort(['depth',], ascending=[1,])
    df.to_csv(os.path.join(args.out_dir,"_".join([args.run_name, "cumsum.csv"])))
    matplot_coverage(dfs,args.run_name)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description = 'Coverage plot.')
    parser.add_argument('-i','--in_dir', required = True, help = 'Alignment folder.')
    parser.add_argument('-b','--bed', required = True, help = 'Bed file that contains the targeted region coordinates.')
    parser.add_argument('-e','--ext', required = False, default='rd.bam', help = 'Extension of files to use')
    parser.add_argument('-o','--out_dir', required = False, help = 'Output folder.')
    parser.add_argument('-r','--run_name', required = True, help = 'Run name.')
    parser.add_argument('-c','--cores', required = False, default=6, type=int, help = 'Number of cores.')
    args = parser.parse_args()
    main(args)
