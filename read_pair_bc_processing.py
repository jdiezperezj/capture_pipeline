#!/usr/bin/env python
import os
import re
import argparse
import gzip
import itertools
import operator
from collections import OrderedDict,Counter
import multiprocessing
from Bio import SeqIO
from Bio.Seq import Seq

def parse_fastq_name(f,directory):
    """ """
    n = f.split(".fastq.gz")[0]
    id,sample,lane,read,other = n.split("_")
    d = {"path":os.path.join(directory,f),"id":id,"sample":sample, "lane":lane, "read":read, "base":n, "filename":f}
    return d


def fastq_file_generator(fastq_folder):
    """ """
    for f in sorted(os.listdir(fastq_folder)):
        if f.endswith(".fastq.gz"):
            f = parse_fastq_name(f,fastq_folder)
            yield f


def fastq_pair_generator(fastq_folder):
    """ """
    fastq_iter = fastq_file_generator(fastq_folder)
    for i in fastq_iter:
        j = fastq_iter.next()
        if i['id'] == j['id'] and i['sample'] == j['sample'] and i['read'] != j['read']:
            name = "_".join([i['id'],i['sample']])
            yield {'id':i['id'],'sample':i['sample'],i['read']:i['path'],j['read']:j['path'],"R1_base":i["base"],"R2_base":j["base"],"name":name}


def read_fastq_pair(pair):
    """ """
    #print pair
    handle1 = gzip.open(pair['R1'], "r")
    handle2 = gzip.open(pair['R2'], "r")
    reads1 = SeqIO.parse(handle1, "fastq")
    reads2 = SeqIO.parse(handle2, "fastq")
    for read1, read2 in itertools.izip( reads1, reads2 ):
        yield (read1,read2)
    handle1.close()
    handle2.close()


def trim_bc(read,rn,modify="id"):
    """ """
    bc = str(read.seq[:6])
    bases = count_bases(bc)
    read = read[6:]
    if modify == "description":
        read.description = ":".join([read.description,bc])
    elif modify == "id":
        read.id = "#".join([read.id,bc]) + "/" + rn
    else:
        raise ValueError
    return read,bases


def reverse_complement(seq):
    """ """
    d={"A":"T","T":"A","C":"G","G":"C","N":"N"}
    r = "".join(reversed([d[i] for i in seq]))
    #print r
    return r


def hide_bc(read):
    """ """
    bc = str(read.seq[:6])
    bases = count_bases(bc)
    #read = read[6:]
    read.letter_annotations.values()[0][:6] = [0,] * 6
    r.description = " ".join([r.description,str(r.seq[:6])])
    return read,bases,bc


def hide_bc_rename_pairs(read1,read2):
    """ """
    bcp = "_".join([str(reverse_complement(str(read1.seq[:6]))),str(read2.seq[:6])])
    read1.id = "#".join([read1.id,bcp]) + "/1"
    read2.id = "#".join([read2.id,bcp]) + "/2"
    read1.letter_annotations.values()[0][:6] = [0,] * 6
    read2.letter_annotations.values()[0][:6] = [0,] * 6
    return (read1,read2,bcp)

def process_fastq_pair_files(p):
    """ """
    f,fastq_out = p
    p1 = os.path.join(fastq_out,f["R1_base"]+".trimmed.fa.gz")
    p2 = os.path.join(fastq_out,f["R2_base"]+".trimmed.fa.gz")
    ofh1 = gzip.open(p1, 'wb')
    ofh2 = gzip.open(p2, 'wb')
    n=0
    bcps=[]
    for rp1,rp2 in read_fastq_pair(f):
        n+=1
        read1,read2,bcp = hide_bc_rename_pairs(rp1,rp2)
        ofh1.write(read1.format("fastq"))
        ofh2.write(read2.format("fastq"))
        bcps += bcp.split("_")
    ofh1.close()
    ofh2.close()
    return (f['id'],n,bcps)  #Link to barcode statistics and return

def entropy(labels):
	freqdist = nltk.FreqDist(labels)
	probs = [freqdist.freq(l) for l in nltk.FreqDist(labels)]
	return -sum([p * math.log(p,2) for p in probs])


def main(args):
    print args
    files = [(f,args.fastq_out) for f in fastq_pair_generator(args.fastq_in) if f["sample"] != "S0"]
    pool = multiprocessing.Pool(args.cores)
    bcps = {}
    allv = []
    for p in pool.imap_unordered(process_fastq_pair_files,files):
    #for f in fastq_pair_generator(args.fastq_in):
    #    if f["sample"] != "S0":
    #        f = (f,args.fastq_out)
    #        p = process_fastq_pair_files(f)
        print "\t".join([p[0],str(p[1])])
        bcps[p[0]] = bcps
    #allbcs = [allv+=j for j in bcps.values()][0]
    #e = [entropy(i) for i in allbcs]
    #plot(sv)
    #get statistics from bcps -> get BC statistics per sample and at run level.
    #SE distribution of values.


if __name__ == '__main__':
    parser = argparse.ArgumentParser("")
    parser.add_argument('--fastq', dest='fastq_in', required=True, help='Fastq folder.')
    parser.add_argument('--fastq_out', dest='fastq_out', required=False, help='FASTQ out directory.')
    parser.add_argument('--cores', dest='cores', type=int, default=2, required=False, help='Number of cores to use')
    args = parser.parse_args()
    main(args)
