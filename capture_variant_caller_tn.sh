#SAMtools v0.1.17 (r973) and Later: Use mpileup
#As of SAMtools v0.1.17 (r973) and later, the pileup command is deprecated and has been replaced with mpileup to accommodate multi-sample calling. You can still generate a pileup file, but make sure you provide only a single BAM:

samtools mpileup -f [reference sequence] [BAM file] >myData.pileup


#Do NOT use any of the variant- or consensus-calling parameters. You just want the raw pileup output. This Perl snippet shows you how to pipe input from SAMtools into VarScan:

$normal_pileup = "samtools mpileup -f $reference $normal_bam";
$tumor_pileup = "samtools mpileup -f $reference $tumor_bam";


#To limit the pileup to reads with mapping quality > 0 (recommended), use this variation:

$normal_pileup = "samtools mpileup -q 1 -f $reference $normal_bam";
$tumor_pileup = "samtools mpileup -q 1 -f $reference $tumor_bam";


#Next, issue a system call that pipes input from these commands into VarScan :

bash -c \"java -jar VarScan.jar somatic <\($normal_pileup\) <\($tumor_pileup\) output \"
