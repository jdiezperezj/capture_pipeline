#!/usr/bin/env bash

#Scripts takes the following arguments:
#indir,outdir,amplicons,basename,read1,read2

args=("$@")

NOVOALIGN=/opt/novocraft/novoalign
NOVOSORT=/opt/novocraft/novosort
INDEX=/home/jdiezperez/data/references/hg19/hg19.ndx
SAMTOOLS=/opt/samtools-0.1.19/samtools

INSERT_SIZE="150,50"

OUTDIR=${args[0]}
AMPLICONS_FILE=${args[1]}

BASENAME=${args[2]}
READ1=${args[3]}
READ2=${args[4]}

ID=$BASENAME
LB="TrueSeq"
SM="hs"

NOVO_LOG="${OUTDIR}/${BASENAME}.log"
UNSORTED_BAM="${OUTDIR}/${BASENAME}.unsorted.bam"
SORTED_BAM="${OUTDIR}/${BASENAME}.bam"
SAM="${OUTDIR}/${BASENAME}.sam"

#novoalign
printf "${AMPLICONS_FILE}\n"
printf "${BASENAME}\n"
printf "${READ1}\n"
printf "${READ2}\n"
printf "${NOVO_LOG}\n"
printf "${UNSORTED_BAM}\n"
printf "${SORTED_BAM}\n"
printf "\n"

nohup $NOVOALIGN --amplicons ${AMPLICONS_FILE} -d  ${INDEX} -r Random -F ILM1.8 -f ${READ1} ${READ2} -i ${INSERT_SIZE} -o SAM $"@RG\\tID:${BASENAME}\\tPL:illumina\\tPU:illumina\\tLB:${LB}\\tSM:${SM}"  2> ${NOVO_LOG} 1> ${SAM} ;

java -jar /opt/picard-tools-1.115/SamFormatConverter.jar INPUT=${SAM} OUTPUT=${UNSORTED_BAM};

#novosort, sort and index.
nohup $NOVOSORT -i -f ${UNSORTED_BAM} -o ${SORTED_BAM} 2>> ${NOVO_LOG};

#keep only sorted
rm ${UNSORTED_BAM};
rm ${SAM};

