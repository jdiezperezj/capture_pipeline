#!/usr/bin/env python

import sys
import os
import csv
import pysam
from multiprocessing import Pool
import argparse
from pybedtools import BedTool
from collections import OrderedDict,Counter
import re
#import pandas
#import bcbio-nextgene
from Bio.Seq import Seq
from Bio.Alphabet import IUPAC


class Panel_Bam(object):
    """ """
    def __init__(self):
        self.panel
        self.panel_group
        self.panel_info
        self.sample_sheet


class Amplicon(object):
    """ """
    def __init__(self,pool):
        self.Pool = pool    #Either pool A or B 
        self.reads = {}
        self.unique_reads = {}
        self.BCs = []
    
    def get_pair(self,):
        pass
    
    def set_pair(self,readpair):
        """ """
        self.reads[readpair.read_name] = readpair
    
    def get_unique_pairs(self,):
        """ """
        for rp in self.reads.values():
            try:
                self.unique_reads[rp.get_ULSO_DLSO()] += rq.consensus
            except KeyError:
                self.unique_reads[rp.get_ULSO_DLSO()] = [rq.consensus,]
            else:
                pass
            finally:
                self.BCs.append(rp.get_ULSO_DLSO())
    
    def BC_counter(self,):
        return Counter(self.BCs)

    def __cmp__(self,other):
        #check other is class Amplicon
        if  isinstance(other,Amplicon) != True:
            raise TypeError("Object evaluated is not Amplicon object but {o}".format(o=other.__class__))
        #compare number of unique number of pairs.
        print "Number of pairs.\n Amplicon A: {a}\nAmplicon B: {b}\n".format(a=len(self.BCs),b=len(other.BCs))

class ReadPair(object):
    """ """
    def __init__(self,):
        self.read_name = False
        self.amplicon = False
        self.ULSO = False
        self.DLSO = False
        self.ULSO_BC = {}
        self.DLSO_BC = {}
        self.seq1 = False
        self.seq2 = False
        self.consensus = False
        
    def validate_amplicon_rp(self,):
        """ """
        #1)aligned, 2)belong to interval, 3)primers found ...
        pass
        
    def equal_seqs(self,):
        return True if (self.seq1 == self.seq2) and all(i != False for i in (self.seq1,self.seq2)) else False
    
    def set_consensus(self,):
        pass
        
    def get_ULSO_DLSO(self,):
        """ """
        return "_".join(self.ULSO,self.DLSO)
        
    def set_read1(self,read):
        """ """
        self.read1 = read
    
    def set_read2(self,):
        """ """
        self.read2 = read
        
    def set_keys(self,dictio,pair,seq):
        """ """
        if pair not in ('pair1','pair2'):
            raise ValueError("Only pair1 and pair2 key values are accepted. Provided key value: {p}".format(p=pair))
        else:
            if pair not in dictio.keys():
                dictio[pair] = seq
            else:
                raise(KeyError)
    
    def set_ULSO_BC(self,pair,seq):
        """ """
        self.set_keys(self.ULSO_BC,pair,seq)
    
    def set_DLSO_BC(self,pair,seq):
        """ """
        self.set_keys(self.DLSO_BC,pair,seq)   
        
    def check_bc_integrity(self,):
        """ """
        if self.ULSO_BC['pair1'] != self.ULSO_BC['pair2'] or self.DLSO_BC['pair1'] != self.DLSO_BC['pair2']:
            raise ValueError("ULSO_BC pair or DLSO_BC pair are not the same in the read pair.")


class SSObject(object):
    """ """
    def __init__(self,header,row):
        try:
            d = dict(zip(header,row))
        except Exception,e:
            print e
        else:
            self.__dict__.update(d)
    
    def __setattr__(self,key,value):
        self.__dict__[key] = value

    def __getattr__(self,key):
        return self.__dict__[key]
    
    def __repr__(self,):
        return "\t".join([":".join([i,j]) for i,j in self.__dict__.items()])

    
class Targets(object):
    """ """
    def __init__(self,objects):
        self.objects = objects
        #self.update_dict()
        self.__dict__.update(d)
        
    def update_dict(self,):
        for i in self.objects:
            self.__dict__.update(i.d)
    
    def __getattr__(self, key):
        return self.__dict__[key]
    
    def get_gene_probes(self,gene):
        return [self.objects[i] for i in self.objects.keys() if i.split('.')[0].startswith(gene)]
    
    def __repr__(self):
        return "\t".join(self.objects.values())
        
    
    
class Probes(Targets):
    """ """
    pass

class Intervals(Targets):
    pass


class Intervals(object):
    """ """
    def __init__(self,intervals):
        self.intervals = intervals
    
    def toBed(self,filename=False):
        """ """
        if filename:
            ofh = open(filename,'w')
            for i in self.intervals:
                ofh.write("\t".join([i.chrom,i.start,i.stop]))
            ofh.close()
        else:
            return "\n".join(["\t".join([i.chorm,i.start,i.stop] for i in self.intervals)])

    def toGff(self,):
        """ """
        pass

class SampleSheet(object):
    """ """
    def __init__(self,f):
        if os.path.isfile(f):
            self.f = os.path.abspath(str(f))
            self.process_sample_sheet()
        else:
            raise ValueError("The provided path doesn't corresponde to a file.")
    
    def __repr__(self, sl = ['Probes','Targets','Intervals']):
        for k in sl:
            print k
            print self.__dict__[k]
        
    def __getattr__(self,key,feats=False):
        """ """
        return self.__dict__[key]

    def __setattr__(self,key,value):
        self.__dict__[key] = value
    
    def get_data(self,rows,header):
        result={}
        header = [i.replace(" ","_") for i in header]
        for row in rows:
            result[row[0]] = SSObject(header,row)
        sorted(result,key=result.get)
        return result

    def get_block_id(self,row):
        """ """
        return row[0][1:-1]

    def process_block(self,rows):
        """ """
        name = self.get_block_id(rows[0])
        data = rows[1:]
        return(name,data)

    def process_sample_sheet(self,):
        """ """
        h=0
        csv.register_dialect('csv', delimiter='\t', quoting=csv.QUOTE_NONE)
        with open(self.f, 'r') as f:
            reader = csv.reader(f, dialect='csv')
            rows = [row for row in reader]
            delims=[i for i,j in enumerate(rows) if all(len(j)==0 for j in  rows[i])] + [len(rows),]
            for i in delims:
                name,data = self.process_block(rows[h:i])
                if name in ['Probes','Targets']:
                    self.__dict__[name] = self.get_data(data[1:], data[0])
                elif name == 'Intervals':
                    self.__dict__[name] = [k[2:5] for k in data[1:]  ]
                else:
                    raise NameError("Not identified field {n}".format(n=name))
                h = i+1

class RunSheet(SampleSheet):
    """ """
    
    def __repr__(self, sl=['Header','Manifests', 'Settings','Reads']):
        for k,v in self.__dict__.items():
            if k in sl:
                print k, v
            else:
                print k,v

    def get_data(self,rows,header):
        """ """
        result={}
        header = [i.replace(" ","_") for i in header]
        for i,row in enumerate(rows):
            #result[row[0]] = SSObject(header + ["SID",], row + ["S{i}".format(i=str(i)),])
            result[row[0]] = SSObject(header,row)
        sorted(result,key=result.get)
        return result

    def get_others(self,rows):
        """ """
        results = {}
        for row in rows:
            results[row[0]] = row[1]
        return results
    
    def get_reads(self,rows):
        """ """
        return sorted([int(row[0]) for row in rows])
        
    def process_sample_sheet(self,):
        h=0
        csv.register_dialect('csv', delimiter=',', quoting=csv.QUOTE_NONE)
        with open(self.f, 'r') as f:
            reader = csv.reader(f, dialect='csv')
            rows = [row for row in reader]
            delims=[i for i,j in enumerate(rows) if all(len(j)==0 for j in  rows[i])] + [len(rows),]
            for i in delims:
                name,data = self.process_block(rows[h:i])
                if name in ['Header','Manifests', 'Settings']:
                    self.__dict__[name] = self.get_others(data)
                elif name == 'Reads':
                    self.__dict__[name] = self.get_reads(data)
                elif name == 'Data':
                    self.__dict__[name] = self.get_data(data[1:], data[0])
                else:
                    raise NameError("Not identified field {n}".format(n=name))
                h = i+1
    def get_data_records(self):
        for i in sorted(self.Data):
            yield i
            
    def get_data_pairs(self):
        gen = self.get_data_records()
        for i in gen:
            j = gen.next()
            if i == j:
                yield(self.Data[i],self.Data[j])


def get_unique_reads(reads):
    """ Get unique reads from a groupd of sequences mapping in a given region.
        from collections import Counter
        dreads1 = Counter([i.seq[:6] for i in reads if i.is_read1])
        dreads2 = Counter([i.seq[-6:] for i in reads if i.is_read2]) """
    d1 = {}
    d2 = {}
    for r in reads:
        k = r.seq[:6]
        s = '-' if r.is_reverse() == True else '+'
        if r.is_read1:
            if k not in d1.keys():
                d1[k] = {'seq':r.seq, 'strand':s, 'count':1}
            else:
                d1[k]['count'] += 1
        if r.is_read2:
            if k not in d2.keys():
                d2[k] = {'seq':r.seq, 'strand':s, 'count':1}
            else:
                d2[k]['count'] += 1
    return d1,d2


def parse_manifest(filename):
    """ Create a SampleSheet object tha contains instances of Targets, Probes and Intervals"""
    pass

def parse_bed(bedfile):
    b = BedTool(bedfile)
    for i in b:
        yield i
    

def parse_intervals(intervals):
    """ Intervals is a long string with multiple lines of intervals. """
    fromscratch = pybedtools.BedTool(intervals, from_string=True)
    return fromscratch

def write_to_bam(bam_ofh,reads):
    for r in reads:
        bam_ofh.write(r)

def get_pairs(iterator):
    """ Return read pairs. """
    reads1 = []
    reads2 = []
    for i in iterator:
        if i.is_read1 == True:
            reads1.append(i)
        else:
            reads2.append(i)
    #print sorted(reads1, key=lambda read: read.qname)
    #print sorted(reads1, key=lambda read: read.qname)
    for i in zip(reads1,reads2):
        if i[0].qname == i[1].qname:
            yield i[0],i[1]
    

def get_primers_location(ULSO,DLSO,read):
    """ Return a tuple containing location coordinates (first base matching) of specific primers in read sequence."""
    ulso_m = [(m.start(0), m.end(0)) for m in re.finditer(str(ULSO),str(read.seq))]
    dlso_m = [(m.start(0), m.end(0)) for m in re.finditer(str(DLSO),str(read.seq))]
    if len(ulso_m) > 1:
        raise ValueError("More than 1 hit found for ULSO sequence {u} in read {r} ".format(u=ULSO, r=read.seq))
    
    if len(dlso_m) > 1:
        raise ValueError("More than 1 hit found for DLSO sequence {u} in read {r} ".format(u=DLSO, r=read.seq))
    
    if all(len(i) > 0 for i in [ulso_m , dlso_m]):
        return ulso_m[0],dlso_m[0]
    
    else:
        return False,False


def get_pair1(ULSO,DLSO,read,strand):
    """ """
    ulso_m,dlso_m = get_primers_location(ULSO,DLSO,read)
    print ulso_m
    print dlso_m
    if dlso_m and ulso_m :
        if strand == "+":
            if dlso_m[1] < len(read.seq) and dlso_m[0] > ulso_m[1]:
                bc = pair1.seq[dlso_m[1]:dlso_m[1] + 6]
                if len(bc) == 6:
                    pair1_bc = bc
                else:
                    pair1_bc = False
            else:
                pair1_bc = False
        elif strand == "-":
            if dlso_m[0] > 6 and dlso_m[1] < ulso_m[0]:
                bc = pair1.seq[dlso_m[0] - 6 :dlso_m[0]]
                if len(bc) == 6:
                    pair1_bc = bc
                else:
                    pair1_bc = False
                
            else:
                pair1_bc = False
    else:
        pair1_bc = False
    
    return pair1_bc
        
def get_pair2(ULSO,DLSO,read,strand):
    """ """
    ulso_m,dlso_m = get_primers_location(ULSO,DLSO,read)
    print ulso_m
    print dlso_m
    if dlso_m and ulso_m:
        if strand == "+":
            if ulso_m[0] > 6 and ulso_m[1] < dlso_m[0]:
                bc = pair2.seq[ulso_m[0] - 6 :ulso_m[0]]
                if len(bc) == 6:
                    pair2_bc = bc
                else:
                    pair2_bc = False
            else:
                pair2_bc = False
        elif strand == "-":
            if ulso_m[1] < len(read.seq) and ulso_m[0] > dlso_m[1]:
                bc = pair2.seq[ulso_m[1] :ulso_m[1] + 6]
                if len(bc) == 6:
                    pair2_bc = bc
                else:
                    pair2_bc = False
            else:
                pair2_bc = False
    else:
        pair2_bc = False
    
    return pair2_bc


def process_bam(bam,SS):
    """ """
    pass


def get_regions(bam,SS):
    """ Get region from bam file."""
    regions = SS.Probes
    targets = SS.Targets
    print len(targets)
    print targets.keys()
    
    with pysam.Samfile( bam, "rb" ) as samfile:
        for region in sorted(regions.keys()):
            target =  targets[region]
            region = regions[region]
            
            print "\n"
            print "Bam Name: ", bam.split("/")[-1]
            print "Region ID: ",region.Target_ID
            print target.Chromosome, int(target.Start_Position), int(target.End_Position)
            print "ULSO: ",region.ULSO_Sequence
            print "DLSO: ",region.DLSO_Sequence
            print "Strand: ", target.Probe_Strand
            print "Target Sequence:", target.Sequence
            
            
            ULSO = Seq(region.ULSO_Sequence, IUPAC.IUPACUnambiguousDNA())
            DLSO = Seq(region.DLSO_Sequence, IUPAC.IUPACUnambiguousDNA())
            if region.Strand == "-":
                DLSO = str(DLSO.reverse_complement())
                ULSO = str(ULSO.reverse_complement())
            else:
                ULSO = str(region.ULSO_Sequence)
                DLSO = str(region.DLSO_Sequence)
                
            reads = samfile.fetch( region.Chromosome, int(target.Start_Position), int(target.End_Position))

            bcs = []

            for i,j in get_pairs(reads):
                #check positions of every read alignment in the interval
                tags = dict(i.tags)
                print "i",i.qname, i.is_unmapped, i.mate_is_unmapped, "j",j.qname, j.is_unmapped, j.mate_is_unmapped
                #print i
                if 'XN' in tags.keys():
                    print tags['XN']
                print len(i.seq)
                print "read1: ", i.seq
                
                pair1_bc = get_pair1(ULSO,DLSO,i,target.Probe_Strand)
                pair2_bc = get_pair2(ULSO,DLSO,j,target.Probe_Strand)

                bc_pair = [pair1_bc,pair2_bc]
                print bc_pair
                if all(k != False for k in bc_pair):
                    bcs.append("_".join(bc_pair))

            counts = Counter(bcs)
            print "N barcodes: ", len(bcs)
            print "BCs frequency: ", len(counts)
            print "BCs Details"
            print counts
    

def is_valid_file(parser, arg):
    if not os.path.exists(arg):
       parser.error("The file {arg} does not exist!".format(arg=arg))
    else:
       return open(arg,'r')
    
def parse_bam_name(b,directory):
    """ """
    n = b.split(".bam")[0]
    id,sample = n.split("_")
    d = {"path":os.path.join(directory,b),"id":id,"sample":sample}
    
    return d

def select_bams(directory):
    """ """
    for b in sorted(os.listdir(directory)):
        if b.endswith(".bam"):
            bam = parse_bam_name(b,directory)
            yield bam


def main(args):
    if args.manifestA :
        pass
    
    bams = {}
    bam_iter = select_bams(args.bamdir)
    #aligned_pairs
    for i in bam_iter:
        print i
        j = bam_iter.next()
        print j
        if i['id'] == j['id']:
            pairs = {i['sample']:i['path'],j['sample']:j['path']}
            pairs = sorted(pairs.items(), key=lambda t: t[0])
            bams[i['id']] = pairs
    
    print bams

    A = SampleSheet(args.manifestA)
    print A.Targets
    B = SampleSheet(args.manifestB)
    print B.Targets
    
    manifests = {'A':A,'B':B}
    
    R = RunSheet(args.run_sample_sheet)
    print ""
    for i in sorted(R.Data):
        if i.endswith('-D'):
            print i, i[:-2]
            print bams[i[:-2]][1]
            print R.Data[i].Manifest
            #get_regions(...)
        else:
            print i
            print bams[i][0]
            print R.Data[i].Manifest
            #get_regions(...)

    

if __name__ == '__main__':
    parser = argparse.ArgumentParser("")
    parser.add_argument('--bam', dest='bamdir', required=True, help='BAM folder.')
    parser.add_argument('--manA', dest='manifestA', required=True, help='Manifest file A.')
    parser.add_argument('--manB', dest='manifestB', required=False, help='Manifest file B.')
    parser.add_argument('--run', dest='run_sample_sheet', required=False, help='Run Sample Sheet.')
    parser.add_argument('--bam_out', dest='bam_out_dir', required=False, help='BAM out directory.')
    parser.add_argument('--vcf_out_directory', dest='vcf_out_dir', required=False, help='VCF out directory.')
    parser.add_argument('--cores', dest='cores', required=False, help='Number of cores to use', type=int)
    args = parser.parse_args()
    main(args)