#!/usr/bin/env bash

#Scripts takes the following arguments:
#outdir,sample_name,index,read1,read2,bedfile

args=("$@")
#echo ${args[@]}

NOVOALIGN=/opt/novocraft/novoalign
NOVOSORT=/opt/novocraft/novosort
#INDEX=/home/jdiezperez/data/references/hg19/hg19.ndx
#INDEX=/home/jdiezperez/data/references/ensembl56/HS.GRCh37.56.ndx
SAMTOOLS=/opt/samtools-0.1.19/samtools

INSERT_SIZE="200,50"

OUTDIR=${args[0]}
BASENAME=${args[1]}
INDEX=${args[2]}
READ1=${args[3]}
READ2=${args[4]}
BED=${args[5]}

LB="TrueSeqCap"
SM="hs"


NOVO_LOG="${OUTDIR}/${BASENAME}.log"
SAM="${OUTDIR}/${BASENAME}.sam"
UNSORTED_BAM="${OUTDIR}/${BASENAME}.unsorted.bam"
SORTED_BAM="${OUTDIR}/${BASENAME}.md.bam"
SORTED_BAM_LOG="${OUTDIR}/${BASENAME}.md.log"
SORTED_BAM_RD="${OUTDIR}/${BASENAME}.rd.bam"
SORTED_BAM_RD_LOG="${OUTDIR}/${BASENAME}.rd.log"


#novoalign
printf "${BASENAME}\n"
printf "${READ1}\n"
printf "${READ2}\n"
printf "${NOVO_LOG}\n"
printf "${UNSORTED_BAM}\n"
printf "${SORTED_BAM}\n"
printf "\n"

nohup $NOVOALIGN -d  ${INDEX} -r Random -F ILM1.8 -f ${READ1} ${READ2} -i ${INSERT_SIZE} -o SAM $"@RG\tID:${BASENAME}\tPL:illumina\tPU:illumina\tLB:${LB}\tSM:${SM}"  2> ${NOVO_LOG} 1> ${SAM} ;

#SAM to BAM format conversion.
#java -jar /opt/picard-tools-1.115/SamFormatConverter.jar INPUT=${SAM} OUTPUT=${UNSORTED_BAM} ;
samtools view -b -S ${SAM} > ${UNSORTED_BAM};

#novosort, sort and index.
nohup $NOVOSORT --md -i -f ${UNSORTED_BAM} -o ${SORTED_BAM} 2> ${SORTED_BAM_LOG};
nohup $NOVOSORT --rd -i -f ${UNSORTED_BAM} -o ${SORTED_BAM_RD} 2> ${SORTED_BAM_RD_LOG};

#CollectInsertSize
#java -jar /opt/picard-tools-1.115/CollectInsertSizeMetrics.jar I=${SORTED_BAM} O=${SIZE_METRICS} H=${SIZE_METRICS_PLOT};

#keep only sorted
rm ${SAM};
rm ${UNSORTED_BAM};

