#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import argparse
import multiprocessing
import pysam
from collections import Counter
from pandas import DataFrame, concat
from math import fabs

def reverse_readline(filename, buf_size=8192, linesep=os.linesep,fieldsep=False):
    """A generator that returns the lines of a file in reverse order. """
    with open(filename) as fh:
        segment = None
        offset = 0
        fh.seek(0, os.SEEK_END)
        total_size = remaining_size = fh.tell()
        while remaining_size > 0:
            offset = min(total_size, offset + buf_size)
            fh.seek(-offset, os.SEEK_END)
            buffer = fh.read(min(remaining_size, buf_size))
            remaining_size -= buf_size
            lines = buffer.split(linesep)
            # the first line of the buffer is probably not a complete line so
            # we'll save it and append it to the last line of the next buffer
            # we read
            if segment is not None:
                lines[-1] += segment
            segment = lines[0]
            for index in range(len(lines) - 1, 0, -1):
                if fieldsep != False:
                    yield lines[index].split(fieldsep)
                else:
                    yield lines[index]
        yield segment

def counter_to_df(counter,header):
    """ """
    df = DataFrame([[k,v] for k,v in counter.items()],columns=header)
    return df


def get_insert_size(data):
    """ """
    tlens = []
    sample,bamfile = data
    samfile = pysam.Samfile( bamfile, "rb" )
    for read in samfile.fetch():
        if read.is_unmapped != True and read.is_proper_pair:
            tlens.append(fabs(read.tlen))
    samfile.close()
    freqs = Counter(tlens)
    df = counter_to_df(freqs,['isize','feq'])
    nrows = df.shape[0]
    sample_col = DataFrame([sample for i in range(nrows)],columns=['sample',])
    df = concat([df,sample_col],axis=1)
    df = df.sort(['isize',], ascending=[1,])
    print df
    return df

def read_bam_directory(directory,ext=False):
    for aln in os.listdir(directory):
        if ext != False:
            if aln.endswith(ext):
                yield aln
        else:
            if aln.endswith('.bam'):
                yield aln

def main(args):
    files = sorted([(bam.split('.')[0],os.path.join(args.in_dir,bam)) for bam in read_bam_directory(args.in_dir,ext=args.ext)])
    print files
    pool = multiprocessing.Pool(args.cores)
    dfs = [p.convert_objects(convert_numeric=True) for p in pool.imap_unordered(get_insert_size, files)]
    df = concat(dfs,axis=0)
    #Use the list of dataframes to get a trace list to pl.
    df = df.sort(['isize',], ascending=[1,])
    df.to_csv(os.path.join(args.out_dir,"_".join([args.run_name, "InsertSize.csv"])))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description = 'Insert size calculation and plotting.')
    parser.add_argument('-i','--in_dir', required = True, help = 'Alignment folder.')
    parser.add_argument('-e','--ext', required = False, default='md.bam', help = 'Extension of files to use')
    parser.add_argument('-o','--out_dir', required = False, help = 'Output folder.')
    parser.add_argument('-r','--run_name', required = True, help = 'Run name.')
    parser.add_argument('-c','--cores', required = False, default=6, type=int, help = 'Number of cores.')
    args = parser.parse_args()
    main(args)

