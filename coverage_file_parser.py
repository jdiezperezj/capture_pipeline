#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import pandas
from fileinput import FileInput
from string import upper
import os


def replace_eol(filename,exclude=False):
    """ """
    for line in FileInput(filename, inplace=1):
        line=line.replace('\r\n', '\n')
        line=line.replace('\t\n', '\n')
        line=line.replace('&', '_')
        if exclude:
            if any(line.startswith(i) for i in exclude):
                pass
            else:
                print line,
        else:
            print line,


def create_excel_file(data,gene_header,outfile,header=False):
    """ Create a multitab excel file from a dictionary of elements. """
    from xlsxwriter import Workbook as wb
    workbook = wb(outfile)
    worksheet=workbook.add_worksheet('Exons Failling')
    for k in data:
	if header != False:
	    worksheet.write_row(1, 0, header)
            worksheet.write_row(2, 0, gene_header)
	for row, row_data in enumerate(data):
	    worksheet.write_row(row +3, 0, map(str,row_data))
    workbook.close()


def exon_numbering(exon):
    n = "ex" + format(int(exon.split('ex')[-1]), '02d')
    return n

def gene_exons(genes,exons):
    res=[]
    n = 0
    for g in genes:
        l =[]
        for e in exons:
            if e.startswith(g):
                e = e.split("_")[1].split('&')
                l += e
        l = sorted(set(l))
        n += len(l)
        res.append(", ".join(l))
    return res + [str(n) + ', ' + "{0:.2f}".format((n/float(25))*100),]


def main(args):
    gene_list = args.gene_list.split(',') if args.gene_list != False else False
    exclude = args.exclude_list.split(',') if args.exclude_list != False else False
    replace_eol(args.in_file,exclude=exclude)
    t = pandas.DataFrame.from_csv(args.in_file,sep='\t')
    t.rename(columns=lambda x: x.split('-')[0], inplace=True)
    names=[i for i in sorted(set(t.columns)) if i not in  ['ACD1','NTC']]
    l=[(i,t[[i]].min(axis=1)) for i in names]
    res=pandas.DataFrame(index=l[0][1].index,columns=names) 
    for i in l:
        res[i[0]]= i[1].values
    
    #mi=[i.split('_',2) for i in t.index.tolist()]
    mi=[]
    for i in t.index.tolist():
        i = i.split('_')
        gene = i[0]
        exons = [exon_numbering(j) for j in i[1:-2]]
        exons = "&".join(exons)
        #print exons
        tile = "_".join(i[-2:])
        mi.append([gene,exons,tile])
        
    mix=pandas.DataFrame(data=mi,columns=["gene","exon","tile"])
    
    
    df_multi = pandas.DataFrame(data= res.values, index = [list(mix.gene.values),list(mix.exon.values),list(mix.tile.values)], columns = list(res.columns.values))
    df_multi.index.names =['gene','exon','tile']
    min_exon = df_multi.min(level=[0,1])
    
    #Get boolean matrix
    df_bool = min_exon.applymap(lambda x: x<150)
    
    #Get records
    b=df_bool[[1]].to_records()
    
    #Empty dict to fill in with results
    d = {}
    
    for i in sorted(df_bool.columns.get_values()):
        d[i] = []
        for j in df_bool[[i]].to_records():
            if j[2] == True:
                if gene_list != False:
                    if j[0] in gene_list:
                        v = "_".join(list(j)[:2])
                        d[i].append(v)
                else:
                    v = "_".join(list(j)[:2])
                    d[i].append(v)
    
    
    #data = [[k,", ".join(d[k])] for k in sorted(d.keys())]
    data = [[k] + gene_exons(gene_list,d[k]) for k in sorted(d.keys())]
       
    head,tail=os.path.split(args.in_file)
    outdir = os.path.join(head,'results')
    
    if os.path.exists(outdir) == False:
	os.makedirs( outdir, 0755 )
        
    header = ['SAMPLE','EXONS_FAILLING']
    print gene_list
    gene_header = ["",] + gene_list + ["PC failing",]
    print gene_header
    outfile = os.path.join(outdir,'exons_failling.xlsx')
    
    create_excel_file(data,gene_header,outfile,header=header)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description = 'Coverage parser')
    parser.add_argument('-i','--in_file', required = True, help = 'Coverage file.')
    parser.add_argument('-g','--gene_list', required = False, default=False, help = 'Genes of interest list.')
    parser.add_argument('-e','--exclude_list', required = False, default=False, help = 'Exons or tiles to exclude.')
    args = parser.parse_args()
    main(args)
