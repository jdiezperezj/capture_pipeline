#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import argparse
import vcf
from multiprocessing import Popen, PIPE
from pandas import DataFrame

# Bash command to concatenate, sort 
# vcf-concat RM3994001-2_S1.del.vcf RM3994001-2_S1.dup.vcf RM3994001-2_S1.inv.vcf RM3994001-2_S1.tra.vcf | vcf-sort | bgzip  > RM3994001-2_S1.sv.vcf.gz
# Tabix index of zipped vcf.
# tabix -p vcf RM3994001-2_S1.sv.vcf.gz


class CNVkit(object):
    """ Wrapper: http://cnvkit.readthedocs.org"""
    def __init__(self):
        """ """
        self.bam_dir = False
        self.bam_file = False
        self.tiled_bed = False
        self.background_bed = False
        self.reference_cnn = False
        self.cores = False
        self.output_dir = False
        self.normal_bam_suffix = False  #'*Normal.bam'
        self.tumor_bam_suffix = False
        self.genome_reference = False
        self.targets_file = False
        self.flatFile = False
        self.genome_regions = False
        self.reference_cnn = False
        #Implement as OrderedDict: provide all in pairs but for script name itself.
    
    def pop(self,cmd):
        """ """
        p = Popen(cmd, stderr=PIPE, stdout=PIPE)
        res = p.communicate()
        return res
    
    def run_batch_multiple(self, ):
        """ """
        #cnvkit.py *.bam --output-dir CNVs/ -t Tiled.bed -a Background.bed -r Reference.cnn -p X    #multiple samples multicore
        cmd = ['cnvkit.py', os.path.join(self.bam_dir ,'*.bam'), '--output-dir', self.output_dir, '-t', self.tiled_bed, '-a', self.background_bed, '-r', self.reference_cnn, '-p',  self.cores]
        return self.pop(cmd)
    
    def run_batch_sample(self, ):
        """ """
        #cnvkit.py Sample.bam -t Tiled.bed -a Background.bed -r Reference.cnn   #One sample
        cmd = ['cnvkit.py', self.bam_file, '--output-dir', self.output_dir, '-t', self.tiled_bed, '-a', self.background_bed, '-r', self.reference_cnn]
        return self.pop(cmd)
    
    def run_batch_tumor_normal(self,):
        #cnvkit.py batch *Tumor.bam --normal *Normal.bam  --targets my_targets.bed --fasta hg19.fasta  --annotate refFlat.txt --split --regions data/access-10kb.hg19.bed  --output-reference my_reference.cnn --output-dir example
        cmd = ['cnvkit.py', 'batch', self.tumor_bam_suffix,  '--normal',  self.normal_bam_suffix,  '--targets',  self.targets_file,  '--fasta',  self.genome_reference,  '--annotate',  self.flatFile , '--split',  '--regions', self.genome_regions,  '--output-reference', self.reference_cnn,  '--output-dir', self.output_dir]
        return self.pop(cmd)
    
    def run_batch_tumor_only(self,):
        #cnvkit.py batch *Tumor.bam --normal *Normal.bam  --targets my_targets.bed --fasta hg19.fasta  --annotate refFlat.txt --split --regions data/access-10kb.hg19.bed  --output-reference my_reference.cnn --output-dir example
        cmd = ['cnvkit.py', 'batch', self.tumor_bam_suffix,  '--normal',  '--targets',  self.targets_file,  '--fasta',  self.genome_reference,  '--annotate',  self.flatFile , '--split',  '--regions', self.genome_regions,  '--output-reference', self.reference_cnn,  '--output-dir', self.output_dir]
        return self.pop(cmd)
    
    def run_batch_tumor_only_reuse(self,):
        #cnvkit.py batch *Tumor.bam --normal *Normal.bam  --targets my_targets.bed --fasta hg19.fasta  --annotate refFlat.txt --split --regions data/access-10kb.hg19.bed  --output-reference my_reference.cnn --output-dir example
        cmd = ['cnvkit.py', 'batch', self.tumor_bam_suffix,  '-r',  'my_reference.cnn',  '-p',  '0',  '--scatter', '--diagram', '-d',  self.output_dir]
        return self.pop(cmd)

    def get_reports(self,):
        pass
    
    def get_graphics(self):
        pass
    
    

class Lumpy(object):
    """ """
    def __init__(self, bam, stype='orginal'):
        if stype == 'original':
            self.original_bam= bam
            self.pesr_bam = False
        elif stype == 'pesr':
            self.pesr_bam = bam
            self.original_bam = False
        else:
            raise("Unrecognized bam type.")
        self.pe_bam = False
        self.sr_bam = False
        self.histo = False
        self.pesr_bedp = False
        
        
    def get_split_reads(self,):
        """ """
        pass
    
    def get_pair_end(self,):
        """ """
        pass
    
    def get_pe_from_pesr(self, ):
        """ Flag Description
        0x0001 Paired in sequencing
        0x0002 Mapped in proper read pair
        0x0004 Query unmapped
        0x0008 Mate unmapped
        0x0010 Strand of the query (1 for reverse)
        0x0020 Strand of the mate (1 for reverse)
        0x0040 First read in the pair
        0x0080 Second read in the pair
        0x0100 Secondary alignment
        0x0200 QC failure
        0x0400 Optical or PCR duplicates
        http://ppotato.wordpress.com/2010/08/25/samtool-bitwise-flag-paired-reads/
        """
        assert (self.pesr_bam != False and self.pe_bam == False)
        with open(self.pe_bam, 'wb') as ofh:
            cmd1 = ['samtools', 'view', '-u', '-F', '0x0002', self.pesr_bam]    #The read is mapped in a proper pair
            p1 = Popen(cmd1, stderr=PIPE, stdout=PIPE)
            cmd2 = ['samtools', 'view', '-u', '-F', '0x0100', '-' ] #If there are multiple alignments for the read, you can use this bit to mark one of the alignments as primary
            p2 = Popen(cmd2, stdin=p1.stdout, stderr=PIPE, stdout=PIPE)
            cmd3 = ['samtools', 'view', '-u', '-F', '0x0004', '-' ] #the 4 flag is set when the read is unmapped
            p3 = Popen(cmd3, stdin=p2.stdout, stderr=PIPE, stdout=PIPE)
            cmd4 = ['samtools', 'view', '-u', '-F', '0x0008', '-' ] #means that the mate is not mapped for ANY record
            p4 = Popen(cmd4, stdin=p3.stdout, stderr=PIPE, stdout=PIPE)
            cmd5 = ['samtools', 'view', '-u', '-F', '0x0400', '-' ] #0x0400 Optical or PCR duplicates -> we can omit this one if rd.bam is used.
            p5 = Popen(cmd5, stdin=p4.stdout, stderr=PIPE, stdout=ofh)
            res = p5.communicate()
        return res


    def get_sr_from_pesr(self, ):
        """ """
        assert (self.pesr_bam != False and self.sr_bam == False)
        with open(self.sr_bam, 'wb') as ofh:
            cmd1 = ['samtools', 'view', '-h', self.pesr_bam]    #Return samfile with header.
            p1 = Popen(cmd1, stderr=PIPE, stdout=PIPE)
            cmd2 = ['extractSplitReads_BwaMem', '-i', 'stdin' ] #lumpy script must be in the path and be executable.
            p2 = Popen(cmd2, stdin=p1.stdout, stderr=PIPE, stdout=PIPE)
            cmd3 = ['samtools', '-Sb' , '-']    #outputs binary file.
            p3 = Popen(cmd3, stdin=p2.stdout, stderr=PIPE, stdout=ofh)
            res = p3.communicate()
        return res

    
    def get_pairend_distro(self,):
        """ """
        cmd1 = ['samtools', 'view', '-h', self.pesr_bam]
        p1 = Popen(cmd1, stderr=PIPE, stdout=PIPE)
        cmd2 = ['tail', '-n+100000']
        p2 = Popen(cmd2, stdin=p1.stdout, stderr=PIPE, stdout=PIPE)
        cmd3 = ['parirend_distro.py', '-r', '150', '-X', '4', '-N', '10000', '-o', self.histo]
        p3 = Popen(cmd3, stdin=p2.stdout, stderr=PIPE, stdout=PIPE)
        res = p3.communicate()
        return res

    
    def test_yaha(self,):
        """ """
        pass
    
    def apply_yaha(self,):
        """ """
        pass
    
    def test_bwasw(self,):
        """ """
        pass
    
    def apply_bwasw(self,):
        """ """
        pass
    
    def apply_lumpy_sr(self,):
        """ """
        pass
    
    def apply_lumpy_pe(self,):
        """ """
        pass
    
    def apply_lumpy_pe_matched(self,):
        """ """
        pass
    
    def apply_lumpy_pe_sr(self,bam):
        """ """
        pe = "bam_file:{bf},histo_file:{hf}stdev:{std},read_lenght:{rl},min_non_overlap:{mno},discodant_z:{dz},back_distance:{bd},weight:{w},id:{id},min_mapping_threshold:{mmt}".format(bf=bam, hf=hf, std=std, rl=rl, mno=mno, dz=dz, bd=bd, id=id, w=w, mmt=mmt)
        sr = "bam_file:{bf},back_distance:{bd}weight:{w},id:{id}, min_mapping_threshold:{mmt}".format(bf=bam, bd=bd, w=w, id=id, mmt=mmt)
        cmd = ['lumpy', '-mw', '4', '-tt', '0.0', '-pe', pe, '-sr', sr]
        with open(self.pesr_bedp, 'w') as pesr:
            p = Popen(cmd, stderr=PIPE, stdout=pesr)
            res = p.communicate()
        return res
    
    def get_high_coverage_regions(self,):
        """ """
        pass
    
    def exclude_high_coverage_regions(self, ):
        """ """
        pass


class Delly(object):
    """ """
    def __init__(self, bams,genome,**kwargs):
        self.bams = bams    #iterator or generator of deduplicated bams.
        self.genome = genome    #path to genome fasta file.
        self.kwargs = kwargs    #delly arguments and other options.
        self.types = ['TRA','INV','DEL','DUP']
        self.outdir #results directory
        self.runname    #run name
        self.merged_vcf = os.path.join(self.outdir, ".".join([self.runname,'delly.vcf.gz']))
    
    def __add__(self,):
        pass
    
    def __call__(self,):
        pass
    
    def bam_to_str(self,):
        return " ".join(sorted([b for b in bams]))
    
    def bams_to_list(self,):
        """ """
        return [b for b in bams]
    
    def get_outfile_name(self,stype):
        """ """
        name = "_".join([runname,stype + ".delly.vcf"])
        return os.path.join(outdir,name)

    def get_vcf_list(self,):
        return [self.get_outfile_name(i) for i in self.type]

    
    def get_delly_command(self, stype):
        cmd = ['delly', '-t', stype, '-q', '10', '-g', self.genome , '-o', self.get_outfile_name(stype)] + self.bams_to_list()
        return cmd

    def call_delly(self,stype):
        """ """
        p = Popen(get_delly_command(stype), stderr=PIPE, stdout=PIPE)
        res = p.comunicate()    #send to log
        
    def call_all_delly(self):
        """ """
        for stype in self.types:
            self.call_delly(stype)
    
    def vcf_concat(self,):
        """ """
        try:
            cmd = ['vcf-concat ',]  + self.get_vcf_list() 
            p1 = Popen(cmd, stderr=PIPE, stdout=PIPE)
            
        except Exception,e:
            pass #print exception with proper step
        
        else:
            try:
                cmd = ' vcf-sort '
                p2 = Popen(cmd, stdin=p1.stdout, stderr=PIPE, stdout=PIPE)
            except Exception,e:
                pass    #print proper exception
            else:
                try:
                    cmd = 'bgzip '
                    with open(self.merged_vcf,'wb') as ofh:
                        p3 = Popen(cmd, stdin=p2.stdout, stderr=PIPE, stdout=self.merged_vcf)
                except Exception,e:
                    pass#print proper exception.
                else:
                    res = p3.comunicate()


    def tabix_indexing(self,):
        """ """
        cmd = ['tabix', '-p', 'vcf', self.merged_vcf ]
        p = Popen(cmd, stderr=PIPE, stdout=PIPE)
        res = p.comunicate()

    def full_pipe(self,):
        """ """
        self.call_all_delly()
        self.vcf_concat()
        self.tabix_indexing()

    def get_all_df(self, ):
        """ Returns a dataframe. """
        return multiple_vcf_parser(vcf_file)
        


def parse_translocation(records):
    """ """
    columns = ['CHR1','POS1','CHR2','POS2','TYPE','SUBTYPE','CT','MAPQ','PE','FILTER']    #FILTER=0 stands for failed to pass 'LowQual' filter, 1 = 'LowQual' filter passed.
    df = DataFrame([[r.CHROM, r.POS, r.INFO['CHR2'], r.INFO['END'], r.var_type, r.var_subtype, r.INFO['CT'], r.INFO['MAPQ'], r.INFO['PE'], (len(r.FILTER)*-1)+1] for r in records], columns = columns)
    #convert to numeric types
    # sort by chrom and position
    return df

def parse_inversion(records):
    """ """
    columns = ['CHR1','START','END','TYPE','SUBTYPE','CT','MAPQ','PE','FILTER']   #FILTER=0 stands for failed to pass 'LowQual' filter, 1 = 'LowQual' filter passed.
    df = DataFrame([[r.CHROM,r.POS,r.INFO['END'],r.var_type,r.var_subtype,r.INFO['CT'],r.INFO['MAPQ'],r.INFO['PE'], (len(r.FILTER)*-1)+1] for r in records], columns = columns)
    return df

def parse_deletion(records):
    """ """
    columns = ['CHR1','START','END','TYPE','SUBTYPE','CT','MAPQ','PE','FILTER']   #FILTER=0 stands for failed to pass 'LowQual' filter, 1 = 'LowQual' filter passed.
    df = DataFrame([[r.CHROM,r.POS,r.INFO['END'],r.var_type,r.var_subtype,r.INFO['CT'],r.INFO['MAPQ'],r.INFO['PE'],(len(r.FILTER)*-1)+1] for r in records], columns = columns)
    return df

def parse_duplication(records):
    """ """
    columns = ['CHR1','START','END','TYPE','SUBTYPE','CT','MAPQ','PE','FILTER']   #FILTER=0 stands for failed to pass 'LowQual' filter, 1 = 'LowQual' filter passed.
    df = DataFrame([[r.CHROM,r.POS,r.INFO['END'],r.var_type,r.var_subtype,r.INFO['CT'],r.INFO['MAPQ'],r.INFO['PE'],(len(r.FILTER)*-1)+1] for r in records], columns = columns)
    return df


def parse_generic(records,sample):
    """ """
    #similar to translocation, extra fields should be included.
    columns = ['SAMPLE', 'CHR1','POS1','CHR2','POS2','TYPE','SUBTYPE','CT','MAPQ','PE','FILTER']    #FILTER=0 stands for failed to pass 'LowQual' filter, 1 = 'LowQual' filter passed.
    df = DataFrame([[sample,r.CHROM, r.POS, r.INFO['CHR2'], r.INFO['END'], r.var_type, r.var_subtype, r.INFO['CT'], r.INFO['MAPQ'], r.INFO['PE'], (len(r.FILTER) * - 1) + 1] for r in records], columns = columns)
    return df

def parse_multi_generic(samples):
    """ Parse a sample object list and returns a DataFrame """
    columns = ['SAMPLE', 'CHR1','POS1','CHR2','POS2','TYPE','SUBTYPE','CT','MAPQ','PE','FILTER']
    df = DataFrame([[sample.sample.split('.')[0], sample.site.CHROM, sample.site.POS, sample.site.INFO['END'], sample.site.var_type, sample.site.var_subtype, sample.site.INFO['CT'], sample.site.INFO['MAPQ'], sample.site.INFO['PE'],(len(sample.site.FILTER)*-1)+1] for sample in samples], columns = columns)
    return df


def structural_vcf_parser(vcf_file, str_type='TRA',parser='specific'):
    """ """
    with open(filename) as vcf_file:
        try:
            fvcf=vcf.Reader(vcf_file)
            records = [r for r in fvcf]
            try:
                assert(len(fvcf.samples)==1)
            except Exception,e:
                raise('Multisample VCF file.')
            else:
                sample = fvcf.samples[0].split('.')[0]
        except Exception,e:
            print e
        else:
            if parser == 'specific':
                for r in records:
                    if r.variant_subtype == 'TRA':
                        yield parse_translocation(r)
                    elif r.variant_subtype == 'INV':
                        yield parse_inversion(r)
                    elif r.variant_subtype == 'DEL':
                        yield parse_deletion(r)
                    elif r.variant_subtype == 'DUP':
                        yield parse_duplication(r)
                    else:
                        raise('Unrecognized Structural Variant type')
            elif parser == 'generic':
                for r in records:
                    yield parse_generic(r,sample=sample)


def simple_vcf_pasert(vcf_file,df=False):
    """ """
    with open(filename) as vcf_file:
        try:
            fvcf=vcf.Reader(open(vcf_file))
            records = [r for r in fvcf]
            try:
                assert(len(fvcf.samples)==1)
            except Exception,e:
                raise('Multisample VCF file.')
            else:
                sample = fvcf.samples[0].split('.')[0]
        except Exception,e:
            print e
        else:
            if df:
                return parse_generic(records,sample)  #returns a dataframe
            else:
                return records
            
def multiple_vcf_parser(filename):
    """ """
    res = []
    columns = ['SAMPLE', 'CHR1','POS1','CHR2','POS2','TYPE','SUBTYPE','CT','MAPQ','PE','FILTER']
    with open(filename, 'r') as vcf_file:
        try:
            fvcf=vcf.Reader(vcf_file)
            records = [r for r in fvcf]
            for record in records:
                for sample in record.samples:
                    if sample.is_variant: 
                        sample_name = sample.sample.split('.')[0]
                        res.append([sample.sample.split('.')[0], sample.site.CHROM, sample.site.POS, sample.site.INFO['CHR2'], sample.site.INFO['END'], sample.site.var_type, sample.site.var_subtype, sample.site.INFO['CT'], sample.site.INFO['MAPQ'], sample.site.INFO['PE'], (len(sample.site.FILTER)*-1)+1])
        except Exception,e:
            print e
        else:
            return DataFrame(res,columns=columns) #returns a dataframe


def main(args):
    files = vcf_generator(args.var,args.out)
    pool = multiprocessing.Pool(args.cores)
    for p in pool.imap_unordered(annotation_joiner, files):
        print "\t".join([str(p[0]),str(p[1])])


if __name__ == '__main__':
    parser = argparse.ArgumentParser("Structural Variant annotation module.")
    parser.add_argument('--var', dest='var', required=False, help='Variants out directory.')
    parser.add_argument('--out', dest='out', required=False, help='Output directory.')
    parser.add_argument('--ext', dest='ext', default='vep.vcf.gz', required=False, help='Vcf files extension.')
    parser.add_argument('--hot', dest='hospots', required=False, help='Alignment folder.')  #
    parser.add_argument('--cores', dest='cores', type=int, default=1, required=False, help='Number of cores to use')
    args = parser.parse_args()
    main(args)