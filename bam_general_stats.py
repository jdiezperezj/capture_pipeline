#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
1.- Alignment general stats -> data and could add plots for run. (script)
2.- Insert size stats -> data and distribution plots. ? (picard + reverse iterator)
3.- BQ, AQ and softclipped stats -> distribution and plots. ? (script)
4.- Coverage stats and plots: Related to targeted regions. (coverage_gc, ...)
"""

import os
import argparse
import pysam
import pybedtools
import json
from collections import OrderedDict
from pandas import DataFrame,concat
from re import findall
from numpy import histogram


class BAM_stats(object):
    """ """
    def __init__(self,bamfile,sample=False,bedfile=False,run=False,isDedup=False):
        self.run = run
        self.sample = sample if sample else bamfile.split('.')[0]
        self.bam = bamfile if isinstance(bamfile, pysam.csamtools.Samfile) else pysam.Samfile(bamfile, 'rb')
        self.bed = bedfile
        self.isDedup = isDedup
        self.collect_general_stats()
        
    def __del__(self):
        """ """
        self.bam.close()
        
    def get_mapq(self,):
        """ MAPQ distribution. """
        mq = [int(i.mapq) for i in self.bam.fetch()]
        hist = histogram(mq)    #hist[0] = count, hist[1]=mapq
        return dict(zip(hist[1][1:], hist[0]))  #mapq vector is 1 element longer than count verctor, because counts refere to map quality intervals.
        
    def get_relevant_info_header(self,):
        """ """
        pass
    
    def get_on_target(self,bedfile=False):
        """ """
        #cheeck 1 or 0 based coordinates.
        #check bedfile class
        if bedfile != False:
            if os.path.isfile(bedfile):
                self.bed = bedfile
            else:
                raise ValueError("Wrong bed file value was provided.")
        elif self.bed != False:
            if os.path.isfile(self.bed):
                pass
            else:
                raise ValueError("Wrong bed file value was provided.")
        bed = pybedtools.BedTool(self.bed)
        res = sum([self.bam.count(reference=s.chrom,start=s.start,end=s.end) for s in bed]) #Use iteration to count other features.
        return res
    
    def collect_binned_statistics(self,):
        """ Fields: alignment_binned,coverage_ext_binned,mapq_binned,?tlen_binned. Display as matrix: [region] x [stat]. Using a predefined window and then agregate ..."""
        pass
    
    def collect_general_stats(self,):
        """ """
        self.bam.mapped
        self.bam.unmapped
        self.duplicates = len([i for i in self.bam.fetch() if i.is_duplicate])
        #test soft clipped reads in the same line.
        self.nreads= self.bam.mapped + self.bam.unmapped
        self.pc_mapped = (self.bam.mapped/float(self.nreads))*100
        self.on_target = self.get_on_target()
        self.pc_ontarget = (self.on_target/float(self.nreads))*100
        self.pc_dup = (self.duplicates/float(self.nreads))*100
        self.descriptive_stats = {self.sample:OrderedDict([('nreads',self.nreads),('mapped',self.bam.mapped),('unmapped',self.bam.unmapped),('duplicates',self.duplicates),('on_target',self.on_target),('pc_mapped',self.pc_mapped),('pc_dup',self.pc_dup),('pc_ontarget',self.pc_ontarget)])}
        if self.isDedup:
            self.to_dedup()
            
    def to_dedup(self,):
        """ Add '_dd' prefix descriptive statistics keys."""
        d = []
        for k,v in self.descriptive_stats[self.sample].items():
            k = "dd_" + k
            d.append((k,v))
        d = OrderedDict(d)
        self.descriptive_stats[self.sample] = d
        
    def to_dict(self,):
        """ """
        return self.descriptive_stats
    
    def to_json(self,):
        """ """
        return json.dumps(self.to_dict())
    
    def to_df(self,):
        """ """
        return DataFrame.from_dict(data=self.to_dict(),orient='index')    #change to alternative orientation using 'index'.
    
    def __add__(self,other):
        """ Returns a concatenated DF. """
        if self.sample == other.sample:
            if self.isDedup == False and other.isDedup == True:
                return concat([self.to_df(),other.to_df()], axis=1)
            else:
                raise ValueError("First bam file should be original or mark for duplicates, second should have duplicates removed.")
        else:
            raise ValueError("Only same sample md and rd bam file results can be concatenated.")


def parse_cigar(cigarstring):
    """ Returns parsed pysam.AlignedRead.cigarstring attribute as a list of tuples. """
    return findall(r'(\d+)(\w)', cigarstring)


def get_bams_stats(bmd,brd):
    """ """
    md = pysam.Samfile(bmd.bam,'rb')
    rd = pysam.Samfile(brd.bam,'rb')
    allb = BAM_stats(md) + BAM_stats(rd)
    pc_unique_reads = DataFrame([numpy.float64(allb['dd_nreads']/allb['nreads']*100),], columns=['pc_unique_reads',], index=[md.sample])
    allb = concat([allb,pc_unique_reads],axis=0)
    return allb

def get_md_rd_bam_pairs(directory):
    """ """
    files = sorted([b for b in os.listdir(directory) if b.endswith('.bam')])
    try:
        assert(len(files % 2) == 0)
    except Exception,e:
        print e
    else:
        ifiles = iter(files)
        for i in ifiles:
            try:
                scnd = ifiles.next()
                assert( i.split('_')[0] == scnd.split('_')[0])  #use of '_' as bam file ids separator.
            except Exception,e:
                print e
            else:
                yield (i,scnd)


def main(args):
    pass


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description = 'Insert size calculation and plotting.')
    parser.add_argument('-i','--in_dir', required = True, help = 'Alignment folder.')
    parser.add_argument('-e','--ext', required = False, default='md.bam', help = 'Extension of files to use')
    parser.add_argument('-o','--out_dir', required = False, help = 'Output folder.')
    parser.add_argument('-r','--run_name', required = True, help = 'Run name.')
    parser.add_argument('-c','--cores', required = False, default=6, type=int, help = 'Number of cores.')
    args = parser.parse_args()
    main(args)