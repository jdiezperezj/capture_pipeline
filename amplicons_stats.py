#!/usr/bin/env python

import os
import subprocess
import pysam
import argparse
import csv
import multiprocessing
from operator import attrgetter
from collections import Counter
from pandas import DataFrame,concat
import matplotlib
import matplotlib.pyplot as plt
import numpy
import tarfile


class Amplicon(object):
    pass

def softclipBam(bam,manifest):
    """ """
    #for each amplicon, take coordinates, sequence and softclip according to match.
    #read so
    pass

def compress_files(output,files):
    """ Compress files and delete originals"""
    tar = tarfile.open(output+".tar.gz", "w:gz")
    for name in files:
        tar.add(name,arcname=os.path.split(name)[1])
        os.remove(name)
    tar.close()

def box_plot(df,name,ylab='',xlab='',title='% Unique Reads'):
    """ Create boxplot. """
    f = plt.figure(figsize=(17, 10))
    ax=f.gca()
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.grid(False)
    ax.tick_params(axis='both', which='major', labelsize=8)
    ax.tick_params(axis='both', which='minor', labelsize=7)
    bp = df.boxplot(ax=ax)  # s is an instance of Series
    plt.title(title, fontsize=18, color='blue')
    plt.ylabel(ylab, fontsize=8)    
    plt.xlabel(xlab, fontsize=7)
    locs, labels = plt.xticks()
    plt.setp(labels, rotation=90)
    plt.savefig(name)


def heat_map(df, filename, title='', rows=6, cols=11, m=0, M=100):
    """ Function to plot heatmap and color bar. """
    #min, max = (0, 100)
    m = numpy.amin(df.as_matrix())
    print m
    M = numpy.amax(df.as_matrix())
    print M
    step = 5
    mymap = matplotlib.colors.LinearSegmentedColormap.from_list('mycolors',['white','yellow','green','blue'])
    Z = [[0,0],[0,0]]
    levels = range(m,M+step,step)
    CS3 = plt.contourf(Z, levels, cmap=mymap)
    fig = plt.figure(figsize=(rows,cols))
    ax1 = plt.axes(frameon=False)
    plt.tick_params(axis='x', which='both', top='off', labelsize=5)
    plt.tick_params(axis='y', labelsize=4, left='off',right='off')
    plt.pcolor(df,cmap=mymap)
    plt.yticks(numpy.arange(0.5, len(df.index), 1), df.index)
    plt.xticks(numpy.arange(0.5, len(df.columns), 1), df.columns)
    locs, labels = plt.xticks()
    plt.setp(labels, rotation=90)
    fig.patch.set_visible(False)
    cbar=plt.colorbar(CS3, shrink=.3, aspect=10)
    cbar.ax.tick_params(labelsize=5)
    cbar.outline.set_visible(False)
    plt.title(title)
    plt.savefig(filename,dpi=200)
    plt.close(fig)
    return filename

def aligned_read_pairs(b,chr,start,stop):
    """ It makes use of mate search. Slow."""
    for alignedread in b.fetch(chr, start, stop):
        if alignedread.is_read1 and alignedread.is_unmapped != True:
            R1 = alignedread
            try:
                R2 = b.mate(R1)
            except ValueError:
                pass
            else:
                yield (R1,R2)

def aligned_read_pairs2(b,chr,start,stop):
    """ Much faster alternative to retrive and yield aligned reads to amplicon regions."""
    reads = [alignedread for alignedread in b.fetch(chr, start, stop)]
    reads = sorted(reads, key=attrgetter('qname'))
    res = [i for i in zip(reads,reads[1:]) if i[0].qname==i[1].qname]
    for pair in res:
        yield pair
        
def base_frequency(counter):
    """ BC base content calculations"""
    if len(counter)>0:
        s = "".join([i*j for i,j in counter.items()]).upper().replace("_","")
        d = dict([(i,j/float(len(s))*100) for i,j in Counter(s).items()])
        for l in list('ATGC'):
            if l not in d.keys():
                d[l] = 0.00
        return d
    else:
        return {'A':0.00,'T':0.00,'C':0.00,'G':0.00}

def base_content(counter,base='G'):
    """ Base content computation. """
    l = lambda x: (x.upper().replace("_","").count(base)/float(len(x)-1)) * 100
    header = ["pc_g","bc_freq"]
    rows = [[l(i),j] for i,j in counter.items()]
    return DataFrame(data=rows,columns=header)

def get_reads_amplicons(b,chr,start,stop, name):
    """ Generate amplicon dictionaries. """
    amplicon={}
    amplicon['name'] = name
    bcs = {}
    amplicon['bcs'] = bcs
    norig = 0
    ndedup = 0
    bclist = []
    for R1,R2 in aligned_read_pairs2(b,chr,start,stop):
        name,bc = R1.qname.split("#")
        if R1.is_unmapped != True and R2.is_unmapped != True:
            try:
                rstart = min(R1.positions)
                rstop = max(R2.positions)
            except Exception,e:
                print "get_read_amplicons: {e}".format(e=e)
            else:
                if rstart >= start and rstop <= stop:
                    if bc not in amplicon['bcs'].keys():
                        amplicon['bcs'][bc] = [(name,R1,R2),]
                        ndedup += 1
                        norig += 1
                        bclist.append(bc)
                    else:
                        amplicon['bcs'][bc].append((name,R1,R2))
                        norig += 1
                        bclist.append(bc)
    if norig == 0:
	print b,name,bclist
        print empty amplicon
    amplicon['counter'] = Counter(bclist)
    try:
        a = numpy.array([i for i in amplicon['counter'].values()])
        amplicon['percentiles'] = dict([("p"+str(n),numpy.percentile(a,n)) for n in [100,75,50,25]])
    except ValueError:
        amplicon['percentiles'] = {'p100':0,'p75':0,'p50':0,'p25':0}
    amplicon['bfreq'] = base_frequency(amplicon['counter'])
    amplicon['top_ten'] = " ; ".join([":".join([i,str(j)]) for i,j in Counter(bclist).most_common(3)]) #https://docs.python.org/2/library/collections.html#collections.Counter
    #Things to add: %bases in barcodes per amplicon, distribution of duplication values, number of targeted sequences per barcode pair, ...
    amplicon['norig'] = norig
    amplicon['ndedup'] = ndedup
    if norig > 0:
        amplicon['pc'] = (ndedup/float(norig))*100
    else:
        amplicon['pc'] = 0
    return amplicon
    

def read_coordinates(amplicons_file):
    with open(amplicons_file, 'rb') as csvfile:
        spamreader = csv.reader(csvfile, delimiter='\t', quoting=csv.QUOTE_NONE)
        for row in spamreader:
            yield row

def bam_file_generator(bamfolder,end=".bam"):
    """ Generate en yield of BAM files list."""
    for f in sorted(os.listdir(bamfolder)):
        if f.endswith(end):
            name = f.split(".")[0]
            p = os.path.join(bamfolder,f)
            yield (name,p)

def process_bam(d):
    """ Main frunction to deduplicate amplicon reads."""
    basename,inbamfile,amplicons,outfolder = d
    try:
        infile = pysam.Samfile( inbamfile, "rb" )
    except Exception,e:
        print e
        pass
    else:
        header = ['SAMPLE','AMPLICON','N_ORIG_PAIRS','N_DEDUP_PAIRS','PC','%A','%T','%G','%C','p100','p75','p50','p25','TOP_3']
        outbamfile = os.path.join(args.out, basename + '.dedup.bam')
        outfile = pysam.Samfile( outbamfile, "wb", template = infile )
        outstats = os.path.join(args.out, basename + '.dedup.bam.stats')
        outstatsfh = open(outstats,"w")
        outstatsfh.write("\t".join(header) + "\n")
        rows = []
        bcs = []
        counter = False
        for a in amplicons:
            name = a[3]
            amplicon = get_reads_amplicons(infile,a[0],int(a[1]),int(a[2]),name)
            if counter == False:
                counter = amplicon['counter']
            else:
                counter += amplicon['counter']
            print "\t".join(["Processing ...",basename,name])
            bf = [amplicon['bfreq'][i] for i in list('ATGC')]
            percentiles = [amplicon['percentiles'][i] for i in ['p100','p75','p50','p25']]
            row = [basename,name,amplicon['norig'],amplicon['ndedup'],amplicon['pc']]+ bf + percentiles +[amplicon['top_ten'],]
            rows.append(row)
            bcs += amplicon['bcs'].keys()
            outstatsfh.write("\t".join(map(str,row)) + "\n")
            print row
            for y,z in amplicon['bcs'].items():
                outfile.write(z[0][1])
                outfile.write(z[0][2])
    infile.close()
    outfile.close()
    outstatsfh.close()
    outbamfilest = os.path.join(args.out, basename + '.dedup.st.bam')
    cmd = ["samtools", "sort", "-f", outbamfile,outbamfilest]
    subprocess.check_call(cmd)
    cmd = ["samtools", "index", outbamfilest]
    subprocess.check_call(cmd)
    subprocess.check_call(["rm",outbamfile])
    return {"basename":basename, "rows":rows, "bcs":bcs, "counter":counter}

def repeated_bcs(scounter,sample):
    """ Repeated BCs are computed listed. """
    scounter_h1 = Counter(dict([y for y in scounter.items() if y[1]>1]))
    t10 = ";".join([":".join([j,str(k)]) for j,k in scounter.most_common(10)])
    print "\t".join([sample,t10])
    bf = base_frequency(scounter_h1)
    bf = [bf[j] for j in list('ATGC')]
    if len(scounter_h1)>0:
        a = numpy.array([k for k in scounter_h1.values()])
        pc = dict([("p"+str(l),numpy.percentile(a,l)) for l in [100,75,50,25]])
        pc = [pc[m] for m in ['p100','p75','p50','p25']]
    else:
        pc = [0.00,0.00,0.00,0.00]
    return [[sample,] + [len(scounter_h1),] + bf + pc + [t10,],]

def main(args):
    amplicons = [i for i in read_coordinates(args.int)]
    pool = multiprocessing.Pool(args.cores)
    arguments = [(n,p,amplicons,args.out) for n,p in bam_file_generator(args.aln)]
    header = ['SAMPLE','AMPLICON','N_ORIG_PAIRS','N_DEDUP_PAIRS','PC','%A','%T','%G','%C','p100','p75','p50','p25','TOP_3']
    header_s = ['SAMPLE','LENGTH','%A','%T','%G','%C','p100','p75','p50','p25','TOP_10']
    for n,i in enumerate(pool.imap_unordered(process_bam, arguments)):
        print "\t".join([i['basename'],"Done"])
        scounter = Counter(i['bcs'])
        row = repeated_bcs(scounter,i['basename'])
        if n == 0:
            rows = i['rows']
            bcs = i['bcs']
            row_s = row
            t_counter = i['counter']
        else:
            rows += i['rows']
            bcs += i['bcs']
            row_s += row
            t_counter += i['counter']
    c_all = Counter(bcs)
    t_counter = base_content(t_counter)
    t_counter_name = os.path.join(args.out,".".join([args.run,"count.png"]))
    f = plt.figure(figsize=(10, 10))
    #ax=f.gca()
    plt.scatter(t_counter.pc_g,t_counter.bc_freq)
    plt.savefig(t_counter_name)

    print c_all.most_common(10)
    r_all = repeated_bcs(c_all,'ALL')
    row_s += r_all
    df_s = DataFrame(row_s,columns=header_s)
    df_s_name = os.path.join(args.out,".".join([args.run,"repeated_bcs.csv"]))
    df_s.to_csv(df_s_name,index=False)
    df = DataFrame(rows,columns=header)
    df['PC'] = df['PC'].astype(numpy.int64)
    df_name = os.path.join(args.out,".".join([args.run,"csv"]))
    df.to_csv(df_name,index=False)
    dfp = df.pivot(index='SAMPLE', columns='AMPLICON', values='PC')
    dfp.sort_index(axis=1)
    dfp.sort_index(axis=0)
    #dfp = concat([dfp,dfp.mean(axis=0)],axis=0)
    #dfp = concat([dfp,dfp.mean(axis=1)],axis=1)
    dfp_name = os.path.join(args.out,".".join([args.run,"matrix.csv"]))
    dfp.to_csv(dfp_name)
    print dfp
    #Create heatmap of dfp
    heat_name = os.path.join(args.out,".".join([args.run,"heatmap.pdf"]))
    heat_map(dfp,heat_name,title="% UNIQUE READS")
    #Creat boxplot dfp
    box_name = os.path.join(args.out,".".join([args.run,"boxplot.amp.pdf"]))
    box_plot(dfp,box_name,xlab='Amplicon',ylab='% Unique Reads')
    box_name_sample = os.path.join(args.out,".".join([args.run,"boxplot.samp.pdf"]))
    box_plot(dfp.transpose(),box_name_sample,xlab='Sample',ylab='% Unique Reads')
    files = [df_name,df_s_name,dfp_name,heat_name,box_name,box_name_sample,t_counter_name]
    compress_files(os.path.join(args.out,args.run),files)
    


if __name__ == '__main__':
    parser = argparse.ArgumentParser("")
    parser.add_argument('--aln', dest='aln', required=True, help='Alignment out directory.')
    parser.add_argument('--int', dest='int', required=True, help='Intervals out file.')
    parser.add_argument('--out', dest='out', required=True, help='Out')
    parser.add_argument('--run', dest='run', required=True,help='Run')
    parser.add_argument('--cores', dest='cores', type=int, default=5, required=False, help='Number of cores to use')
    args = parser.parse_args()
    main(args)
