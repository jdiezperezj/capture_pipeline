#!/usr/bin/env python

import os
import subprocess
import pysam
import argparse
import multiprocessing
from timeit import timeit
#from time import strftime,gmtime
import datetime


def parse_fastq_name(f,directory):
    """ """
    n = f.split(".fastq")[0]
    id,sample,lane,read,other = n.split("_")
    d = {"path":os.path.join(directory,f),"id":id,"sample":sample, "lane":lane, "read":read, "base":n, "filename":f}
    return d


def fastq_file_generator(fastq_folder,ext):
    """ """
    for f in sorted(os.listdir(fastq_folder)):
        if f.endswith(ext):
            f = parse_fastq_name(f,fastq_folder)
            yield f


def fastq_pair_generator(fastq_folder,ext):
    """ """
    fastq_iter = fastq_file_generator(fastq_folder,ext)
    for i in fastq_iter:
        j = fastq_iter.next()
        print i,j
        if i['id'] == j['id'] and i['sample'] == j['sample'] and i['read'] != j['read']:
            name = "_".join([i['id'],i['sample']])
            print name
            yield {'id':i['id'],'sample':i['sample'],i['read']:i['path'],j['read']:j['path'],"R1_base":i["base"],"R2_base":j["base"],"name":name}


def align_capture_sample(f):
    """ """
    outdir, index , bed, sample = f
    cmd = ["novoalign_capture.sh", outdir, sample["name"], index, sample["R1"], sample["R2"], bed]
    try:
        #time_used = timeit(subprocess.check_call(cmd))
        #time_used = strftime("%H:%M:%S", gmtime(time_used))
        start_time = datetime.datetime.now()
        subprocess.check_call(cmd)
        end_time = datetime.datetime.now()
        time_used = str(end_time - start_time)
        
    except Exception,e:
        print e
        return (sample["name"], "Error!", time_used)
    else:
        return (sample["name"], "Done", time_used)


def call_bwa_mem(fastq1,fastq2,genome,sample_name):
    """ """
    cmd1 = ['bwa', 'mem', genome, fastq1, fastq2, '-M']
    p1 = Popen(cmd1, stdin=p2.stdout, stderr=PIPE, stdout=PIPE)
    cmd2 = ['samtools', 'view', '-S', '-b' '-']
    name = ".".join([sample_name, 'pesr.bam'])
    with open(name, 'wb') as foh:
        p2 = Popen(cmd2, stdin=p1.stdout, stderr=PIPE, stdout=foh)
        res = p2.comunicate()
    return name
    

def main(args):
    files = [(args.aln.rstrip("/"), args.index, args.bed, f) for f in fastq_pair_generator(args.fastq.rstrip("/"),args.ext) if f["sample"] != "S0"]
    print files
    pool = multiprocessing.Pool(args.cores)
    aln_log = open(os.path.join(args.aln,"log.txt"),"w")
    for p in pool.imap_unordered(align_capture_sample, files):
        log = "\t".join([p[0], p[1], p[2]])
        print log
        aln_log.write(log + "\n")
    aln_log.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser("")
    parser.add_argument('--fastq', dest='fastq', required=True, help='Fastq folder.')
    parser.add_argument('--index', dest='index', required=True, help='Index file.')
    parser.add_argument('--aln', dest='aln', required=False, help='Alignment out directory.')
    parser.add_argument('--bed', dest='bed', required=False, help='Bed file used for coverage statistics.')
    parser.add_argument('--ext', dest='ext', default='fastq.gz', required=False, help='Fastq files extension.')
    parser.add_argument('--cores', dest='cores', type=int, default=1, required=False, help='Number of cores to use')
    args = parser.parse_args()
    main(args)
