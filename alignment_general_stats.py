#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import multiprocessing
from subprocess import Popen, PIPE
import pandas
from pandas import DataFrame, concat
import argparse
import pybedtools
import numpy
from operator import itemgetter


def exe_f(command, shell=True):
    """Function to execute a command and return stuff"""
    process = Popen(command, shell=shell, stdout=PIPE, stderr=PIPE)
    (stdout, stderr) = process.communicate()
    return process, stderr, stdout


def get_flagstats(bamfile,samplename=False,output='df'):
    """
    Simple read mapping statistics.
    samplename in_total,duplicates ... pc_mapped, pc_duplicates,pc_singletons
    """
    if not samplename :
        h,t = os.path.split(bamfile)
        samplename = t.split('.')[0]
    p,e,o = exe_f("samtools flagstat {f}".format(f=bamfile))
    r = [i.split(" + ",1) for i in o.split("\n")]
    
    r = [["_".join(j[1].split("(")[0].split()[1:]), int(j[0])] for j in r if len(j)>1]
    df = pandas.DataFrame(r,columns=["samplename",samplename,])    #Change reads per sample name.
    df = df.set_index(['samplename'])
    df = df.transpose()
    
    pc_mapped = round((df['mapped'][samplename]/float(df['in_total'][samplename])) * 100, 2)
    pc_singletons = round((df['singletons'][samplename]/float(df['in_total'][samplename])) * 100, 2)
    if df['duplicates'][samplename] != 0:
        pc_duplicates = round((df['duplicates'][samplename]/float(df['in_total'][samplename])) * 100, 2)
    else:
        pc_duplicates = 0
    
    #The returned dataFrame include percentages
    dfm = pandas.DataFrame([[samplename,pc_mapped,pc_duplicates,pc_singletons],] ,columns=['samplename', 'pc_mapped', 'pc_duplicates', 'pc_singletons',])
    dfm = dfm.set_index(['samplename'])
    
    #Concat df and dfm:
    df = pandas.concat([df,dfm],axis=1)
    
    #Returns a dictionary or a dataFrame.
    if output == 'dict':
        #return df.transpose().to_dict()[samplename] #return dictionary of dictionaries.
        return df.iloc[0,].to_dict()  #alternative
    elif output == 'df':
        return df
    else:
        raise ValueError("Unrecognized value for output parameter.")
    
    

def count_ontarget_reads(bamfile,bedfile):
    """ On target reads. """
    bam = pybedtools.BedTool(bamfile)
    bed = pybedtools.BedTool(bedfile)
    cols = ['CHR', 'START', 'STOP', 'LOCUS', 'SCORE','STRAND','ONTARGET','COV1X', 'LENGTH', 'FRACTION']
    cov_data = [[i.fields[0], numpy.int(i.fields[1]), numpy.int(i.fields[2]), i.fields[3], i.fields[4], i.fields[5], numpy.int(i.fields[6]), numpy.int(i.fields[7]), numpy.int(i.fields[8]), numpy.float(i.fields[9])] for i in bam.coverage(bed)]
    #print cov_data
    df = pandas.DataFrame(cov_data,columns = cols)
    total_ontarget = df.ONTARGET.sum()
    return {'ot':total_ontarget, 'df':df}


def get_general_stats(bam_pair):
    fmd = get_flagstats(bam_pair['md'],samplename = bam_pair['sample'], output='dict')
    frd = get_flagstats(bam_pair['rd'],samplename = bam_pair['sample'], output='dict')
    #put into a dataframe
    mdot = count_ontarget_reads(bam_pair['md'], bam_pair['bed'])
    rdot = count_ontarget_reads(bam_pair['rd'], bam_pair['bed'])
    #get what we need from each df and return as df.
    header = ['sample', 'total_reads', 'total_ontarget', 'duplicates', 'mapped', 'ontarget_unique', 'total_unique', 'mapped_unique', 'pc_mapped','pc_duplicates', 'pc_ontarget', 'pc_mapped_unique', 'pc_ontarget_unique', ]
    data = [bam_pair['sample'], fmd['in_total'], mdot['ot'],  fmd['duplicates'], fmd['mapped'], rdot['ot'], frd['in_total'], frd['mapped'], fmd['pc_mapped'], fmd['pc_duplicates'], round((mdot['ot']/fmd['mapped'] * 100), 2),frd['pc_mapped'],round((rdot['ot']/frd['mapped'] * 100), 2) ,]
    return pandas.DataFrame([data,],columns=header)


# The following 3 functions  deal with alignment files:

def parse_alignment_name(f,directory):
    """ Parse alignment file names: sample_name.atype.bam"""
    sample,atype = f.split(".")[:-1]
    d = {"path":os.path.join(directory,f),"sample":sample, "atype":atype,"filename":f}
    return d


def bam_file_generator(bam_folder,ext='bam'):
    """ Bam file generator. """
    for f in sorted(os.listdir(bam_folder)):
        if f.endswith(ext):
            f = parse_alignment_name(f,bam_folder)
            yield f

def alignment_pair_generator(aln_folder, bed, ext='bam'):
    """ Alignment pair generator. """
    bam_iter = bam_file_generator(aln_folder,ext)
    for i in bam_iter:
        j = bam_iter.next()
        if i['sample'] == j['sample'] and i['atype'] != j['atype']:
            name = i['sample']
            yield {'sample':i['sample'],i['atype']:i['path'],j['atype']:j['path'], 'bed':bed}


def main():
    """ """
    files = sorted([bam_p for bam_p in alignment_pair_generator(args.in_dir, args.bed, args.ext)])
    pool = multiprocessing.Pool(args.cores)
    dfs = [p for p in pool.imap_unordered(get_general_stats, files)]
    df = pandas.concat(dfs, axis=0)
    ofh = os.path.join(args.out, 'Alignment_metrics' + '.csv')
    df.to_csv(ofh, index = 0)
    #may be interested to plot the results.
    #Use seaborn for barplots of each: http://web.stanford.edu/~mwaskom/software/seaborn/tutorial/axis_grids.html


if __name__ == '__main__':
    parser = argparse.ArgumentParser("")
    parser.add_argument('--aln_dir', dest='aln_dir', required=False, help='Alignment out directory.')
    parser.add_argument('--bed', dest='bed', required=False, help='Bed file.')
    parser.add_argument('--out', dest='out', required=False, help='Out.')
    parser.add_argument('--run', dest='run', required=False, help='Run.')
    parser.add_argument('--ext', dest='ext', required=False, default='.bam', help='Extension.')
    parser.add_argument('--cores', dest='cores', type=int, default=5, required=False, help='Number of cores to use')
    args = parser.parse_args()
    main(args)
