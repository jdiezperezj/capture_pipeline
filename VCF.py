#!/usr/bin/env python

import os
import argparse
import vcf
import csv
from string import upper
import multiprocessing
from collections import OrderedDict
from pandas import DataFrame,concat
import hashlib
import pybedtools


class Oncotator(object):
    """ Load oncotator annotation file and extract information. """
    def __init__(self,filename,format='tsv',genome_version='hg19'):
        self.filename = filename
        self.format = format
        self.genome_version = genome_version
        self.name = os.path.split(self.filename)[1].split('.')[0]
        self.comment_char = '#'
        csv.register_dialect('tsv', delimiter='\t', quoting=csv.QUOTE_NONE)
        self.maf = self.parse_maf()
        
    def parse_maf(self,):
        """ Mutation Annotation Format Parser: https://wiki.nci.nih.gov/display/TCGA/Mutation+Annotation+Format+%28MAF%29+Specification """
        comments = []
        content = []
        header = False
        n = 0
        with open(self.filename, 'r') as f:
            for i in csv.reader(f,dialect='tsv'):
                if i[0].startswith(self.comment_char):
                    comments.append(i)
                else:
                    if n == 0:
                        header = i
                        n += 1
                    else:
                        if len(i) == len(header):
                            if n == 1:
                                self.df = self.process_line(i,header)
                                n += 1
                            else:
                                df = self.process_line(i,header)
                                df.sample_name[0] = self.name 
                                self.df = concat([self.df,df])

                        #else:
                            #res = check_data_frame([i,],header)
                            #self.df = concat(self.df,res)

        #subset and filter
        self.subset_oncotator()
        #return result
        return {'comments':comments, 'header':header, 'content':self.df}


    def process_line(self,i,header):
        """ """
        if len(i) == len(header):
            df = DataFrame(data=[i,],columns=header)
            ref = df.ref_allele[0] if df.ref_allele[0].isalpha() else ''
            alt = df.alt_allele[0] if df.alt_allele[0].isalpha() else ''
            df.sample_name[0] = self.name
            df.chr[0] = format_chr(df.chr[0])
            df.start[0] = int(df.start[0])
            hash_key = get_hash("_".join(map(str,[df.chr[0],df.start[0],ref,alt])))
            key = DataFrame([hash_key,],columns=["KEY",])
            df = concat([df,key],axis=1)
        else:
            df = DataFrame(data=[["" for j in range(len(header))],],columns=header)
        return df
    
    def maf_as_df(self,):
        """ """
        #May need to create a key to join annotation by chr-pos-ref-allele
        return self.maf['content']
    
    def subset_oncotator(self,):
        """
        ['Chr','Start','End', 'gene','ccds_id','HGNC_RefSeq IDs','transcript_id','variant_classification','gene_type','transcript_exon','transcript_position','ref_allele','alt_allele','read_depth','allelic_depth','FREQ','genotype','transcript_change','protein_change','dbSNP_RS','1000Genome_EUR_AF']
        Ccds_id -> no blank values
        variant_classification -> deleterious
        """
        header = ['KEY', 'chr','start','end', 'gene','ccds_id','HGNC_RefSeq IDs','transcript_id','variant_classification','gene_type','transcript_exon','transcript_position','ref_allele','alt_allele','read_depth','allelic_depth','FREQ','genotype','transcript_change','protein_change','dbSNP_RS','1000Genome_EUR_AF']
        yih = [h for h in self.df.columns.values.tolist() if h in header]
        nih = [k for k in header if k not in self.df.columns.values.tolist()]
        self.df   = self.df.loc[:,header]
        targeted_consequences = ['Frame_Shift_Del', 'Frame_Shift_Ins', 'In_Frame_Del', 'In_Frame_Ins', 'Missense_Mutation', 'Nonsense_Mutation', 'Splice_Site', 'Translation_Start_Site', 'Nonstop_Mutation', 'De_novo_Start_InFrame', 'De_novo_Start_OutOfFrame','Silent']
        self.df = self.df[self.df.variant_classification.isin(targeted_consequences)]
        #elf.df.select(lambda x: x not in targeted_consequences)
        self.df = self.df[self.df.ccds_id != ""]

class AnnotatedVariants(object):
    def __init__(self):
        pass

def get_hash(word):
    """ Returns hash from input string. """
    word = unicode(word)
    hash_object = hashlib.md5(word)
    return hash_object.hexdigest()

def format_chr(chr):
    """ Format chromosome as two digit str for autosomes. """
    chr = chr[3:] if chr.startswith('chr') else chr
    try:
	chr = format(int(chr), '02d')
    except Exception,e:
	return chr
    else:
	return chr


def parse_varscan_vcf_variant(variant):
    #all must return:
    #chr
    #pos
    #ref
    #alt
    #ref_dc
    #alg_dc
    pass

def parse_illumina_vcf_variant(variant):
    pass

def parse_mutect_vcf_variant(variant):
    pass

def parse_platypus_vcf_variant(variant):
    pass


def get_genotype(s):
    """ New version allows vcf with no variant calls in hotspots. """
    if s.is_het:
        gen = 'AB'
    else:
        if s.is_variant == False:
            gen = 'AA'
        else:
            gen = 'BB'
    return gen


def parse_varscan_1sample_annotated_vcf(filename,thr=150,genome_version='hg19',consequences = ['splice_donor_variant','splice_acceptor_variant', 'stop_gained','frameshift_variant','stop_lost','initiator_codon_variant','inframe_insertion','inframe_deletion', 'missense_variant', 'splice_region_variant','NMD_transcript_variant', 'transcript_ablation', 'transcript_amplification', 'coding_sequence_variant']):
    """ Very simple parser. To be fully implemented later on. """
    VARIANTS = {}
    basename = os.path.split(filename)[1]
    basename = basename.split('.')[0]
    vf = vcf.Reader(open(filename, 'r'))
    info_fields = tuple(vf.infos['CSQ'].desc.split()[-1].split('|'))
    vcf_records = [record for record in vf]
    for record in vcf_records:
        hotspot = False
	if len(record.samples) == 1:
            s = record.samples[0]
            if s.is_variant != True:
                evpf = "|".join(["" for j in range(len(info_fields))])
                evp = [evpf,]
                transcript_annotations = [OrderedDict(zip(info_fields, k.split('|'))) for k in evp]
                transcript_annotations[0]["CANONICAL"] = "HOTSPOT"
                hotspot = True
            elif s.is_variant == True:
                evp = record.INFO['CSQ']
                transcript_annotations = [OrderedDict(zip(info_fields, k.split('|'))) for k in evp]
                #transcript_annotations = [t for t in t_annotations if t["CANONICAL"] == "YES" and t["Feature"].startswith("NM_")]
            
            t_ans_good = []
            t_ans_bad = []
            for tr in transcript_annotations:
                #Change to pick up hotspots.
                if any(tj in consequences for tj in tr["Consequence"].split("&")):
                    if tr["CANONICAL"] == "YES" and tr["Feature"].startswith("NM_"):
                        t_ans_good.append(tr)
                    elif tr["CANONICAL"] == "YES" and tr["Feature"].startswith("XM_"):
                        t_ans_good.append(tr)
                    else:
                        t_ans_bad.append(tr)
                elif tr["CANONICAL"] == 'HOTSPOT':
                    t_ans_good.append(tr)
                else:
                    t_ans_bad.append(tr)
	    ALT_DC = s.data.AD
            REF_DC = s.data.RD
            COV = s.data.DP
	    COV_L = "HIGH_COV" if COV >= thr else "LOW_COV"
            try:
                FRQ = "{0:.2f}".format(ALT_DC/float(COV)* 100)
            except Exception,e:
                print record
                print "COV:\t",COV
                print "ALT_DC:\t",ALT_DC
                print "REF_DC:\t",REF_DC
                print e
                FRQ = "0.00"
            ref = s.site.REF if str(s.site.REF).isalpha() else ''
            alt = s.site.ALT[0] if str(s.site.ALT[0]).isalpha() else ''
            h = get_hash("_".join(map(str,[format_chr(s.site.CHROM), s.site.POS,ref, alt])))
	    chr = format_chr(s.site.CHROM)
            gen = get_genotype(s)
            zygosity = 'HOM' if gen in ['AA','BB'] else 'HET'
            vtype = record.var_type if hotspot == False else "HOTSPOT"
            subtype = record.var_subtype if hotspot == False else "HOTSPOT"
            #Include variant type and variant subtype
	    VARIANTS[h] = {"SAMPLE":basename, "GENOME_VERSION":genome_version, "CHR":chr, "POS":s.site.POS, "REF": s.site.REF, "ALT":s.site.ALT[0],"GEN":gen,"ZYG":zygosity,"TYPE":vtype,"SUBTYPE":subtype, "REF_DOC":REF_DC, "ALT_DOC":ALT_DC, "COV":COV , "FREQ":FRQ, "ANNOTATIONS":transcript_annotations, "GOOD":t_ans_good, "IGNORE":t_ans_bad,"INFO":info_fields}
        else:
	    pass
    return (VARIANTS,info_fields)


def variants_to_dataframe(variants,info,vkeys=["SAMPLE","CHR","POS","REF","ALT","GEN","ZYG","TYPE","SUBTYPE","COV","REF_DOC","ALT_DOC","FREQ","GENOME_VERSION"]):
    """Transforms variants dictionary to a dataframe. """
    data = []
    good = []
    bad = []
    header = ["KEY",] + vkeys + list(info)
    for k,v in variants.items():
        vvalues = [v[vkey] for vkey in vkeys]
        tvalues = [[k, ] + vvalues + x.values() for x in v['ANNOTATIONS']]
        gvalues = [[k, ] + vvalues + x.values() for x in v['GOOD']]
        bvalues = [[k, ] + vvalues + x.values() for x in v['IGNORE']]
        data += tvalues
        good += gvalues
        bad += bvalues
    #all_data = DataFrame(data=data, columns=header)  #FILTER COULD BE DONE AT THIS STEP
    all_data = check_data_frame(data,header)
    #good_data = DataFrame(data=good, columns=header)
    good_data = check_data_frame(good, header)
    #bad_data = DataFrame(data=bad, columns=header)
    bad_data = check_data_frame(bad, header)
    return (all_data,good_data,bad_data)
    


def generate_clonality_input_file(variants, vkeys=["SAMPLE","CHR","POS","REF","ALT","GEN","ZYG","TYPE","SUBTYPE","COV","REF_DOC","ALT_DOC","FREQ"]):
    """
    https://bitbucket.org/aroth85/pyclone/wiki/Tutorial
    mutation_id, ref_counts, var_counts, normal_cn, minor_cn, major_cn, variant_case, variant_freq, genotype
    ex: NA12156:BB:chr2:175263063	3812	14	2	0	2	NA12156	0.0036591741	BB
    normal_cn : This is the copy number of the mutant locus for the normal cells in the sample. In most cases this will be 2, and 1 for X,Y.
    minor_cn : This is the minor parental copy number predicted from the tumour sample.
    major_cn : This is the major parental copy number predicted from the tumour sample.
    variant_case : The 1000 genomes ID of the case which contains the variant genotype for this mutation.
    variant_freq : The fraction of reads showing the variant allele.
    genotype : The genotype of the case with the variant.
    """
    data = []
    header = ["MUTATION_ID",] + ["REF_DOC","ALT_DOC","NORMAL_CN","MINOR_CN","MAJOR_CN", "VARIANT_CASE","VARIANT_FREQ","GENOTYPE"]
    for k,v in variants.items():
        vid = ":".join(map(str,[v[vkey] for vkey in vkeys[:6]]))
        vvalues = [vid,] + [v[vkey] for vkey in ["REF_DOC","ALT_DOC"]] + [2,0,2] + [k,] + [v[vkey] for vkey in ["FREQ","GEN"]]
        data.append(vvalues)
    result = check_data_frame(data,header)
    return result #It can be written to csv or tsv directly.


def modify_data(df,name,output,sample):
    df1 = df.drop('KEY', axis=1)
    df1.sort(['CHR', 'POS'], ascending=[1, 1], inplace=True)
    df1.drop('Allele', axis=1, inplace=True)
    df1.drop('Gene', axis=1, inplace=True)
    #fname = os.path.join(output,sample + "." + name +".VEP_annotation.csv")
    #df1.to_csv(fname,index=False)
    header = [df1.columns.values.tolist(),]
    return header + list(df1.to_records(index=False))


def create_excel_file(data,outfile,order):
    """ Create a multitab excel file from a dictionary of elements. """
    from xlsxwriter import Workbook as wb
    workbook = wb(outfile)
    worksheets={}
    for k in order:
	worksheets[k] = workbook.add_worksheet(k)
	for row, row_data in enumerate(data[k]):
	    worksheets[k].write_row(row, 0, map(str,row_data))
    workbook.close()


def annotation_joiner(unit):
    """ """
    #http://www.ensembl.org/info/genome/variation/predicted_data.html#consequences
    
    output,sample,vep,oncotator = unit
    print sample
    variants,info = parse_varscan_1sample_annotated_vcf(vep)
    vdf,gdf,bdf = variants_to_dataframe(variants,info)
    alld = modify_data(vdf,"all",output,sample)
    goodd = modify_data(gdf,"good",output,sample)
    badd = modify_data(bdf,"bad",output,sample)
    onc = Oncotator(oncotator)
    onc.df.drop('KEY', axis=1, inplace=True)
    odf = [onc.df.columns.values.tolist(),] + list(onc.df.to_records(index=False))
    data = {"pass":goodd,"discarded":badd,"oncotator":odf}
    fname = os.path.join(output, sample + ".VEP_annotation.xlsx")
    create_excel_file(data,fname,["pass","discarded","oncotator"])
    #vdf1 = vdf.drop('KEY', axis=1)
    #vdf1.sort(['CHR', 'POS'], ascending=[1, 1], inplace=True)
    #vdf1.drop('Allele', axis=1, inplace=True)
    #vdf1.drop('Gene', axis=1, inplace=True)
    #fname = os.path.join(output,sample + ".VEP_annotation.csv")
    #vdf1.to_csv(fname,index=False)
    #onc = Oncotator(oncotator)
    #odf = onc.df
    #merged_df = vdf.merge(odf,left_on=['KEY'],right_on=['KEY'])
    #merged_df = concat([vdf,odf], axis=0, join='outer', join_axes=None, ignore_index=False, keys=['KEY'], levels=None, names=None, verify_integrity=False)
    #merged_df.drop('KEY', axis=1, inplace=True)
    #merged_df = merged_df.query(" 'M_' in Feature & CANONICAL == 'YES'")
    #merged_df.sort(['CHR', 'POS'], ascending=[1, 1], inplace=True)    #Sort by chromosome and position
    #subset columns in list
    #subset only important
    #fname = os.path.join(output,sample + ".joint_annotation.csv")
    #merged_df.to_csv(fname,index=False)
    return (sample,"Done")


def get_hotspots_coverage(filename,bamfile):
    pass

def read_hotspots(filename):
    pass


def check_data_frame(data,header):
    """ """
    empty = True if len(data)==1 and len(data[0]) == 0  else False
    if empty:
        data = [[''] * len(header)]
        return DataFrame(data, columns=map(str,header))
    else:
        return DataFrame(data, columns = header)

def vcf_generator(directory,output,ext=".vep.vcf.gz"):
    """ Directory extension returns a tuple """
    return [(output,f.split(".")[0], os.path.join(directory,f),os.path.join(directory,f.split(".")[0] + ".tsv")) for f in os.listdir(directory) if f.endswith(".vep.vcf.gz")]


def main(args):
    files = vcf_generator(args.var,args.out)
    pool = multiprocessing.Pool(args.cores)
    for p in pool.imap_unordered(annotation_joiner, files):
        print "\t".join([str(p[0]),str(p[1])])
        


if __name__ == '__main__':
    parser = argparse.ArgumentParser("Variant annotation module.")
    parser.add_argument('--var', dest='var', required=False, help='Variants out directory.')
    parser.add_argument('--out', dest='out', required=False, help='Output directory.')
    parser.add_argument('--ext', dest='ext', default='vep.vcf.gz', required=False, help='Vcf files extension.')
    parser.add_argument('--hot', dest='hospots', required=False, help='Alignment folder.')
    #parser.add_argument('--aln', dest='aln', required=True, help='Alignment foldert.')
    parser.add_argument('--cores', dest='cores', type=int, default=1, required=False, help='Number of cores to use')
    args = parser.parse_args()
    main(args)

