#!/usr/bin/env python
# -*- coding: utf-8 -*-

import hashlib
import os
import sys
import vcf
from operator import itemgetter
from string import upper
import argparse

"""
Compare vcf directories of VCF from original and deduplicated BAM files.
"""


class VCF_file(object):
    """ VCF file wrapper. """
    def __init__(self,f,directory):
	""" Fields to be provided. """
	self.f = f
	self.directory = directory
	self.get_path()
	self.get_fields()
	self.parse_vcf()
	
    def __repr__(self):
	return self.name
    
    def is_pair(self,other):
	""" Check if compared VCF objects are pairs. """
	#need to change it
	if other.fs == self.fs and  self.sid != other.sid:
	    return True
	else:
	    return False

    
    def get_path(self,):
	""" It returns file path. """
	self.path = os.path.join(self.directory,self.f)
    
    def get_fields(self,):
	""" It get id fields from file name. """
	self.name = self.f.split('.vep.vcf.gz')[0]
	self.fs, self.sid = self.name.split('_')

    def parse_vcf(self,thr=150):
	""" Very simple VCF parser. To be improved in next version. """
	self.hash = {}
	vf = vcf.Reader(open(self.path, 'r'))
	info_fields = map(upper,tuple(vf.infos['CSQ'].desc.split()[-1].split('|')))
	vcf_records = [record for record in vf]
	for i in vcf_records:
	    	if len(i.samples) == 1 and i.samples[0].is_variant == True:
			evp = dict(i.INFO)['CSQ']
			transcript_annotations = [dict(zip(info_fields, k.split('|'))) for k in evp]
			s = i.samples[0]
			r_dc = s.data.RD
			a_dc = s.data.AD
			cov = r_dc + a_dc
			cov_l = "HIGH_COV" if cov >= thr else "LOW_COV" 
			frq = "{0:.2f}".format((float(a_dc)/float(cov))* 100)
			h = get_hash("_".join(map(str,[s.site.CHROM, s.site.POS,s.site.REF, s.site.ALT[0]])))
			chr = format_chr(s.site.CHROM)
			transcript = [[t['SYMBOL'],t['CANONICAL'],t['CLIN_SIG'],t['CONSEQUENCE'],t['HGVSC'],t['HGVSP']] for t in transcript_annotations if t['FEATURE'].startswith("NM_")][0]
			self.hash[h] = [chr, s.site.POS,s.site.REF, s.site.ALT[0],] +transcript + [cov_l, r_dc,a_dc,frq,]
	
class VCF_pair(object):
    """ This class deal with VCF pair object as sequencing replicates. """
    def __init__(self,a,b):
	self.p1 = a
	self.p2 = b
	self.check_pair()
	self.data = self.cmp_vcf_hash()
	self.data_dict = (self.p1.fs,self.data)#Turn into real directory
    
    def check_pair(self,):
	""" This function check if files are true VCF pair files. """
	if self.p1.is_pair(self.p2) != True:
		raise PairError(self.p1,self.p2)
	else:
		print "\t".join(["Analysis for pair:",str(self.p1),str(self.p2)])

    def get_paths(self,):
	""" It returns a tuple of file absoulte paths."""
	return (self.p1.path, self.p2.path)
    
    def cmp_vcf_hash(self,):
	""" It returns values."""
	s1 = set(self.p1.hash.keys())
	s2 = set(self.p2.hash.keys())
	intersection = s1.intersection(s2)
	#s1_only = s1.difference(s2)
	#s2_only = s1.difference(s1)
	#res = sorted([self.p1.hash[i] + self.p2.hash[i][-4:] for i in intersection], key=itemgetter(0,1))
        #Return dict
	#intersection = dict([(i, self.p1.hash[i] + self.p2.hash[i][-4:]) for i in intersection])
        return dict([(i, self.p1.hash[i] + self.p2.hash[i][-4:]) for i in intersection])
	#return res difference and/or symmetric difference as well.
	#res = {"intersection":intersection,"s1_only":s1_only,"s2_only":s2_only}



class PairError(Exception):
    """ Error class for VCF pairs. """
    def __init__(self, p1,p2):
	self.value = "Files {p1} and {p2} are not pairs.".format(p1=p1,p2=p2)
    def __str__(self):
	return repr(self.value)

class DirPairError(Exception):
    """ Error class for paired VCF directory. """
    def __init__(self, msg,n):
	self.value = msg % n
    def __str__(self):
	return repr(self.value)

    
def get_hash(word):
    """ Returns hash from input string. """
    word = unicode(word)
    hash_object = hashlib.md5(word)
    return hash_object.hexdigest()

def format_chr(chr):
    """ Format chromosome as two digit str for autosomes. """
    chr = chr[3:] if chr.startswith('chr') else chr
    try:
	chr = format(int(chr), '02d')
    except Exception,e:
	return chr
    else:
	return chr

def vep_variant_file_parser(filename):
    """ VCF parser for vcf 4.1 and VEP annotation. """
    vf = vcf.Reader(open(filename, 'r'))
    info_fields = tuple(vf.infos['CSQ'].desc.split()[-1].split('|'))
    vcf_records = [record for record in vf]
    variants = []
    for i in vcf_records:
        for s in range(len(i.samples)):
            s = i.samples[s]
            if s.is_variant:
                evp = dict(i.INFO)['CSQ']
                transcript_annotations = [dict(zip(info_fields, k.split('|'))) for k in evp]
                variants.append(Variant(s, transcript_annotations, info_fields))
    return VCF_file(filename,variants)  #Returns a class of type VCF_file

def parse_vcf(filename,thr=150):
	""" Very simple parser. To be fully implemented later on. """
    	res = {}
    	basename = filename.split('.')[0]
    	vf = vcf.Reader(open(filename, 'r'))
    	info_fields = tuple(vf.infos['CSQ'].desc.split()[-1].split('|'))
    	vcf_records = [record for record in vf]
    	for i in vcf_records:
		if len(i.samples) == 1 and i.samples[0].is_variant == True:
	    		evp = i.INFO['CSQ']
			print evp
	    		transcript_annotations = [dict(zip(info_fields, k.split('|'))) for k in evp]
	    		transcript = [[t['CLIN_SIG'],t['HGVSc'],t['HGVSp']] for t in transcript_annotations if t['FEATURE'].startswith("NM_") and t['CANONICAL'] == "YES"][0]
	    		s = i.samples[0]
	    		r_dc,a_dc = s.data.AD
	    		cov = r_dc + a_dc
	    		cov_l = "HIGH_COV" if cov >= thr else "LOW_COV" 
	    		frq = "{0:.2f}".format((float(a_dc)/float(cov))* 100)
            		h = get_hash("_".join(map(str,[s.site.CHROM, s.site.POS,s.site.REF, s.site.ALT[0]])))
	    		chr = format_chr(s.site.CHROM)
	    		res[h] = [chr, s.site.POS,s.site.REF, s.site.ALT[0]] + transcript + [ cov_l, r_dc, a_dc, frq,]	#cov_l: class (HIGH or LOW coverage variant), r_dc: reference allele depth of coverage, a_dc: alternative allele depth of coverage , frq: allele frequency
		else:
	    		pass
    	return res

def cmp_vcf_hash(h1,h2):
    """ Comparisson of vcf hashes list. Returns variant intersection. """
    s1 = set(h1.keys())
    intersection = s1.intersection(set(h2.keys()))
    res = sorted([h1[i] + h2[i][-4:] for i in intersection], key=itemgetter(0,1))
    return res

def create_excel_file(data,outfile,header=False):
    """ Create a multitab excel file from a dictionary of elements. """
    from xlsxwriter import Workbook as wb
    workbook = wb(outfile)
    worksheets={}
    for k in data.keys():
	worksheets[k] = workbook.add_worksheet(k)
	if header != False:
	    worksheets[k].write_row(1, 0, header)
	for row, row_data in enumerate(data[k]):
	    worksheets[k].write_row(row +2, 0, map(str,row_data))
    workbook.close()
    
def vcf_file_pair_generator(directory,file_ext='.vep.vcf.gz'):
	""" Generator of pair objects from a paired directory. """
	from itertools import izip
	data = sorted([i for i in os.listdir(directory) if i.endswith(file_ext)])
	res = [(a,b) for a,b in zip(data,data[1:]) if a.split("_")[0] == b.split("_")[0]]
	for x,y in res:
		x = VCF_file(x,directory)
		y = VCF_file(y,directory)
		yield VCF_pair(x,y)


def compare_dd_samples(outdir,d1,d2,sample):
    s1 = set(d1.keys())
    intersection = s1.intersection(set(d2.keys()))
    header = map(upper,['chromosome', 'position', 'reference', 'alternative','SYMBOL','CANONICAL','CLIN_SIG','CONSEQUENCE','HGVSc','HGVSp', 'class_A', 'ref_cov_A', 'alt_cov_A', 'freq_A','class_B', 'ref_cov_B', 'alt_cov_B', 'freq_B'])
    outfile1 = os.path.join(outdir,args.run_name + "." +sample +'.variants_intersection.xlsx')
    h={}
    h[sample] = sorted([i for i in d1.values()], key=itemgetter(0,1))
    h[sample+'_dd'] = sorted([j for j in d2.values()], key=itemgetter(0,1))
    create_excel_file(h,outfile1,header=header)
    return sorted([d1[k] + d2[k][10:] for k in intersection], key=itemgetter(0,1))  
    

def main(args):
    data = dict([pair1.data_dict for pair1 in vcf_file_pair_generator(args.in_dir)])
    data2 = dict([pair2.data_dict for pair2 in vcf_file_pair_generator(args.dd_dir)])
    res = {}
    for k,v in data.items():
	try:
	    res[k] = compare_dd_samples(args.out_dir,v,data2[k],k)
	except KeyError:
	    pass
    outdir = args.out_dir if args.out_dir != False else os.path.join(args.in_dir,'results')
    if os.path.exists(outdir) == False:
	os.makedirs(outdir, 0755)
    header = map(upper,['chromosome', 'position', 'reference', 'alternative','SYMBOL','CANONICAL','CLIN_SIG','CONSEQUENCE','HGVSc','HGVSp', 'class_A', 'ref_cov_A', 'alt_cov_A', 'freq_A','class_B', 'ref_cov_B', 'alt_cov_B', 'freq_B','dd_class_A', 'dd_ref_cov_A', 'dd_alt_cov_A', 'dd_freq_A','dd_class_B', 'dd_ref_cov_B', 'dd_alt_cov_B', 'dd_freq_B'])
    outfile = os.path.join(outdir,args.run_name + '.variants_intersection.xlsx')
    create_excel_file(res,outfile,header=header)
    print "VCF folder comparisson was performed successfully."
    print "Results file path is: {of}".format(of=outfile)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description = 'VCF original vs deduplicated comparisson.')
    parser.add_argument('-i','--in_dir', required = True, help = 'Original folder.')
    parser.add_argument('-d','--dd_dir', required = True, help = 'Deduplicated folder.')
    parser.add_argument('-o','--out_dir', required = False, help = 'Output folder.')
    parser.add_argument('-r','--run_name', required = False, help = 'Run name.')
    args = parser.parse_args()
    main(args)
