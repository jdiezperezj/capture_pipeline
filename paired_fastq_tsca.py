#!/usr/bin/env python

import os
import re
import argparse
import gzip
import itertools
import operator
from collections import OrderedDict,Counter

from Illumina import SampleSheet, RunSheet

from Bio import SeqIO
from Bio.Seq import Seq
from Bio.Alphabet import IUPAC

import matplotlib.pyplot as plt,pylab

from pandas import HDFStore,DataFrame


"""
Identifier field from casava 1.8 on:

@EAS139:136:FC706VJ:2:2104:15343:197393 1:Y:18:ATCACG

@M02616:1:000000000-A76EY:1:1101:15325:1430 1:N:0:34

EAS139 	the unique instrument name
136 	the run id
FC706VJ 	the flowcell id
2 	flowcell lane
2104 	tile number within the flowcell lane
15343 	'x'-coordinate of the cluster within the tile
197393 	'y'-coordinate of the cluster within the tile
1 	the member of a pair, 1 or 2 (paired-end or mate-pair reads only)
Y 	Y if the read is filtered, N otherwise
18 	0 when none of the control bits are on, otherwise it is an even number
ATCACG 	index sequence (or SS id)

## Possibility, add a new field for BC ->
@M02616:1:000000000-A76EY:1:1101:15325:1430 1:N:0:34:GGGGGG
## Even possible to include Amplicon name in header
@M02616:1:000000000-A76EY:1:1101:15325:1430 1:N:0:34:GGGGGG:AmpliconX


Output:
Sample  ReadName    Amplicon    BCPair  BC1 BC2
...
First 2 are mandatory, the 4 last fields can be empty al False
"""


class IlluminaFastqRun(object):
    def __init__(self):
        pass


class AmpliconSample(object):
    def __init__(self,name,ss):
        self.name = name
        self.amplicons = {}
        
    def __repr__(self):
        pass
    
    def get_amplicons(self,):
        pass
    
    def get_amplicon(self,amplicon):
        try:
            res = self.aplicons[amplicon]
        except KeyError:
            return false
        else:
            return res
    
    def set_amplicon(self,amplicon):
        if amplicon.ss == self.ss:
            if amplicon.name not in self.amplicons.keys():
                self.amplicons[amplicon.name] = amplicon
            else:
                self.amplicon[amplicon.name].reads += amplicon.reads
        else:
            pass

class Amplicon(object):
    def __init__(self,name,ss):
        self.name = name
        self.ss = ss
        self.reads = []
        
    def __repr__(self):
        pass
    
    def add_reads(self, reads):
        self.reads.append(reads)
    
    def get_reads(self, ):
        pass
    
    def get_BC_freqs(self):
        pass
    
    def get_reads_by_BC(self,):
        pass
    

class ReadPair(object):
    def __init__(self,name,R1,R2):
        self.name = name
        self.R1 = R1
        self.R2 = R2
        self.strand
        self.ULSO
        self.DLSO
        self.BCs
        self.BC_String
    def __repr__(self):
        pass
    

def plot_hist(values, title,filename):
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.hist(values, 10, normed=1, facecolor='green', alpha=0.75)
    fig.savefig(filename)
    fig.close()

def local_match(read,seq, method='re'):
    """ Method could be pairwise or regular expression"""
    if match:
        return coordinates,seq.name
    else:
        return false

def get_primers(amplicon,ULSO,DLSO,read):
    """ Return a tuple containing location coordinates (first base matching) of specific primers in read sequence."""
    ulso_m = [(m.start(0), m.end(0)) for m in re.finditer(str(ULSO),str(read.seq))]
    dlso_m = [(m.start(0), m.end(0)) for m in re.finditer(str(DLSO),str(read.seq))]
    ULSO = Seq(str(ULSO), IUPAC.IUPACUnambiguousDNA())
    DLSO = Seq(str(DLSO), IUPAC.IUPACUnambiguousDNA())
    ULSO_r = str(ULSO.reverse_complement())
    DLSO_r = str(DLSO.reverse_complement())
    ulso_r = [(m.start(0), m.end(0)) for m in re.finditer(str(ULSO_r),str(read.seq))]
    dlso_r = [(m.start(0), m.end(0)) for m in re.finditer(str(DLSO_r),str(read.seq))]
    d = {'ulso_m':ulso_m, 'ulso_r':ulso_r, 'dlso_m':dlso_m, 'dlso_r':dlso_r}
    d = dict([(k,v[0]) for k,v in d.items() if len(v) > 0 ])
    if len(d) > 0 and isinstance(d,dict) == True:
        for k,v in d.items():
            if k in ['ulso_m','dlso_r']:
                res = {k:v}
                return res
    else:
        return False

def get_primers_location(sample,amplicon,ULSO,DLSO,read1,read2):
    """ Return a tuple containing location coordinates (first base matching) of specific primers in read sequence."""
    d1 = get_primers(amplicon,ULSO,DLSO,read1)
    d2 = get_primers(amplicon,ULSO,DLSO,read2)
    if all(isinstance(i,dict) for i in [d1,d2]):
        try:
            t1 = min(d1['dlso_r']) == 6 or False
            t2 = min(d2['ulso_m']) == 6 or False
        except KeyError:
            pass
        else:
            if all(j!=False for j in [t1,t2]):
                bc1=str(read1.seq[:6])
                bc2=str(read2.seq[:6])
                r1t=read1[6:]
                    #http://en.wikipedia.org/wiki/FASTQ_format
                    #@HWUSI-EAS100R:6:73:941:1973#0/1
                    #@EAS139:136:FC706VJ:2:2104:15343:197393 1:Y:18:ATCACG
                r1t.id = "#".join([r1t.id,bc1]) + "/1"
                r2t=read2[6:]
                r2t.id = "#".join([r2t.id,bc2]) + "/2"
                    #Use description and annotation fields to store bc
                r1t.annotations['bc'] = bc1
                r1t.annotations['amplicon'] = amplicon
                r2t.annotations['bc'] = bc2
                r2t.annotations['amplicon'] = amplicon
                return (sample,amplicon,ULSO,DLSO,r1t,r2t,bc1,bc2)
            else:
                return False
    else:
        return False
    """
    if len(d1)>0 and len(d2)>0:
        print sample
        print amplicon
        print read1.name, read2.name
        print 'R1', d1, read1.seq
        print 'R2', d2, read2.seq
    """


def parse_fastq_name(f,directory):
    """ """
    n = f.split(".fastq.gz")[0]
    id,sample,lane,read,other = n.split("_")
    d = {"path":os.path.join(directory,f),"id":id,"sample":sample, "lane":lane, "read":read}
    return d

def fastq_file_generator(fastq_folder):
    """ """
    for f in sorted(os.listdir(fastq_folder)):
        if f.endswith(".fastq.gz"):
            f = parse_fastq_name(f,fastq_folder)
            yield f

def fastq_pair_generator(fastq_folder):
    """ """
    fastq_iter = fastq_file_generator(fastq_folder)
    for i in fastq_iter:
        j = fastq_iter.next()
        if i['id'] == j['id'] and i['sample'] == j['sample'] and i['read'] != j['read']:
            yield {'id':i['id'],'sample':i['sample'],i['read']:i['path'],j['read']:j['path']}


def read_fastq_pair(pair):
    """ """
    #print pair
    handle1 = gzip.open(pair['R1'], "r")
    handle2 = gzip.open(pair['R2'], "r")
    reads1 = SeqIO.parse(handle1, "fastq")
    reads2 = SeqIO.parse(handle2, "fastq")
    for read1, read2 in itertools.izip( reads1, reads2 ):
        yield (read1,read2)
    handle1.close()
    handle2.close()
    

def print_plot(args):
    A = SampleSheet(args.manifestA)
    B = SampleSheet(args.manifestB)
    
    manifests = {'A':A,'B':B}
    
    R = RunSheet(args.run_sample_sheet)
    fastq_dir = args.fastq
    
    res = []
    for i in fastq_pair_generator(fastq_dir):
        if i['sample'] != 'S0':
            s_res = []
            n=0
            #s = Sample()
            #m = R.get_manifest(i['sample']).upper()
            #primers = [(j.Target_ID,j.ULSO_Sequence,j.DLSO_Sequence) for j in  manifests[m].Probes.values()]
            for rp1,rp2 in read_fastq_pair(i):
                s_res.append("_".join([str(rp1.seq[:6]),str(rp2.seq[:6])]))
                n += 1
            c = Counter(s_res)
            sorted_c = sorted(c.iteritems(), key=operator.itemgetter(1), reverse=True)
            #print sorted(Counter([str(z[1]) for z in sorted_c]).items(),key=operator.itemgetter(1), reverse=False)
            print i['sample'], n, len(sorted_c)
            ofh = open(os.path.join(args.res,i['sample'] + '.txt'),'w')
            for k,v in sorted_c:
                ofh.write("\t".join([k,str(v)]) + "\n")
            ofh.close()
            plot_hist([z[1] for z in sorted_c], i['sample'],os.path.join(args.res,i['sample'] + '.png'))
            res += s_res
                #for p in primers:
                    #get_primers_location(i['sample'],p[0],p[1],p[2],rp1,rp2)
                    #if primer.match == True:
                    #   print location
                    #df=pandas.DataFrame(columns=["A","B","C"])
                    #df1=df1.append(pandas.DataFrame([[1,2,3],],columns=["A","B","C"])) #http://pandas.pydata.org/pandas-docs/stable/merging.html#appending-rows-to-a-dataframe
                    #http://pandas.pydata.org/pandas-docs/stable/io.html#io-hdf5
    tc = Counter(res)
    sorted_tc = sorted(tc.iteritems(), key=operator.itemgetter(1), reverse=True)
    #print sorted(Counter([str(y[1]) for y in sorted_tc]).items(),key=operator.itemgetter(1), reverse=False)
    ofh = open(os.path.join(args.res, 'total.txt'),'w')
    for k,v in sorted_tc:
        ofh.write("\t".join([k,str(v)]) + "\n")
    ofh.close()
    plot_hist([z[1] for z in sorted_c], i['sample'],os.path.join(args.res,'total.png'))

    
def main(args):

    A = SampleSheet(args.manifestA)
    B = SampleSheet(args.manifestB)
    
    manifests = {'A':A,'B':B}
    
    R = RunSheet(args.run_sample_sheet)
    fastq_dir = args.fastq
    
    store = HDFStore('store.h5')
    df = DataFrame(columns=["sample","amplicon","ULSO", "DLSO","read1","read2","bc1","bc2","jbc"])
    store['df'] = df
    
    for i in fastq_pair_generator(fastq_dir):
        if i['sample'] != 'S0':
            m = R.get_manifest(i['sample']).upper()
            primers = [(j.Target_ID,j.ULSO_Sequence,j.DLSO_Sequence) for j in  manifests[m].Probes.values()]
            print i['sample']
            for rp1,rp2 in read_fastq_pair(i):
                for p in primers:
                    res = get_primers_location(i['sample'],p[0],p[1],p[2],rp1,rp2)
                    if isinstance(res,tuple) and len(res) == 8:
                        sample,amplicon,ULSO,DLSO,r1t,r2t,bc1,bc2 = res
                        f = DataFrame([[sample,amplicon,ULSO,DLSO,r1t.id,r2t.id,bc1,bc2,"_".join([bc1,bc2])],],columns=["sample","amplicon","ULSO", "DLSO","read1","read2","bc1","bc2","jbc"])
                        df = df.append(f) #http://pandas.pydata.org/pandas-docs/stable/merging.html#appending-rows-to-a-dataframe
                        break
                        #http://pandas.pydata.org/pandas-docs/stable/io.html#io-hdf5


if __name__ == '__main__':
    parser = argparse.ArgumentParser("")
    parser.add_argument('--fastq', dest='fastq', required=True, help='Fastq folder.')
    parser.add_argument('--manA', dest='manifestA', required=False, help='Manifest file A.')
    parser.add_argument('--manB', dest='manifestB', required=False, help='Manifest file B.')
    parser.add_argument('--run', dest='run_sample_sheet', required=False, help='Run Sample Sheet.')
    parser.add_argument('--fastq_out', dest='fastq_out_dir', required=False, help='FASTQ out directory.')
    parser.add_argument('--res', dest='res', required=False, help='FASTQ out directory.')
    parser.add_argument('--cores', dest='cores', required=False, help='Number of cores to use', type=int)
    args = parser.parse_args()
    main(args)