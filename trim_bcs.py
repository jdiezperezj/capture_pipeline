#!/usr/bin/env python

import os
import re
import argparse
import gzip

from Bio import SeqIO
from Bio.Seq import Seq

from numpy import mean

from multiprocessing import Pool

from json import dumps

from pandas import DataFrame

import matplotlib.pyplot as plt


BASES = ["G","C","A","T","N"]  


def count_bases(sequence):
    res = {}   
    for i in BASES:
        res[i] = sequence.count(i)/float(len(sequence))
    return res


def trim_bc(read,rn,modify="id"):
    """ """
    bc = str(read.seq[:6])
    bases = count_bases(bc)
    read = read[6:]
    if modify == "description":
        read.description = ":".join([read.description,bc])
    elif modify == "id":
        read.id = "#".join([read.id,bc]) + "/" + rn
    else:
        raise ValueError
    return read,bases


def hide_bc(read):
    """ """
    bc = str(read.seq[:6])
    bases = count_bases(bc)
    #read = read[6:]
    read.letter_annotations.values()[0][:6] = [0,] * 6
    r.description = " ".join([r.description,str(r.seq[:6])])
    return read,bases,bc



def fastq_file_generator(fastq_folder,out_folder,tag="trimmed"):
    """ """
    for f in sorted(os.listdir(fastq_folder)):
        if f.endswith(".fastq.gz"):
            basename = f.split(".")[0]
            f = os.path.join(fastq_folder, f)
            o = os.path.join(out_folder, basename + "." + tag + ".fastq.gz")
            yield basename,f,o

def read_fastq(filename):
    """ """
    #print pair
    base,head = os.path.split(filename)
    rn = head.split("_")[3][1]
    handle = gzip.open(filename, "r")
    reads = SeqIO.parse(handle, "fastq")
    for read in reads:
        yield trim_bc(read,rn)
    handle.close()
    
def convert_reads(inp):
    basename,f,o = inp
    contents = {"A":[],"T":[],"G":[],"C":[],"N":[]}
    ofh = gzip.open(o, 'wb')
    for r,b in read_fastq(f):
        ofh.write(r.format("fastq"))
        for k,v in b.items():
            contents[k].append(v)
    ofh.close()
    for key,value in contents.items():
        contents[key] = mean(value)
    return basename,contents


def main(args):
    #pool = Pool(processes=args.cores)
    res = {}
    for basename,f,o in fastq_file_generator(args.fastq,args.fastq_out):
        print "Analyzing: {n}".format(n=basename)
        contents = {"A":[],"T":[],"G":[],"C":[],"N":[]}
        ofh = gzip.open(o, 'wb')
        for r,b in read_fastq(f):
            ofh.write(r.format("fastq"))
            for k,v in b.items():
                contents[k].append(v)
        ofh.close()
    
    #result = pool.apply_async(convert_reads,input_list)
        for b in BASES:
            contents[b] = mean(contents[b])
            print b,": ",str(contents[b])
        res[basename] = contents
    #Save res as JSON
    j = dumps(res, indent=4)
    fj = open(os.path.join(args.fastq_out,'base_content.json'), 'w')
    print >> fj, j
    fj.close()
    #Transform res into DF and perform plots and calculations.
    total = [[res[k],] + [res[k][j] for j in list("ATGCN")] for k in res.keys()]
    columns = ['sample',] + list("ATGCN")
    df = DataFrame(total,columns=columns)
    df = df.set_index("sample")
    df.boxplot()
    fig = plt.gcf()
    fig.savefig(os.path.join(args.fastq_out,args.name + '.png'))


if __name__ == '__main__':
    parser = argparse.ArgumentParser("")
    parser.add_argument('--fastq', dest='fastq', required=True, help='Fastq folder.')
    parser.add_argument('--fastq_out', dest='fastq_out', required=False, help='FASTQ out directory.')
    parser.add_argument('--name', dest='name', required=True, help='Fastq folder.')
    parser.add_argument('--cores', dest='cores', type=int, required=False, help='Number of cores to use')
    args = parser.parse_args()
    main(args)