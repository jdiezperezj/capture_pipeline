#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import argparse
import vcf
import csv
import hashlib
import multiprocessing
from string import upper


def get_hash(word):
    """ Returns hash from input string. """
    word = unicode(word)
    hash_object = hashlib.md5(word)
    return hash_object.hexdigest()

def format_chr(chr):
    """ Format chromosome as two digit str for autosomes. """
    chr = chr[3:] if chr.startswith('chr') else chr
    try:
	chr = format(int(chr), '02d')
    except Exception,e:
	return chr
    else:
	return chr


def read_coords_file(filename, fieldnames = ["chrom","name","start","end","ref","alt","mutation_type","cosmic_id"]):
    """ """
    csv.register_dialect('TSV', delimiter='\t',doublequote=False,lineterminator='\n',quoting=csv.QUOTE_NONE)
    with open(filename,'r') as f:
        reader = csv.DictReader(f, fieldnames=fieldnames, dialect='TSV')
        for row in reader:
            fields = map(str,[format_chr(row['chrom']), row['start']])
            h = get_hash("_".join(fields))
            yield (h,fields)


def filter_vcf(vcf_file,hotspots,outdir):
    """ """
    dir,name = os.path.split(vcf_file)
    vcf_reader = vcf.Reader(filename=vcf_file)
    name,ext = name.split(".",1)
    vcf_w = ".".join([name,"cns.filtered.vcf"])
    vcf_w = os.path.join(outdir,vcf_w)
    with open(vcf_w, 'w') as f:
        vcf_writer = vcf.Writer(f, vcf_reader)
        coords = dict([(k,v) for k,v in read_coords_file(hotspots)])
        keys = coords.keys()
        for record in vcf_reader:
            fields = map(str,[format_chr(record.CHROM), record.POS])
            h = get_hash("_".join(fields))
            if record.samples[0].is_variant == True:
                vcf_writer.write_record(record)
                print record
            else:
                if h in keys: #would be a hotspot
                    print record
                    vcf_writer.write_record(record)
    return name


def filter_vcf_folder(arguments):
    """ """
    return filter_vcf(arguments[0],arguments[1],arguments[2])


def main(args):
    if args.out_dir == False:
        args.out_dir = args.input
    if os.path.isfile(args.input):
        try:
            filter_vcf(args.input, args.hotspots, args.out_dir)
        except Exception,e:
            print e
    elif os.path.isdir(args.input):
        data = sorted([(os.path.join(args.in_dir,i),args.hotspots,args.out_dir) for i in os.listdir(args.in_dir) if i.endswith(args.file_ext)])
        pool = multiprocessing.Pool(args.cores)
        for name in pool.imap_unordered(filter_vcf_folder, data):
            print "\t".join([name,"Done"])


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description = 'VCF filtering based on coordinate file.')
    parser.add_argument('-i','--input', required = True, help = 'Original folder.')
    parser.add_argument('-e','--file_ext', required = False, default='.vcf.gz', help = 'File extension.')
    parser.add_argument('-s','--hotspots', required = True, help = 'TSV file that contains hotspots.')
    parser.add_argument('-o','--out_dir', required = False, default=False, help = 'Output folder.')
    parser.add_argument('-c','--cores', required = False, default = 6, help = 'Number of cores used.')
    args = parser.parse_args()
    main(args)