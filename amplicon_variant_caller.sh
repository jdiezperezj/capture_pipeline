#!/usr/bin/env bash

#Scripts takes the following arguments:
#variant_folder, interval_file, name,  path
args=("$@")
echo ${args}

VARIANT_FOLDER=${args[0]}
INTERVALS=${args[1]}
BASENAME=${args[2]}
FILENAME=${args[3]}
HG19=/home/jdiezperez/data/references/hg19/hg19.fa


SAMTOOLS=/opt/samtools-0.1.19/samtools

VCF_LOG="${VARIANT_FOLDER}/${BASENAME}.log"
VCF_SNP="${VARIANT_FOLDER}/${BASENAME}.snp.vcf"
VCF_INDEL="${VARIANT_FOLDER}/${BASENAME}.indel.vcf"
VCF_CONCAT="${VARIANT_FOLDER}/${BASENAME}.concat.vcf"
VCF_CONCAT_SORTED="${VARIANT_FOLDER}/${BASENAME}.vcf.gz"
ONCOTATOR="${VARIANT_FOLDER}/${BASENAME}.tsv"
VEP_VCF="${VARIANT_FOLDER}/${BASENAME}.vep.vcf"

/opt/samtools-0.1.19/samtools mpileup -l $INTERVALS -f ${HG19} ${FILENAME} | java -jar /opt/VarScan.v2.3.6.jar mpileup2snp --variants --output-vcf 1 --min-avg-qual 25 --min-coverage 100 --min-reads2 5 --min-var-freq 0.01 --p-value 0.001 1> ${VCF_SNP} 2> ${VCF_LOG};

/opt/samtools-0.1.19/samtools mpileup -l $INTERVALS -f ${HG19} ${FILENAME} | java -jar /opt/VarScan.v2.3.6.jar mpileup2indel --variants --output-vcf 1 --min-avg-qual 25 --min-coverage 100 --min-reads2 5 --min-var-freq 0.01 --p-value 0.001 1> ${VCF_INDEL} 2>> ${VCF_LOG} ;

vcf-concat ${VCF_SNP} ${VCF_INDEL} 1> ${VCF_CONCAT} 2>> ${VCF_LOG};

cat ${VCF_CONCAT} | vcf-sort | gzip -c9 1> ${VCF_CONCAT_SORTED} 2>> ${VCF_LOG};

#oncotator -o SIMPLE_TSV -i VCF --db-dir /home/jdiezperez/data/annotation/oncotator/oncotator_v1_ds_June112014  ${VCF_CONCAT_SORTED} ${ONCOTATOR} hg19;

perl /home/jdiezperez/data/annotation/vep/ensembl-tools-release-75/scripts/variant_effect_predictor/variant_effect_predictor.pl --cache --refseq --canonical --everything --database --vcf --force_overwrite -i ${VCF_CONCAT_SORTED} -o ${VEP_VCF} ;

gzip ${VEP_VCF} ;

#rm  ${VARIANT_FOLDER}/${BASENAME}.html ;

#rm  ${VARIANT_FOLDER}/${BASENAME}*.vcf ;
