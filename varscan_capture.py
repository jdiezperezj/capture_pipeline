#!/usr/bin/env python

import os
import subprocess
import pysam
import argparse
import multiprocessing



def bam_file_generator(bamfolder,end=".bam"):
    """ """
    for f in sorted(os.listdir(bamfolder)):
        if f.endswith(end):
            name = f.split(".")[0]
            p = os.path.join(bamfolder,f)
            yield (name,p)

def call_variants(f):
    """ """
    variant_folder, interval_file, name, path = f
    cmd = ["capture_variant_caller.sh", variant_folder, interval_file, name, path]

    subprocess.check_call(cmd)
    return (name,"Done")


def main(args):
    pool = multiprocessing.Pool(args.cores)
    files = [(args.var,args.int,n,p) for n,p in bam_file_generator(args.aln)]
    for k,v in pool.imap_unordered(call_variants, files):
        print "\t".join([k,v])
    for i in os.listdir(args.var):
        if i.endswith('.vcf'):
            os.remove(os.path.join(args.var,i))

if __name__ == '__main__':
    parser = argparse.ArgumentParser("")
    parser.add_argument('--aln', dest='aln', required=False, help='Alignment out directory.')
    parser.add_argument('--int', dest='int', required=False, help='Intervals out file.')
    parser.add_argument('--var', dest='var', required=False, help='Variant out directory.')
    parser.add_argument('--cores', dest='cores', type=int, default=5, required=False, help='Number of cores to use')
    args = parser.parse_args()
    main(args)

#!/usr/bin/env python

