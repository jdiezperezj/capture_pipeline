#!/usr/bin/env python

import os
import argparse
import multiprocessing
import vcf
import pybedtools
import pandas

"""
#pybedtools.BedTool.slop -> l=1, r=1
#pybedtools.BedTool.nucleotide_content -pattern
chrom
start
stop
name
score
strand
1) %AT content
2) %GC content
3) Number of As observed
4) Number of Cs observed
5) Number of Gs observed
6) Number of Ts observed
7) Number of Ns observed
8) Number of other bases observed
9) The length of the explored sequence/interval.
10) The seq. extracted from the FASTA file. (opt., if -seq is used)
11) The number of times a user's pattern was observed.(opt., if -pattern is used.)

"""

def process_vcf(filename):
    """ """
    vcf_records = [record for record in vcf.Reader(open(filename, 'r'))]
    result = []
    for i in vcf_records:
	for s in range(len(i.samples)):
	    s = i.samples[s]
	    if s.is_variant:
                if snp:
                    results.append("\t".join(map(str,[s.site.CHROM, int(s.site.POS) - 2, int(s.site.POS) + 1])))    #VCF is 1 based and Bed is 0 based, so to get -1 and +1 coords, we do -2 and +1
    bed = pybedtools.BedTool("\n".join(results), from_string=True)
    return bed

def get_nucleotides(name,bed,fasta):
    """ """
    nuc_content = bed.nucleotide_content(fi=fasta,seq=True)
    header = ['chrom','start','stop','name','AT','GC','SEQ']
    nuc_list = [[i[0],i[1],i[2],i[3],i[6],i[7],i[15]] for i in nuc_content]
    nuc_list = pandas.DataFrame(nuc_list,columns=header)
    return nuc_list
    
def get_genome_context(arguments):
    name,vcf,genome,output = arguments
    bed = process_vcf(filename)
    return get_nucleotides(name,bed,genome)
        

def main(args):
    files = [[i,os.path.join(args.input,i),args.genome,args.output] for i in os.listdir(args.input) if i.endswith('.vcf')]
    pool = multiprocessing.Pool(args.cores)
    res =  False
    for i,f in enumerate(pool.imap_unordered(get_genome_context, files)):
        if i == 0:
            res = f
        else:
            res = pandas.concat([res,f], axis=0)
    pv.to_csv(os.path.join(args.output,args.run_name + ".csv"))


if __name__ == '__main__':
    parser = argparse.ArgumentParser("Get genome context from vcf file directory.")
    parser.add_argument('--input', dest='input', required=False, help='Input directory.')
    parser.add_argument('--output', dest='output', required=False, help='Out directory.')
    parser.add_argument('--genome', dest='genome', required=False, help='Extension of TSV files to be parsed.')
    parser.add_argument('--run_name', dest='run_name', required=False, help='Run name.')
    parser.add_argument('--cores', dest='cores', type=int, default=5, required=False, help='Number of cores to use.')
    args = parser.parse_args()
    main(args)
