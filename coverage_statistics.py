#!/usr/bin/env python

import os
import multiprocessing
from subprocess import Popen, PIPE
from subprocess import communicate
import pandas
import argparse
import bedtools
import numpy
from operator import itemgetter

"""
QUALITY CHECK REPORT WILL CONTAIN 3 PARTS:
1.- FILE GENERAL STATISTICS TABLE: reads, duplicates, mapped, ... including percentages.
2.- ON-TARGET FEATURES GENERAL STATISTICS TABLE: reads per feature.
3.- OFF-TARGET FEATURES GENERAL STATISTICS TABLE: reads per chromosome.
    ** Intersect regions with high coverage with gtf -> off target features.
**JOIN WITH PREVIOUS REPORT:
*4.- RUN COVERAGE REPORT.
*5.- FEATURES REPORT (usually by exon).
*6.- AGGREGATION REPORT (usually by gene).

#THINGS TO ADD:
    #READS BASE QUALITY PLOT ..
    #ALIGNMENT BASE QUALITY
    #GC CONTENT
    #FEATURES OF NON MAPPED READS -> ...
    #FEATURES OF LOCI WITH HIGH COVERAGE OUT OF TARGETED REGIONS
    #CONTAMINATION AND CLONALITY ??? WITH VCF ANALYSIS ???
"""



def exe_f(command, shell=True):
    """Function to execute a command and return stuff"""
    process = Popen(command, shell=shell, stdout=PIPE, stderr=PIPE)
    (stdout, stderr) = process.communicate()
    return process, stderr, stdout


def get_flagstats(bamfile,samplename,output='df'):
    """
    Simple read mapping statistics.
    samplename in_total,duplicates ... pc_mapped, pc_duplicates,pc_singletons
    """
    p,e,o = exe_f("samtools flagstats {f}".format(f=bamfile))
    r = [i.split(" + ",1) for i in o.split("\n")]
    #r = [[j[1].split("0 ")[1].split("(")[0].rstrip().replace(" ", "_"), int(j[0])] for j in r if len(j)>1]
    r = [["_".join(j[1].split("(")[0].split()[1:]), int(j[0])] for j in r if len(j)>1]
    df = DataFrame(r,columns=["Param",samplename,])    #Change reads per sample name.
    df = df.set_index(['Param'])
    df = df.transpose()

    #add percentages -> mapped,duplicates, ...
    pc_mapped = round((df[samplename]['mapped']/float(df[samplename]['in_total']))*100, 2)
    pc_singletons = round((df[samplename]['singletons']/float(df[samplename]['in_total']))*100, 2)
    if df[samplename]['duplicates'] != 0:
        pc_duplicates = round((df[samplename]['duplicates']/float(df[samplename]['in_total']))*100, 2)
    else:
        pc_duplicates = 0
    
    #The returned dataFrame include percentages
    pandas.DataFrame([samplename,pc_mapped,pc_duplicates,pc_singletons], columns=['samplename', 'pc_mapped', 'pc_duplicates', 'pc_singletons'])
    df = df.set_index(['samplename'])
    
    #Returns a dictionary or a dataFrame.
    if output == 'dict':
        return df.to_dict()[samplename]
    elif output == 'df':
        return df
    else:
        raise ValueError("Unrecognized value for output parameter.")


def get_on_target(bamfile,bedfile):
    """ Get on target coverage, but can be used for off target as well with complementary bed. """
    #Field 7 in the resulting file is n_reads overlapping a given feature in windows file, when the number of columns in the input bedfile is 6.
    process,stderr,stdout = exe_f("coverageBed -abam {bam} -b {bed} | awk '{ SUM += $7 } END { print SUM }' ".format(bam = bamfile, bed = bedfile))
    result = int(stdout.rstrip())
    return result


def get_off_target(bamfile,bedfile):
    """ Returns offtarget coverage. Reads computed only if they fully overlap with the complementary intervals. """
    #Field 7 in the resulting file is n_reads overlapping a given feature in windows file, when the number of columns in the input bedfile is 6.
    process,stderr,stdout = exe_f("intersectBed -abam {bam} -b {bed} -f 1.0 | coverageBed -a stdin -b {bed}| awk '{ SUM += $7 } END { print SUM }'".format(bam = bamfile, bed = bedfile))
    result = int(stdout.rstrip())
    return result


def bam_read_count(bamfile):
    """ Return a tuple of the number of mapped and unmapped reads in a bam file """
    #p = Popen(['samtools', 'idxstats', bamfile], stdout=PIPE)
    process,stderr,stdout = exe_f('samtools idxstats {bamfile}'.format(bamfile=bamfile))
    mapped = 0
    unmapped = 0
    for line in stdout:
        rname, rlen, nm, nu = line.rstrip().split(" + ")
        mapped += int(nm)
        unmapped += int(nu)
    return (mapped, unmapped)


def bam_flagstat_parser(bamfile):
    """ Return a tuple of the number of mapped and unmapped reads in a bam file """
    #p = Popen(['samtools', 'flagstat', bamfile], stdout=PIPE)
    process,stderr,stdout = exe_f('samtools flagstat {bamfile}'.format(bamfile=bamfile))
    for line in stdout:
        rname, rlen, nm, nu = line.rstrip().split(" + ")
        mapped += int(nm)
        unmapped += int(nu)
    return (mapped, unmapped)

def complement_bed(bedfile,genome='hg19',flank=75):
    """ """
    b = pybedtools.BedTool(bedfile)
    b = b.flank(genome=genome, b=flank)
    b = b.merge()
    return b.complement(genome=genome)

def group_by_chromosome(coverage):
    """ """
    #return df.groupby('CHR')['ONTARGET'].agg({'readsnum' : np.sum, 'readsmean' : np.mean})  #http://pandas.pydata.org/pandas-docs/dev/groupby.html
    return df.groupby('CHR')['ONTARGET'].agg({'readsnum' : np.sum,})

def count_ontarget_reads(bamfile,bedfile):
    """ On target reads. """
    bam = pybedtools.BedTool(bamfile)
    bed = pybedtools.BedTool(bedfile)
    cols = ['CHR', 'START', 'STOP', 'LOCUS', 'SCORE','STRAND' 'ONTARGET','COV1X', 'LENGTH', 'FRACTION']
    cov_data = [[i.fields[0], numpy.int(i.fields[1]), numpy.int(i.fields[2]), i.fields[3], i.fields[4], i.fields[5], numpy.int(i.fields[6]), numpy.int(i.fields[7]), numpy.int(i.fields[8]), numpy.float(i.fields[9])] for i in bam.coverage(bed)]
    df = pandas.DataFrame(cov_data,columns = cols)
    total_ontarget = df.ONTARGET.sum()
    return (total_ontarget,df)


def count_reads_chrom(bamfile,bedfile):
    """ """
    cbed = complement_bed(bedfile)
    ot = count_ontarget_reads(bamfile,cbed)
    rbychrom = group_by_chromosome(ot[1])


def master_analytics(parameters):
    """ """
    md = os.path.join(parameters['input'],parameters['id']) + ".md.bam"
    rd = os.path.join(parameters['input'],parameters['id']) + ".rd.bam"
    #flagstats for both.
    md_flags = get_flagstats(md,parameters['id'])
    rd_flags = get_flagstats(rd,parameters['id'])
    #ontarget for md and rd.
    md_on_target = count_ontarget_reads(md,parameters['bed'])
    rd_on_target = count_ontarget_reads(rd,parameters['bed'])
    #add ontarget percentages to flags
    df = pandas.DataFrame(['on_target',md_on_target[1]],columns=["Param",parameters['id']])
    df.set_index("Param")
    md_flags = pandas.concat([md_flags,df],index=0)
    df = pandas.DataFrame(['on_target',rd_on_target[1]],columns=["Param",parameters['id']])
    df.set_index("Param")
    rd_flags = pandas.concat([rd_flags,df],index=0)
    #per chromosome for md and rd.
    md_reads_by_chrom = count_reads_chrom(md,bedfile)
    rd_reads_by_chrom = count_reads_chrom(rd,bedfile)
    
    d_md = {'flags':md_flags, 'on_target':md_on_target, 'off_target':md_reads_by_chrom}
    d_rd = {'flags':rd_flags, 'on_target':rd_on_target, 'off_target':rd_reads_by_chrom}
    #Concat both dfs
    
    return {'rd':d_rd,'md':d_md}


def main(args):
    ids = [{"id":i.split(".")[0],"input":args.aln,"bed":args.bed,"genome":args.genome,"output":args.out} for i in os.listdir(args.input) if i.endswith('.md.bam')]
    md = [[i.split(".")[0], os.path.join(args.input,i)] for i in os.listdir(args.input) if i.endswith('.md.bam')]
    md = sorted(md, key = itemgetter(0))
    rd = [[i.split(".")[0], os.path.join(args.input,i)] for i in os.listdir(args.input) if i.endswith('.rd.bam')]
    rd = sorted(rd, key = itemgetter(0))
    arguments = [[j, args.bed, args.out] for j in zip(md,rd)]
    for i,f in enumerate(pool.imap_unordered(master_analytics, files)):
        if i == 0:
            md_flags = f['md']['flags']
            rd_flags = f['rd']['flags']
            md_on_target = f['md']['on_target']
            rd_on_target = f['rd']['on_target']
            md_off_target = f['md']['off_target']
            rd_off_target = f['rd']['off_target']
        else:
            #concat flagstats per sample
            md_flags = pandas.concat([md_flags,f['md']['flags']], index=0)
            rd_flags = pandas.concat([rd_flags,f['rd']['flags']], index=0)
            #on_target
            md_on_target = pandas.concat([md_on_target,f['md']['on_target']], index=0)
            rd_on_target = pandas.concat([rd_on_target,f['rd']['on_target']], index=0)
            #off_target
            md_off_target = pandas.concat([md_off_target,f['md']['off_target']], index=0)
            rd_off_target = pandas.concat([rd_off_target,f['rd']['off_target']], index=0)
    #pivot reads on target per locus -> heatmap
    #pivot reads off target per chromosome -> ordered boxplot or violin plot
    #?? plot off target chromosome coverage ??

    
if __name__ == '__main__':
    parser = argparse.ArgumentParser("")
    parser.add_argument('--aln', dest='aln', required=False, help='Alignment out directory.')
    parser.add_argument('--bed', dest='bed', required=False, help='Bed file. 6 Columns bed file where 4th represent exons and 6th gene names.')
    parser.add_argument('--genome', dest='genome', required=False, help='Genome reference file.')
    parser.add_argument('--out', dest='out', required=False, help='Out')
    parser.add_argument('--cores', dest='cores', type=int, default=5, required=False, help='Number of cores to use')
    args = parser.parse_args()
    main(args)