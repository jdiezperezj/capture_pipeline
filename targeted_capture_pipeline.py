#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import argparse
import shutil
import subprocess
from datetime import datetime
from hashlib import sha1
from pandas import DataFrame
import logging

class IlluminaSampleSheet(object):
    """ Illumnia SampleSheet.csv files parser. Tested functional for both Amplicon and Capture SampleSheets on 04/11/2014.\n"""
    
    def __init__(self, filename, sep=','):
        """ """
        self.filename = filename
        self.sep = sep
        self.headers = ['Data', 'Settings', 'Reads', 'Manifests', 'Header']
        self.data = self.process_sample_sheet() #Dict where data is DF, and the rest of headers are lists of list.
        self.__dict__.update(self.data)

    def read_reverse(self, ):
        """ Return a reversed list of lines from the original file. """
        with open(self.filename, 'r') as ifh:
            for line in ifh.readlines()[::-1]:
                yield line.rstrip().split(',')
    
    def process_sample_sheet(self, ):
        """ Process the file lines in reverse order and returns a dictionary of SampleSheet values. """
        buff = []
        res = {}
        
        get_buff = lambda x: [b for b in x[::-1] if not all([ len(i) == 0 for i in b])]
        
        for line in self.read_reverse():
            if not line[0].startswith('['):
                buff.append(line)

            else:
                name = line[0][1 : -1]  #we get rid of braces.
                if name == 'Data':
                    header = buff[-1]
                    data = [b for b in buff[:-1][::-1] if len(b) == len(header) and not all([ len(i) == 0 for i in b])]
                    try:
                        df = DataFrame(data, columns=header)
                    except Exception,e:
                        print "SampleSheet parsing failed: {e}".format(e=e)
                    else:
                        res[name] = df
                        buff = []

                elif name == 'Header':
                    res[name] = dict([(i[0],i[1]) for i in get_buff(buff)] )
                    buff = []

                elif name == 'Reads':
                    res[name] = [i[0] for i in get_buff(buff)]
                    buff = []

                elif name in ('Settings','Manifests'):
                    res[name] = dict([(i[0],i[1]) for i in get_buff(buff)])
                    buff = []

                else:
                    raise(ValueError,'Section header unrecognized: {n}'.format(n=name))
                    
        return res
    
    def n_samples(self,):
        """ Get number of samples. """
        try:
            length = len(self.Data)
        except Exception,e:
            print "Data seems not defined: {e}".format(e=e)
        else:
            return length
        
    def get_samples(self,):
        """ Get sample ids"""
        return sorted(self.Data.Sample_ID.tolist())
    
    def get_reads_length(self,):
        """ Get design read length. """
        if len(self.Reads) == 2:
            return (map (int, self.Reads))
        else:
            raise ValueError('Reads length is not equal to 2. Check SampleSheet content.')


class PairError(Exception):
    """ It catch error when number of samples in a directory is not pair. """
    def __init__(self, directory,value):
        print "PairDirectoryError: {directory} contains {value} fastq files. ".format(directory=directory, value=value)


class SampleNumberError(Exception):
    """ Catch files contained in a given directory is different from the files stated in the original SampleSheet file. """
    def __init__(self, directory, fnsamples, onsamples):
        print "SampleNumberError: {directory} contains {value} samples, but SampleSheet stated {evalue}. ".format(directory=directory, value=fnsamples, evalue=onsamples)


class Targeted_Capture_Pipe(object):
    """ """
    def __init__(self,indir,outdir,index,reference,bedfile,intervals,groups_file,hotspots, runname, cores=12, fastq_ext='fastq.gz', seed='cmp_mdx'):
        self.in_dir = indir
        self.out_dir = outdir
        self.index = index
        self.reference = reference #reference fasta file.
        self.bedfile = bedfile
        self.intervals = intervals
        self.groups_file = groups_file
        self.hotspots = hotspots
        self.run_name = runname
        self.cores = cores
        self.fastq_ext = fastq_ext
        self.run_output_dir = os.path.join(self.out_dir,self.run_name)
        self.dirs = {'Alignments':False, 'Variants':False, 'Stats':False}
        self.analysis_date = str(datetime.today())
        self.analysis_id = sha1(";".join([seed, self.run_name, self.analysis_date])).hexdigest()

    def get_fastq_files(self,):
        """ Get fastq file list. """
        return sorted([i for i in os.listdir(self.in_dir) if i.endswith(self.fastq_ext)])

    def get_samples(self, ):
        """ Get samples. """
        return sorted(list(set(["_".join(f.split('_')[:2]) for f in self.get_fastq_files()])))

    def check_fastq_dir(self,):
        """ Check fastq directory. """
        ss = os.path.join(self.in_dir,'SampleSheet.csv')
        if os.path.isfile(ss):
            try:
                iss = IlluminaSampleSheet(ss)
            except Exception,e:
                 "Error detected while checking fastq directory: {e}".format(e=e)
            else:
                sample_number = iss.n_samples()
                
        #self.original_dir_length = len([i for i in os.listdir(self.in_dir) if i.endswith(self.fastq_ext)])
        n_fastq_files = len(self.get_fastq_files())
        n_samples = len(self.get_samples())
        
        if n_fastq_files % 2 != 0:
            raise PairError(self.in_dir,self.original_dir_length)
        
        elif n_samples != sample_number + 1:
            print iss.n_samples()
            print sample_number
            print self.get_fastq_files()
            raise SampleNumberError(self.in_dir, n_samples, sample_number)
        else:
            return True

    def create_dir_tree(self, ):
        """ Create a new dictory tree hierarchy.  """
        for key,value in self.dirs.items():
            p = os.path.join(self.run_output_dir,key)
            if os.path.exists(p):
                raise IOError('Run analysis folder {d} already exists.'.format(d=p))
                sys.exit()
            else:
                try:
                    os.makedirs(p)
                except Exception,e:
                    print e
                else:
                    self.dirs[key] = p
        return True
        
    def create_logger(self, ):
        """ Create log file for the main pipeline script. """
        #For more information: http://docs.python.org/2/howto/logging
        self.logger = logging.getLogger('Capture Panel Pipeline Logger')
        self.logger.setLevel(logging.DEBUG)
        # create file handler which logs even debug messages
        self.log_file = os.path.join(self.run_output_dir, 'capture_panel_pipeline.log')
        open(self.log_file, 'w').close()
        
        fh = logging.FileHandler(self.log_file,mode='a',encoding=None)
        fh.setLevel(logging.DEBUG)
        fh.setLevel(logging.INFO)#
        
        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.ERROR)
        ch.setLevel(logging.INFO)#
        
        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        ch.setFormatter(formatter)
        fh.setFormatter(formatter)
        
        # add the handlers to logger
        self.logger.addHandler(ch)
        self.logger.addHandler(fh)


    def execute_subprocess(self, cmd, step):
        """ """
        try:
            process = subprocess.Popen(cmd, shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            (stdout, stderr) = process.communicate()
        except Exception,e:
            self.logger.error('Exception: {e}, while executing commad {cmd} for step {st}'.format(e=e, cmd=" ".join(cmd), st=step))
        else:
            self.logger.info('Executed commad {cmd} for {st}'.format(cmd=" ".join(cmd), st=step))
            return process, stderr, stdout
        
    def create_readme_file(self,):
        """ Create a readme file in the main analysis folder. """
        fname = os.path.join(self.run_output_dir, 'README.txt')
        ofh = open(fname, 'w')
        text = "Analysis ID: {id} .\nAnalysisDate: {dt} .\n".format(id = self.analysis_id, dt = self.analysis_date)
        ofh.write(text)
        ofh.close()
        self.logger.info('README.txt has been created.')
        
    def alignment(self,):
        """ """
        self.logger.info('Alignment has started.')
        cmd = ['aligner_capture.py', '--fastq', self.in_dir, '--index', self.index, '--aln', self.dirs['Alignments'], '--bed', self.bedfile, '--cores', '1']
        self.execute_subprocess(cmd, 'alignment')
        self.logger.info('Alignment has finished.')
        

    def qa(self):
        """ """
        self.logger.info('QA has started.')
        cmd = ['coverage_hist.py', '-i', self.dirs['Alignments'], '-o', self.dirs['Stats'], '-b', self.bedfile, '--run_name',  self.run_name, '--cores', str(self.cores)]
        self.execute_subprocess(cmd, 'qa - coverage_hist')

        cmd = ['coverage_dir.py', '-i',  self.dirs['Alignments'], '-o',  self.dirs['Stats'], '-b', self.bedfile, '-r',  self.run_name, '-n', self.cores, '-g', 'FULL_PANEL',  '--gene_group_file', self.groups_file]
        self.execute_subprocess(cmd, 'qa - coverage_dir')

        cmd = ['alignment_general_stats.py', '--aln_dir', self.dirs['Alignments'], '--bed', self.bedfile, '--run', self.run_name, '--out', self.dirs['Stats']] 
        self.execute_subprocess(cmd, 'qa - alignment_general_stats')
        self.logger.info('QA has finished.')
    
    def snv_indel_caller(self,):
        """ """
        self.logger.info('Variant caller has started.')
        cmd = ['varscan_capture_full.py', '--aln', self.dirs['Alignments'], '--var', self.dirs['Variants'],  '--int',  self.intervals,  '--hot', self.hotspots, '--cores', '6']
        self.execute_subprocess(cmd, 'snv_indel_caller')
        self.logger.info('Variant caller has finished.')
    
    def snv_indel_reporter(self,):
        """ """
        self.logger.info('Variant reporter has started.')
        cmd = ['VCF.py', '--var',  self.dirs['Variants'], '--out', self.dirs['Variants'], '--hot',  self.hotspots]
        self.execute_subprocess(cmd, 'snv_indel_reporter')
        self.logger.info('Variant reporter has finished.')
    
    def structural_caller(self):
        """ """
        self.logger.info('Structural caller has started.')
        cmd = ['delly_wrapper.py', '--in_dir', self.dirs['Alignments'], '--ref', self.reference, '--run_name', self.run_name, '--out_dir', self.dirs['Variants']] #to be done
        self.execute_subprocess(cmd, 'structural_caller')
        self.logger.info('Structural caller has finished.')
    
    def execute_pipeline(self,):
        """ """
        if self.check_fastq_dir() and self.create_dir_tree():
            self.create_logger()
            self.create_readme_file()
            #Analysis
            self.alignment()
            self.qa()
            self.snv_indel_caller()
            self.snv_indel_reporter()
            self.structural_caller()


def main(args):
    """ """
    #tcp = Targeted_Capture_Pipe(args.in_dir, args.out_dir,args.run_name,args.index,args.bed_file,args.groups_file,args.hotspots)   #pass arguments
    tcp = Targeted_Capture_Pipe(args.in_dir, args.out_dir, args.index, args.ref, args.bed_file, args.intervals, args.groups_file, args.hotspots, args.run_name)
    tcp.execute_pipeline()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description = 'Targeted capture panel pipeline.')
    parser.add_argument('-i','--in_dir', required = True, help = 'Original FASTQ folder.')
    parser.add_argument('-o','--out_dir', required = False, help = 'Output folder.')
    parser.add_argument('-x','--index', required = True, help = 'Index file.')
    parser.add_argument('-f', '--ref', required = True, help = 'Genome reference file.')
    parser.add_argument('-b','--bed_file', required = True, help = 'Bed file.')    #Interval file is probably needed.
    parser.add_argument('-t','--intervals', required = True, help = 'Intervals file.')  
    parser.add_argument('-g','--groups_file', required = True, help = 'Regions groups file.')    #Interval file is probably needed.
    parser.add_argument('-s','--hotspots', required = True, help = 'Hotspots tabular file.')    #Interval file is probably needed.
    parser.add_argument('-r','--run_name', required = True, help = 'Run name.')
    args = parser.parse_args()
    main(args)
