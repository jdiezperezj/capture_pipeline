#!/usr/bin/env bash

#Scripts takes the following arguments:
#variant_folder, interval_file, name,  path
args=("$@")
echo ${args}

VARIANT_FOLDER=${args[0]}
INTERVALS=${args[1]}
BASENAME=${args[2]}
FILENAME=${args[3]}
HG19=/home/jdiezperez/data/references/hg19/hg19.fa


SAMTOOLS=/opt/samtools-0.1.19/samtools

VCF_LOG="${VARIANT_FOLDER}/${BASENAME}.log"
VCF_SNP="${VARIANT_FOLDER}/${BASENAME}.snp.vcf"
VCF_INDEL="${VARIANT_FOLDER}/${BASENAME}.indel.vcf"
VCF_CONCAT="${VARIANT_FOLDER}/${BASENAME}.concat.vcf"
VCF_CONCAT_SORTED="${VARIANT_FOLDER}/${BASENAME}.vcf.gz"
#VCF_CNS="${VARIANT_FOLDER}/${BASENAME}.cns.vcf"
#VCF_CNS_SORTED="${VARIANT_FOLDER}/${BASENAME}.cns.vcf.gz"
#VCF_CNS_SORTED_FILTERED="${VARIANT_FOLDER}/${BASENAME}.cns.filtered.vcf.gz"
ONCOTATOR="${VARIANT_FOLDER}/${BASENAME}.tsv"

/opt/samtools-0.1.19/samtools mpileup -l $INTERVALS -f ${HG19} ${FILENAME} | java -jar /opt/VarScan.v2.3.6.jar mpileup2snp --variants --output-vcf 1  --strand-filter 1 --min-avg-qual 25 --min-coverage 20 --min-reads2 3 --min-var-freq 0.01 --p-value 0.001 1> ${VCF_SNP} 2> ${VCF_LOG};

/opt/samtools-0.1.19/samtools mpileup -l $INTERVALS -f ${HG19} ${FILENAME} | java -jar /opt/VarScan.v2.3.6.jar mpileup2indel --variants --output-vcf 1 --strand-filter 1 --min-avg-qual 25 --min-coverage 20 --min-reads2 3 --min-var-freq 0.01 --p-value 0.001 1> ${VCF_INDEL} 2>> ${VCF_LOG} ;

#/opt/samtools-0.1.19/samtools mpileup -l $INTERVALS -f ${HG19} ${FILENAME} | java -jar /opt/VarScan.v2.3.6.jar mpileup2cns --strand-filter 1 --output-vcf 1 --min-avg-qual 25 --min-coverage 20 --min-reads2 3 --min-var-freq 0.01 --p-value 0.001 1> ${VCF_CNS} 2>> ${VCF_LOG} ;
#sorted
#cat ${VCF_CONCAT} | vcf-sort | gzip -c9 1> ${VCF_CONCAT_SORTED} 2>> ${VCF_LOG};
#filtered
#python_script
#annotation
#annotate filtered vcf

vcf-concat ${VCF_SNP} ${VCF_INDEL} 1> ${VCF_CONCAT} 2>> ${VCF_LOG};

cat ${VCF_CONCAT} | vcf-sort | bgzip -f -c 1> ${VCF_CONCAT_SORTED} 2>> ${VCF_LOG};

tabix -p vcf ${VCF_CONCAT_SORTED} ;

oncotator -o SIMPLE_TSV -i VCF --db-dir /home/jdiezperez/data/annotation/oncotator/oncotator_v1_ds_June112014  ${VCF_CONCAT_SORTED} ${ONCOTATOR} hg19;
